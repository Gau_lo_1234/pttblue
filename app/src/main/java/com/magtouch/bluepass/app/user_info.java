package com.magtouch.bluepass.app;

public class user_info {




        public int id;
        public String usercompany;
        public String ExpiryDate;
        public String AccountID;
        public String userphone;
        public String pttmodule;
        public String smsmodule;
        public String QR_Scans;
        public String Custom_events;
        public String alerts;
        public String channel_name;




    public user_info(){

        }
    public user_info(int id, String channel_name){
        this.id=id;
        this.channel_name=channel_name;
    }

        public user_info(int id, String usercompany,String ExpiryDate,String userphone, String pttmodule, String smsmodule, String QR_Scans, String Custom_events, String alerts, String channel_name){
            this.id=id;
             this.usercompany=usercompany;
             this.ExpiryDate=ExpiryDate;
             this.AccountID=AccountID;
             this.userphone=userphone;
             this.pttmodule=pttmodule;
             this.smsmodule=smsmodule;
             this.QR_Scans=QR_Scans;
             this.Custom_events=Custom_events;
             this.alerts=alerts;
            this.channel_name=channel_name;
        }


    public user_info(String usercompany,String ExpiryDate,String userphone, String pttmodule, String smsmodule, String QR_Scans, String Custom_events, String alerts, String channel_name){
        this.usercompany=usercompany;
        this.ExpiryDate=ExpiryDate;
        this.AccountID=AccountID;
        this.userphone=userphone;
        this.pttmodule=pttmodule;
        this.smsmodule=smsmodule;
        this.QR_Scans=QR_Scans;
        this.Custom_events=Custom_events;
        this.alerts=alerts;
        this.channel_name=channel_name;
    }

    public user_info(int id, String usercompany,String ExpiryDate,String userphone, String pttmodule, String smsmodule, String alerts, String channel_name){
        this.id=id;
        this.usercompany=usercompany;
        this.ExpiryDate=ExpiryDate;
        this.userphone=userphone;
        this.pttmodule=pttmodule;
        this.smsmodule=smsmodule;
        this.alerts=alerts;
        this.channel_name=channel_name;
    }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }


    public String getUsercompany() {
        return usercompany;
    }

    public void setUsercompany(String usercompany) {
        this.usercompany = usercompany;
    }

    public String getExpiryDate() {
        return ExpiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        ExpiryDate = expiryDate;
    }

    public String getAccountID() {
        return AccountID;
    }

    public void setAccountID(String accountID) {
        AccountID = accountID;
    }

    public String getUserphone() {
        return userphone;
    }

    public void setUserphone(String userphone) {
        this.userphone = userphone;
    }

    public String getPttmodule() {
        return pttmodule;
    }

    public void setPttmodule(String pttmodule) {
        this.pttmodule = pttmodule;
    }

    public String getSmsmodule() {
        return smsmodule;
    }

    public void setSmsmodule(String smsmodule) {
        this.smsmodule = smsmodule;
    }

    public String getQR_Scans() {
        return QR_Scans;
    }

    public void setQR_Scans(String QR_Scans) {
        this.QR_Scans = QR_Scans;
    }

    public String getCustom_events() {
        return Custom_events;
    }

    public void setCustom_events(String custom_events) {
        Custom_events = custom_events;
    }

    public String getAlerts() {
        return alerts;
    }

    public void setChannel_name(String channel_name) {
        this.channel_name = channel_name;
    }

    public String getChannel_name() {
        return channel_name;
    }

    public void setAlerts(String alerts) {
        this.alerts = alerts;
    }
}
