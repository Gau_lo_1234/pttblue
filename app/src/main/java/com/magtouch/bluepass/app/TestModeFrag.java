package com.magtouch.bluepass.app;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import com.magtouch.bluepass.R;
import com.magtouch.bluepass.db.PlumbleSQLiteDatabase;


public class TestModeFrag extends Fragment {
    private Switch myswitch;
    public static  String TestModeState="TEST MODE: OFF";
    public PlumbleSQLiteDatabase db;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate ( R.layout.fragment_test_mode, container, false );
         db = new PlumbleSQLiteDatabase (getActivity ());
        myswitch=view.findViewById(R.id.switch1);
        final Settingz settingz = db.getSettingz(1);
        myswitch.setChecked(Boolean.parseBoolean(String.valueOf(settingz.get_value())));
        myswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    AlertsFree.isSwitchOn=true;
                    db.updateSettingz(new Settingz("true", "TESTMODE"));
                    TestModeState="TEST MODE: ON";
                    Toast.makeText(getContext (), "Test Mode is turned  on " ,Toast.LENGTH_LONG).show();
                }
                else{
                    db.updateSettingz(new Settingz("false", "TESTMODE"));
                    AlertsFree.isSwitchOn=false;
                    TestModeState="TEST MODE: OFF";
                    Toast.makeText(getContext (), "Test Mode is turned off" , Toast.LENGTH_LONG).show();

                }
            }
        });
        return view;
    }


}
