package com.magtouch.bluepass.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.blikoon.qrcodescanner.QrCodeActivity;
import com.magtouch.jumble.model.Server;
import com.magtouch.bluepass.R;
import com.magtouch.bluepass.db.PlumbleDatabase;
import com.magtouch.bluepass.db.PlumbleSQLiteDatabase;
import com.magtouch.bluepass.servers.FavouriteServerAdapter;


public class BlankFragment extends Fragment implements AdapterView.OnItemClickListener, FavouriteServerAdapter.FavouriteServerAdapterMenuListener {
    private FrameLayout mMainContent;
    private TextView mContentText;
    int[] names = new int[]{R.string.m_bike,R.string.m_boat,R.string.m_car,R.string.m_airport};
    private Button button;
    private ImageView scan_now;
    private static final int REQUEST_CODE_QR_SCAN = 101;
    private final String LOGTAG = "QRCScanner-MainActivity";
    public EditText qrcode;
    public Button qrbut;
    public String server_url="";
    private PlumbleDatabase mDatabase;
    public String IMEI_Number_Holder;
    public TelephonyManager telephonyManager;
    private com.magtouch.bluepass.app.DrawerAdapter mDrawerAdapter;
    private PlumbleActivity test = new PlumbleActivity ();

    int[] icons = new int[]{R.drawable.ic_directions_bike_white_36dp,R.drawable.ic_directions_boat_white_36dp,R.drawable.ic_directions_car_white_36dp,R.drawable.ic_local_airport_white_36dp};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate ( R.layout.fragment_blank, container, false );
        mContentText = (TextView) view.findViewById(R.id.content_text);
        mMainContent = (FrameLayout) view.findViewById(R.id.main_content);
        PlumbleSQLiteDatabase db = new PlumbleSQLiteDatabase(getContext ());

        return view;
    }


    @Override
    public void onItemClick(AdapterView <?> parent, View view, int position, long id) {

    }

    @Override
    public void editServer(Server server) {

    }

    @Override
    public void shareServer(Server server) {

    }

    @Override
    public void deleteServer(Server server) {

    }



}
