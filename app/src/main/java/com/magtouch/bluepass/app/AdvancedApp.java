package com.magtouch.bluepass.app;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.blikoon.qrcodescanner.QrCodeActivity;
import com.magtouch.bluepass.R;
import com.magtouch.bluepass.db.PlumbleDatabase;

public class AdvancedApp extends AppCompatActivity {
ImageView Push_TT;
ImageView Push_QR;
    ImageView Push_ALERT;
    private Button button;
    private ImageView scan_now;
    private static final int REQUEST_CODE_QR_SCAN = 101;
    private final String LOGTAG = "QRCScanner-MainActivity";
    public EditText qrcode;
    public Button qrbut;
    public String server_url="";
    private PlumbleDatabase mDatabase;
    public String IMEI_Number_Holder;
    public TelephonyManager telephonyManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_advanced_app );

        Push_TT=findViewById ( R.id.img_ptt);
        Push_QR=findViewById ( R.id.img_qrscan);
        Push_ALERT=findViewById ( R.id.img_alert);
        Push_TT.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                Intent plumble=new Intent(AdvancedApp.this, PlumbleActivity.class);
                startActivity ( plumble );
            }
        } );
        Push_ALERT.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                Intent alert=new Intent(AdvancedApp.this, AlertActivity.class);
                startActivity (alert );
            }
        } );
        Push_QR.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AdvancedApp.this,QrCodeActivity.class);
                startActivityForResult( i,REQUEST_CODE_QR_SCAN);
            }
        } );
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(resultCode != Activity.RESULT_OK)
        {
            Log.d(LOGTAG,"COULD NOT GET A GOOD RESULT.");
            if(data==null)
                return;
            //Getting the passed result
            String result = data.getStringExtra("com.blikoon.qrcodescanner.error_decoding_image");
            if( result!=null)
            {
                AlertDialog alertDialog = new AlertDialog.Builder(AdvancedApp.this).create();
                alertDialog.setTitle("Scan Error");
                alertDialog.setMessage("QR Code could not be scanned");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
            return;

        }
        if(requestCode == REQUEST_CODE_QR_SCAN)
        {
            if(data==null)
                return;
            //Getting the passed result
            String result = data.getStringExtra("com.blikoon.qrcodescanner.got_qr_scan_relult");
            Log.d(LOGTAG,"Have scan result in your app activity :"+ result);
            AlertDialog alertDialog = new AlertDialog.Builder(AdvancedApp.this).create();
            alertDialog.setTitle("Scan result");
            alertDialog.setMessage(result);
            qrcode.setText ( result );
            alertDialog.setButton( AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            // alertDialog.show();

        }
    }
    private void getDeviceImei() {

        if (ActivityCompat.checkSelfPermission ( this, Manifest.permission.READ_PHONE_STATE ) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        IMEI_Number_Holder = telephonyManager.getDeviceId ();

    }
}
