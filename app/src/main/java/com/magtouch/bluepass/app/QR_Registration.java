package com.magtouch.bluepass.app;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.blikoon.qrcodescanner.QrCodeActivity;
import com.magtouch.jumble.model.Server;
import com.magtouch.bluepass.db.PlumbleDatabase;
import com.magtouch.bluepass.R;
import com.magtouch.bluepass.db.PlumbleSQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QR_Registration extends Activity {
    private Button button;
    public String message = "";
    public String hashedKey="";
    private TextView txtSkip;
    private ImageView scan_now;
    private static final int REQUEST_CODE_QR_SCAN = 101;
    private static final int REQUEST_INTERNET = 200;
    private final String LOGTAG = "QRCScanner-MainActivity";
    public EditText qrcode;
    public ImageButton qrbut;
    public String passkey;
    public String server_url = "";
    public String munch_url = Constants.server_url;
    public String user_exists = "";
    public String batterLevel="";
    public String todaydate="";
    public String temparature="";
    public double batteryLvl;
    private String voltageStr = "";
    public String varSerial="";
    public String SignalStrength="24,0";
    private PlumbleDatabase mDatabase;
    public PlumbleSQLiteDatabase db;
    public  String encryptedString;
    public String IMEI_Number_Holder;
    public TelephonyManager telephonyManager;
    private static final int MY_PERMISSIONS_REQUEST_ACCOUNTS = 1;
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 201;
    public myPhoneStateListener psListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_qr__registration );
        scan_now = (ImageView) findViewById ( R.id.scan_now );
        qrcode = (EditText) findViewById ( R.id.qr_edit );
        qrbut = (ImageButton) findViewById ( R.id.Activated );
        txtSkip = findViewById ( R.id.txtSkip );
        db = new PlumbleSQLiteDatabase(this);
        batterLevel=String.valueOf (  getVoltage ());
        todaydate=current_date ();
        temparature=batteryTemperature ( getApplicationContext () );
        telephonyManager = (TelephonyManager) this.getSystemService ( Context.TELEPHONY_SERVICE );
        psListener = new myPhoneStateListener ();
        telephonyManager = (TelephonyManager) this.getSystemService ( Context.TELEPHONY_SERVICE );
        telephonyManager.listen(psListener,PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
        txtSkip.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                PlumbleSQLiteDatabase rs = new PlumbleSQLiteDatabase ( getBaseContext () );
                rs.updateTerms ( new Terms_Conditions ( 1, "true" ) );
                mDatabase.addPassCode ( new Pass_Code ( 1, "none", "false" ) );
                rs.updatefreeApp ( new freeApp ( 1, "false", "false" ) );
                //Intent free = new Intent ( getBaseContext (),AlertsFree.class );
                Intent free = new Intent ( getBaseContext (), Registration.class );
                startActivity ( free );
                finish();
            }
        } );
        checkAndRequestPermissions ();
        telephonyManager = (TelephonyManager) this.getSystemService ( Context.TELEPHONY_SERVICE );
        mDatabase = new PlumbleSQLiteDatabase ( this ); // TODO add support for cloud storage
        mDatabase.open ();
        getDeviceImei ();
        scan_now.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View view) {
                Intent i = new Intent ( QR_Registration.this, QrCodeActivity.class );
                startActivityForResult ( i, REQUEST_CODE_QR_SCAN );
            }
        } );

        qrbut.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                passkey = qrcode.getText ().toString ();
                device_authenticate ();
               // Toast.makeText ( getBaseContext (), passkey, Toast.LENGTH_SHORT ).show ();
                //Toast.makeText ( getBaseContext (),message,Toast.LENGTH_SHORT ).show ();

                //Toast.makeText ( getBaseContext (),message,Toast.LENGTH_SHORT ).show ();


            }

        } );

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode != Activity.RESULT_OK) {
            Log.d ( LOGTAG, "COULD NOT GET A GOOD RESULT." );
            if (data == null)
                return;
            //Getting the passed result
            String result = data.getStringExtra ( "com.blikoon.qrcodescanner.error_decoding_image" );
            if (result != null) {
                AlertDialog alertDialog = new AlertDialog.Builder ( QR_Registration.this ).create ();
                alertDialog.setTitle ( "Scan Error" );
                alertDialog.setMessage ( "QR Code could not be scanned" );
                alertDialog.setButton ( AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener () {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss ();
                            }
                        } );
                alertDialog.show ();
            }
            return;

        }
        if (requestCode == REQUEST_CODE_QR_SCAN) {
            if (data == null)
                return;
            //Getting the passed result
            String result = data.getStringExtra ( "com.blikoon.qrcodescanner.got_qr_scan_relult" );
            Log.d ( LOGTAG, "Have scan result in your app activity :" + result );
            AlertDialog alertDialog = new AlertDialog.Builder ( QR_Registration.this ).create ();
            alertDialog.setTitle ( "Scan result" );
            alertDialog.setMessage ( result );
            qrcode.setText ( result );
            passkey = result;
            //device_authenticate ();
            PlumbleSQLiteDatabase rs = new PlumbleSQLiteDatabase ( this );
            rs.updateTerms ( new Terms_Conditions ( 1, "true" ) );

            alertDialog.setButton ( AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener () {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss ();
                        }
                    } );
            // alertDialog.show();

        }
    }

    /**
     * The function returns the imei number
     */
    private void getDeviceImei() {

        if (ActivityCompat.checkSelfPermission ( this, Manifest.permission.READ_PHONE_STATE ) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        IMEI_Number_Holder = telephonyManager.getDeviceId ();
        encryptedString = md5(IMEI_Number_Holder);

    }

    private boolean checkAndRequestPermissions() {
        int permissionCAMERA = ContextCompat.checkSelfPermission ( this,
                Manifest.permission.CAMERA );


        int storagePermission = ContextCompat.checkSelfPermission ( this,


                Manifest.permission.READ_EXTERNAL_STORAGE );

        int smsPermission = ContextCompat.checkSelfPermission ( this,


                Manifest.permission.READ_SMS );
        int sendPermission = ContextCompat.checkSelfPermission ( this,


                Manifest.permission.SEND_SMS );
        int contactsPermission = ContextCompat.checkSelfPermission ( this,


                Manifest.permission.READ_CONTACTS );
        int locationPermission = ContextCompat.checkSelfPermission ( this,


                Manifest.permission.ACCESS_COARSE_LOCATION );
        int statePermission = ContextCompat.checkSelfPermission ( this,


                Manifest.permission.READ_PHONE_STATE );

        int recordPermission = ContextCompat.checkSelfPermission ( this,


                Manifest.permission.RECORD_AUDIO );
        int writePermission = ContextCompat.checkSelfPermission ( this,

                Manifest.permission.WRITE_CONTACTS );


        List <String> listPermissionsNeeded = new ArrayList <> ();
        if (storagePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add ( Manifest.permission.READ_EXTERNAL_STORAGE );
        }
        if (smsPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add ( Manifest.permission.READ_SMS );
        }
        if (writePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add ( Manifest.permission.WRITE_CONTACTS );
        }
        if (recordPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add ( Manifest.permission.RECORD_AUDIO );
        }
        if (statePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add ( Manifest.permission.READ_PHONE_STATE );
        }
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add ( Manifest.permission.ACCESS_COARSE_LOCATION );
        }
        if (contactsPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add ( Manifest.permission.READ_CONTACTS );
        }
        if (sendPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add ( Manifest.permission.SEND_SMS );
        }
        if (permissionCAMERA != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add ( Manifest.permission.CAMERA );
        }

        if (!listPermissionsNeeded.isEmpty ()) {
            ActivityCompat.requestPermissions ( this,


                    listPermissionsNeeded.toArray ( new String[listPermissionsNeeded.size ()] ), MY_PERMISSIONS_REQUEST_CAMERA );
            return false;
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCOUNTS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    //Permission Granted Successfully. Write working code here.
                } else {
                    //You did not accept the request can not use the functionality.
                }
                break;
        }
    }



    /**
     * This function is called every time the device is loading...
     */
    public void device_authenticate(){
        final String server_url = Constants.device_connect + passkey + "/" + md5 ( IMEI_Number_Holder);
       Toast.makeText ( getBaseContext (), passkey + IMEI_Number_Holder,Toast.LENGTH_LONG ).show ();
        StringRequest stringRequest= new StringRequest( Request.Method.GET, server_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONArray jsonArray = null;
                    jsonArray = new JSONArray (response);
                    JSONObject jsonObject;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObject = jsonArray.getJSONObject(i);
                        message = jsonObject.getString("message");

                    }

                    if (message.equals ( "no" )) {

                        PlumbleSQLiteDatabase rs = new PlumbleSQLiteDatabase ( getBaseContext () );
                        rs.updateTerms ( new Terms_Conditions ( 1, "false" ) );
                        AlertDialog alertDialog = new AlertDialog.Builder ( QR_Registration.this ).create ();
                        alertDialog.setTitle ( "Alert !!" );
                        alertDialog.setMessage ( "Please contact your provider, you cant utilize our services at this moment" );
                        alertDialog.setButton ( AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener () {
                                    public void onClick(DialogInterface dialog, int which) {

                                        dialog.dismiss ();
                                        //finish();
                                        //System.exit ( 0 );
                                    }
                                } );
                        alertDialog.show ();

                    }
                    if (message.equals ( "yes" )) {
                        munchRequest ();
                        AlertDialog alertDialog = new AlertDialog.Builder ( QR_Registration.this ).create ();
                        alertDialog.setTitle ( "Alert !!" );
                        alertDialog.setMessage ( "Congraulations you can now enjoy all services" );
                        alertDialog.setButton ( AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener () {
                                    public void onClick(DialogInterface dialog, int which) {
                                        PlumbleSQLiteDatabase rs = new PlumbleSQLiteDatabase ( getBaseContext () );
                                        mDatabase.addPassCode ( new Pass_Code ( 1, passkey, "true" ) );
                                        rs.updatefreeApp ( new freeApp ( 1, "true", "true" ) );

                                        Intent home = new Intent ( getBaseContext (), PlumbleActivity.class );
                                        startActivity ( home );
                                        finish();
                                        dialog.dismiss ();

                                    }
                                } );
                        alertDialog.show ();

                    }
                    if (message.equals ( "wel" )) {
                        munchRequest ();
                        AlertDialog alertDialog = new AlertDialog.Builder ( QR_Registration.this ).create ();
                        alertDialog.setTitle ( "Alert !!" );
                        alertDialog.setMessage ( "Welcome back you can now enjoy all services" );
                        alertDialog.setButton ( AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener () {
                                    public void onClick(DialogInterface dialog, int which) {
                                        PlumbleSQLiteDatabase rs = new PlumbleSQLiteDatabase ( getBaseContext () );
                                        mDatabase.addPassCode ( new Pass_Code ( 1, passkey, "true" ) );
                                        rs.updatefreeApp ( new freeApp ( 1, "true", "true" ) );

                                        Intent home = new Intent ( getBaseContext (), PlumbleActivity.class );
                                        startActivity ( home );
                                        finish();
                                        dialog.dismiss ();

                                    }
                                } );
                        alertDialog.show ();

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder ( getBaseContext () ).create ();

                    alertDialog.setTitle ( "Device Information" );
                    alertDialog.setMessage ( response );

                    alertDialog.setButton ( android.support.v7.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener () {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss ();
                                }
                            } );
                   // alertDialog.show ();

                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText ( getBaseContext (), "Check internet connections, else contact your provider", Toast.LENGTH_LONG ).show ();
                error.printStackTrace();

            }
        });


        MySingleton.getInstance(getApplicationContext ()).addToRequestQue(stringRequest);

    }
    public void munchRequest() {

        StringRequest stringRequest = new StringRequest ( Request.Method.POST, munch_url, new Response.Listener <String> () {
            @Override
            public void onResponse(String response) {
                varSerial= (response.substring(response.indexOf("sn=") + 3, 10));
                db.updateSerial( new Serial (1,IMEI_Number_Holder,encryptedString,"",varSerial ) );
                Toast.makeText ( getBaseContext (),varSerial,Toast.LENGTH_LONG ).show ();
                //This code will set the magcell code in the magcell signals table
                device_connect ();
            }

        }, new Response.ErrorListener () {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText ( QR_Registration.this, "Failed to connect to server", Toast.LENGTH_SHORT ).show ();
                error.printStackTrace ();
            }
        } ) {

            @Override
            protected Map<String, String> getParams()  {
                Map <String, String> params = new HashMap<> ();
                params.put ( "id", encryptedString);
                params.put ( "batv", batterLevel);
                params.put ( "temp", temparature);
                params.put ( "signal", SignalStrength);
                params.put ( "sim", "0");
                params.put ( "event", "Z0,Z " + "2010/01/01 01:1S:09");
                return params;
            }
        };
        MySingleton.getInstance ( QR_Registration.this ).addToRequestQue ( stringRequest );
    }

public static final String md5(final String toEncrypt) {
    try {
        final MessageDigest digest = MessageDigest.getInstance("md5");
        digest.update(toEncrypt.getBytes());
        final byte[] bytes = digest.digest();
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            sb.append(String.format("%02X", bytes[i]));
        }
        return sb.toString().toUpperCase();
    } catch (Exception exc) {
        return "";
    }
}

    /**
     * This code returns the battery voltage
     * @return battery voltage
     */
    public String getVoltage()
    {
        DecimalFormat formatter = new DecimalFormat ();
        IntentFilter ifilter = new IntentFilter( Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = this.registerReceiver(null, ifilter);
        int level = batteryStatus.getIntExtra( BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        int voltage = batteryStatus.getIntExtra(BatteryManager.EXTRA_VOLTAGE,
                0);

        double batteryPct = (level / (float)scale)*100;
        batteryLvl=round ( batteryPct,1 );
        formatter.applyPattern(".##");
        voltageStr = formatter.format( (float)voltage/1000 );
        //batteryPct=Double.parseDouble ( voltageStr );
        return voltageStr;
    }
    private static double round (double value, int precision) {
        int scale = (int) Math.pow(10, precision);
        return (double) Math.round(value * scale) / scale;
    }

    /**
     * Functions returns the current date and time
     * @return the current date and time
     */
    public static String current_date(){
        DateFormat dateFormat = new SimpleDateFormat ("yyyy/MM/dd HH:mm:ss");
        Date date = new Date ();
        return dateFormat.format(date);
    }
    /**
     * Functions returns the battery temparature
     * @return the battery temparature
     */
    public static String batteryTemperature(Context context)
    {
        Intent intent = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        float  temp   = ((float) intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE,0)) / 10;
        return String.valueOf(temp);
    }
    public class  myPhoneStateListener extends PhoneStateListener {

        public int signalSupport = 0;

        @Override
        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            super.onSignalStrengthsChanged(signalStrength);

            signalSupport = signalStrength.getGsmSignalStrength();
            SignalStrength=String.valueOf (signalSupport) +",0";
            Log.d(getClass().getCanonicalName(), "------ gsm signal --> " + signalSupport);

            if (signalSupport > 30) {
                Log.d(getClass().getCanonicalName(), "Signal GSM : Good");


            } else if (signalSupport > 20 && signalSupport < 30) {
                Log.d(getClass().getCanonicalName(), "Signal GSM : Avarage");


            } else if (signalSupport < 20 && signalSupport > 3) {
                Log.d(getClass().getCanonicalName(), "Signal GSM : Week");


            } else if (signalSupport < 3) {
                Log.d(getClass().getCanonicalName(), "Signal GSM : Very week");


            }
        }
    }
    /**
     * This function is called every time the device is loading...
     */
    public void device_connect(){
        Serial serial = db.getSerial (1);
        final String magcell_code=serial.getIMEI ();
        server_url=Constants.connect_url.trim() + passkey.trim() + "/" + md5 (IMEI_Number_Holder) + "/" + magcell_code ;
        //Toast.makeText ( getBaseContext (),thisSerial,Toast.LENGTH_LONG ).show ();
        StringRequest stringRequest= new StringRequest( Request.Method.GET, server_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Toast.makeText ( getBaseContext (),magcell_code + server_url + "" ,Toast.LENGTH_LONG ).show ();
                try {

                    JSONArray jsonArray = null;
                    jsonArray = new JSONArray (response);
                    JSONObject jsonObject;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObject = jsonArray.getJSONObject(i);
                        String serverhost = jsonObject.getString("server_ip");
                        String servername=jsonObject.getString ( "server_name" );
                        int serverport = jsonObject.getInt ("server_port");
                        String serverpass="";
                        String serveruser="";
                        String module_status="";
                        //serveruser=jsonObject.getString("distributor_name");
                        //String usercompany=serveruser=jsonObject.getString("distributor_name");
                        //String ExpiryDate=jsonObject.getString("expiry");
                        // String userphone=jsonObject.getString("distributor_phone");
                        //String email= jsonObject.getString ("email");
                        //String AccountID=jsonObject.getString ("device_id");
                        String usercompany=serveruser=jsonObject.getString("firstname");
                        String ExpiryDate=jsonObject.getString("expiry_date");
                        String userphone=jsonObject.getString("cellnumber");
                        String pttmodule=module_status (jsonObject.getString("ptt"));
                        String smsmodule=module_status (jsonObject.getString("sms"));
                        String alerts=module_status (jsonObject.getString("alerts"));
                        String channel_name=jsonObject.getString("channel");

                        db.updateUserInfo (new user_info (1,usercompany,ExpiryDate,userphone,pttmodule,smsmodule,alerts, channel_name));




                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(QR_Registration.this).create();
                    alertDialog.setTitle("Setting Not Found");
                    alertDialog.setMessage(" Settings for your device were not found on this server please check your internet setting first, otherwise contact your service provider");

                    alertDialog.setButton( android.support.v7.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    /*
                                    Intent intent = new Intent ( getApplicationContext (), AlertsFree.class );
                                    startActivity ( intent );
                                    */
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();

                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText (getApplicationContext (), "Something went wrong", Toast.LENGTH_LONG ).show ();
                error.printStackTrace();

            }
        });

        MySingleton.getInstance(QR_Registration.this).addToRequestQue(stringRequest);

    }
    public String module_status(String module){
        String status="Not Active";
        if(module.equals ( "0" )){
            status="Active";
        }
        else{
            status="Not Active";
        }

        return status;
    }

}
