/*
 * Copyright (C) 2014 Andrew Comminos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.magtouch.bluepass.app;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.blikoon.qrcodescanner.QrCodeActivity;
import com.mag_touch.alertmodule.AlertActivity;
import com.magtouch.jumble.IJumbleService;
import com.magtouch.jumble.IJumbleSession;
import com.magtouch.jumble.model.Server;
import com.magtouch.jumble.protobuf.Mumble;
import com.magtouch.jumble.util.JumbleException;
import com.magtouch.jumble.util.JumbleObserver;
import com.magtouch.jumble.util.MumbleURLParser;
import com.magtouch.bluepass.BuildConfig;
import com.magtouch.bluepass.R;
import com.magtouch.bluepass.Settings;
import com.magtouch.bluepass.channel.AccessTokenFragment;
import com.magtouch.bluepass.channel.ChannelFragment;
import com.magtouch.bluepass.channel.ServerInfoFragment;
import com.magtouch.bluepass.db.DatabaseProvider;
import com.magtouch.bluepass.db.PlumbleDatabase;
import com.magtouch.bluepass.db.PlumbleSQLiteDatabase;
import com.magtouch.bluepass.db.PublicServer;
import com.magtouch.bluepass.preference.Preferences;
import com.magtouch.bluepass.servers.FavouriteServerListFragment;
import com.magtouch.bluepass.servers.PublicServerListFragment;
import com.magtouch.bluepass.servers.ServerAdapter;
import com.magtouch.bluepass.servers.ServerEditFragment;
import com.magtouch.bluepass.service.IPlumbleService;
import com.magtouch.bluepass.service.PlumbleService;
import com.magtouch.bluepass.util.JumbleServiceFragment;
import com.magtouch.bluepass.util.JumbleServiceProvider;
import com.magtouch.bluepass.util.PlumbleTrustStore;
import com.noob.lumberjack.LogLevel;
import com.noob.noobcameraflash.managers.NoobCameraManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.spongycastle.util.encoders.Hex;

import java.net.MalformedURLException;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import info.guardianproject.netcipher.proxy.OrbotHelper;

public class AlertsFree extends AppCompatActivity implements ListView.OnItemClickListener, DatabaseProvider,
        SharedPreferences.OnSharedPreferenceChangeListener, freeDrawerAdapter.DrawerDataProvider, LocationListener {

    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    public static String AlertFrag = "";
    public boolean isFlash;
    public static boolean isSwitchOn = true;
    public Button QReg;
    public final int i=0;
    private FavouriteServerListFragment.ServerConnectHandler mConnectHandler;
    private DatabaseProvider mDatabaseProvider;
    private ServerAdapter mServerAdapter;
    private ViewPager mViewPager;
    private Toast mToastToShow;
    public static final String EXTRA_DRAWER_FRAGMENT = "drawer_fragment";
    private IPlumbleService mService;
    private PlumbleDatabase mDatabase;
    private Settings mSettings;
    private static final int REQUEST_CODE_QR_SCAN = 101;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private com.magtouch.bluepass.app.freeDrawerAdapter mDrawerAdapter;
    private FloatingActionButton fab;
    private ProgressDialog mConnectingDialog;
    private AlertDialog mErrorDialog;
    private AlertDialog.Builder mDisconnectPromptBuilder;
    private Boolean isTorchOn = false;
    public ChannelFragment ptt = new ChannelFragment ();
    public PlumbleSQLiteDatabase db = new PlumbleSQLiteDatabase ( this );
    public String servername;
    public String serverhost;
    public String serveruser;
    public int serverport;
    public Camera camera;
    Camera.Parameters parameters;
    public String serverpass;
    public String server_url = "";
    public String IMEI_Number_Holder;
    public String hashedKey = "";
    public Double latitude = 0.0;
    public Double longitude = 0.0;
    private LocationManager locationManager;
    public static String alertStats = "";
    public TelephonyManager telephonyManager;
    /** List of fragments to be notified about service state changes. */
    private List <JumbleServiceFragment> mServiceFragments = new ArrayList <JumbleServiceFragment> ();





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mSettings = Settings.getInstance ( this );
        setTheme ( mSettings.getTheme () );

        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_main );

        setStayAwake ( mSettings.shouldStayAwake () );

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences ( this );
        preferences.registerOnSharedPreferenceChangeListener ( this );
        telephonyManager = (TelephonyManager) this.getSystemService ( Context.TELEPHONY_SERVICE );
        mDatabase = new PlumbleSQLiteDatabase ( this ); // TODO add support for cloud storage
        mDatabase.open ();

        mDrawerLayout = (DrawerLayout) findViewById ( R.id.drawer_layout );
        mDrawerList = (ListView) findViewById ( R.id.left_drawer );
        QReg = (Button) findViewById ( R.id.QReg );

        mDrawerList.setOnItemClickListener ( this );
        locationManager = (LocationManager) getSystemService ( Context.LOCATION_SERVICE );

        mDrawerAdapter = new com.magtouch.bluepass.app.freeDrawerAdapter ( this, this );


        mDrawerList.setAdapter ( mDrawerAdapter );
        ;
        fab = (FloatingActionButton) findViewById ( R.id.floatingActionButton );
        Timer timer = new Timer ();
        TimerTask hourlyTask = new TimerTask () {
            @Override
            public void run () {
                try{Toast.makeText ( getApplicationContext (),"Timer working", Toast.LENGTH_LONG ).show ();}catch(Exception e){}
                if(haveNetworkConnection ()) {
                    int i=0;
                    PlumbleSQLiteDatabase db = new PlumbleSQLiteDatabase(getBaseContext ());
                    List<freeApp> freeAppList = db.getAllfreeApp ();
                    for(freeApp fl : freeAppList){
                        AlertFrag = fl.getAlertMenu ();
                    }

                    if(AlertFrag.equals("true")) {
                        Intent home = new Intent ( getBaseContext (), PlumbleActivity.class );
                        startActivity ( home );
                        finish();
                    }

                }
                else{ Log.i("Gaulo", "Gaudencio Solivatore off");}
            }
        };

        timer.schedule (hourlyTask, 0l, 2000);


        mDrawerToggle = new ActionBarDrawerToggle ( this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close ) {
            @Override
            public void onDrawerClosed(View drawerView) {
                supportInvalidateOptionsMenu ();
            }


            @Override
            public void onDrawerStateChanged(int newState) {
                super.onDrawerStateChanged ( newState );
                // Prevent push to talk from getting stuck on when the drawer is opened.

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                supportInvalidateOptionsMenu ();
            }
        };

        mDrawerLayout.setDrawerListener ( mDrawerToggle );
        getSupportActionBar ().setDisplayHomeAsUpEnabled ( true );
        getSupportActionBar ().setHomeButtonEnabled ( true );

        // Tint logo to theme
        int iconColor = getTheme ().obtainStyledAttributes ( new int[]{android.R.attr.textColorPrimaryInverse} ).getColor ( 0, -1 );
        Drawable logo = getResources ().getDrawable ( R.drawable.ic_home );
        logo.setColorFilter ( iconColor, PorterDuff.Mode.MULTIPLY );
        getSupportActionBar ().setLogo ( logo );

        AlertDialog.Builder dadb = new AlertDialog.Builder ( this );
        dadb.setMessage ( R.string.disconnectSure );
        dadb.setPositiveButton ( R.string.confirm, new DialogInterface.OnClickListener () {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (mService != null) mService.disconnect ();
                loadDrawerFragment ( com.magtouch.bluepass.app.freeDrawerAdapter.ITEM_FAVOURITES );

            }
        } );
        dadb.setNegativeButton ( android.R.string.cancel, null );
        mDisconnectPromptBuilder = dadb;

        if (savedInstanceState == null) {
            if (getIntent () != null && getIntent ().hasExtra ( EXTRA_DRAWER_FRAGMENT )) {
                loadDrawerFragment ( getIntent ().getIntExtra ( EXTRA_DRAWER_FRAGMENT,
                        freeDrawerAdapter.ITEM_ALERTS ) );
            } else {
                loadDrawerFragment ( freeDrawerAdapter.ITEM_ALERTS );
            }
        }

        // If we're given a Mumble URL to show, open up a server edit fragment.
        if (getIntent () != null &&
                Intent.ACTION_VIEW.equals ( getIntent ().getAction () )) {
            String url = getIntent ().getDataString ();
            try {
                Server server = MumbleURLParser.parseURL ( url );

                // Open a dialog prompting the user to connect to the Mumble server.
                DialogFragment fragment = (DialogFragment) ServerEditFragment.createServerEditDialog (
                        AlertsFree.this, server, ServerEditFragment.Action.CONNECT_ACTION, true );
                fragment.show ( getSupportFragmentManager (), "url_edit" );
            } catch (MalformedURLException e) {
                Toast.makeText ( this, getString ( R.string.mumble_url_parse_failed ), Toast.LENGTH_LONG ).show ();
                e.printStackTrace ();
            }
        }

        /**
         * Connect to server and start ppt connection
         */
        getDeviceImei ();
        try{

            hasCameraPermission ();
            hasCamera();}catch (Exception ex){

        }
        if (mService == null) {
            fab.setVisibility ( View.GONE );
        }

        /**
         * The following method is called when the fab button is clicked
         */
        fab.setOnTouchListener ( new View.OnTouchListener () {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction ()) {
                    case MotionEvent.ACTION_DOWN:

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            fab.setImageDrawable ( getResources ().getDrawable ( R.drawable.ic_action_microphone, getBaseContext ().getTheme () ) );
                        }
                        Toast.makeText ( getBaseContext (), "Talking", Toast.LENGTH_SHORT ).show ();
                        //showToast ( v );

                        break;
                    case MotionEvent.ACTION_UP:
                        fab.setBackgroundTintList ( getResources ().getColorStateList ( R.color.solarized_base01 ) );

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            fab.setImageDrawable ( getResources ().getDrawable ( R.drawable.ic_action_microphone_muted, getBaseContext ().getTheme () ) );
                        }
                        Toast.makeText ( getBaseContext (), "Call Ended", Toast.LENGTH_SHORT ).show ();
                        break;
                }
                return true;
            }
        } );
        setVolumeControlStream ( mSettings.isHandsetMode () ?
                AudioManager.STREAM_VOICE_CALL : AudioManager.STREAM_MUSIC );


        //connectToServer (mDatabase.getServer (new Server(1))  );
        try{
        Pass_Code pass_code = db.getPassCode ( 1 );
        hashedKey = pass_code.getPass_key ();}catch (Exception e){

        }
        //device_connect ();
        try {
            NoobCameraManager.getInstance ().init ( this, LogLevel.Verbose );
        } catch (Exception e) {
            e.printStackTrace ();
        }
        if (ActivityCompat.checkSelfPermission ( this, Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission ( this, Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        releaseCam ();
        locationManager.requestLocationUpdates ( LocationManager.GPS_PROVIDER,
                2000, 1, this );
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();

    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onDestroy() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        preferences.unregisterOnSharedPreferenceChangeListener(this);
        mDatabase.close();
        super.onDestroy();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem torchSwitch = menu.findItem(R.id.menu_turn_flash);
        if(isFlash==false){
            torchSwitch.setVisible ( false );
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            torchSwitch.setVisible ( true );
        }

        // Color the action bar icons to the primary text color of the theme.
        int foregroundColor = getSupportActionBar().getThemedContext()
                .obtainStyledAttributes(new int[] { android.R.attr.textColor })
                .getColor(0, -1);
        for(int x=0;x<menu.size();x++) {
            MenuItem item = menu.getItem(x);
            if(item.getIcon() != null) {
                Drawable icon = item.getIcon().mutate(); // Mutate the icon so that the color filter is exclusive to the action bar
                icon.setColorFilter(foregroundColor, PorterDuff.Mode.MULTIPLY);
            }
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.plumble, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(mDrawerToggle.onOptionsItemSelected(item))
            return true;

        switch (item.getItemId()) {
            case R.id.action_disconnect:
                fab.setVisibility ( View.GONE );

                return true;
            case R.id.menu_turn_flash:


              if (isTorchOn==false) {
                  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                      try {
                          NoobCameraManager.getInstance().turnOnFlash();
                      } catch (Exception e) {
                          e.printStackTrace();
                      }
                      updateStatus();
                  }else if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
                  {
                      try {
                          camera = Camera.open ();
                          parameters = camera.getParameters ();
                          parameters.setFlashMode ( Camera.Parameters.FLASH_MODE_TORCH );
                          camera.setParameters ( parameters );
                          camera.startPreview ();
                      }catch (Exception e) {
                          e.printStackTrace();
                      }

                  }
                    item.setIcon(R.drawable.ic_flash_on);
                    isTorchOn = true;
                } else {
                  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                      try {
                          NoobCameraManager.getInstance().turnOffFlash();
                      } catch (Exception e) {
                          e.printStackTrace();
                      }
                      updateStatus();

                  }if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                      try {

                        releaseCam ();

                      }catch (Exception ex){
                          ex.printStackTrace();
                      }

                  }
                    item.setIcon(R.drawable.ic_flash_off);
                    isTorchOn = false;
                }


                return true;
        }

        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (mService != null && keyCode == mSettings.getPushToTalkKey()) {
            mService.onTalkKeyDown();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (mService != null && keyCode == mSettings.getPushToTalkKey()) {
            mService.onTalkKeyUp();
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mDrawerLayout.closeDrawers();
    try{loadDrawerFragment((int) id);}catch(Exception e){
        e.printStackTrace();
    }
    }


    /**
     * Loads a fragment from the drawer.
     */
    private void loadDrawerFragment(int fragmentId) {
        Class<? extends Fragment> fragmentClass = null;
        Bundle args = new Bundle();
        switch (fragmentId) {

            case com.magtouch.bluepass.app.freeDrawerAdapter.ITEM_BLANK:

                    fragmentClass = FreeAppFrag.class;


                break;
            case freeDrawerAdapter.ITEM_NOTIFY:
                //device_connect();
                fragmentClass = Notifylist.class;
                break;
            case com.magtouch.bluepass.app.freeDrawerAdapter.ITEM_ALERTS:

                //device_connect();
                fragmentClass = FreeAppFrag.class;
                break;
            case com.magtouch.bluepass.app.freeDrawerAdapter.ITEM_PREMIUM:
                int i=0;
                PlumbleSQLiteDatabase db = new PlumbleSQLiteDatabase(getBaseContext ());
                List<freeApp> freeAppList = db.getAllfreeApp ();
                for(freeApp fl : freeAppList){
                    AlertFrag = fl.getAlertMenu ();
                }

                if(AlertFrag.equals("true")) {
                    Intent home = new Intent ( getBaseContext (), PlumbleActivity.class );
                    startActivity ( home );
                    finish();
                }else{
                    android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(AlertsFree.this).create();
                    alertDialog.setTitle("Setting Not Found");
                    alertDialog.setMessage(" Settings for your device were not found on this server contact your service provider or visit www.liveguarding.co.za/fav2 to find  a provider. If you have your QR code click Activate tab and enjoy");

                    alertDialog.setButton( android.support.v7.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
                break;
            case com.magtouch.bluepass.app.freeDrawerAdapter.ITEM_PROFILE:
                fragmentClass = Profile.class;
                break;

            case com.magtouch.bluepass.app.freeDrawerAdapter.ITEM_PINNED_CHANNELS:
                fragmentClass = ChannelFragment.class;
                args.putBoolean("pinned", true);
                break;
            case com.magtouch.bluepass.app.freeDrawerAdapter.ITEM_FAVOURITES:
                fragmentClass = SignalEvents.class;
                //fragmentClass = FavouriteServerListFragment.class;
                break;
            case com.magtouch.bluepass.app.freeDrawerAdapter.ITEM_CUSTOM:
                fragmentClass = CustomMessage.class;
                //fragmentClass = FavouriteServerListFragment.class;
                break;
            case freeDrawerAdapter.ITEM_MANAGE:
                fragmentClass = ManageLists.class;
                break;
            case com.magtouch.bluepass.app.freeDrawerAdapter.ITEM_INSTRUCT:
                fragmentClass = InstructionsFragment.class;
                break;
            case com.magtouch.bluepass.app.freeDrawerAdapter.ITEM_PREFS:
                Intent pref = new Intent(this, Preferences.class);
                startActivity(pref);
                break;
            case com.magtouch.bluepass.app.freeDrawerAdapter.ITEM_EXIT:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    finishAffinity ();
                }
                finish ();

                break;
            case com.magtouch.bluepass.app.freeDrawerAdapter.ITEM_TESTMODE:
                fragmentClass = TestModeFrag.class;
                break;
            case com.magtouch.bluepass.app.freeDrawerAdapter.ITEM_SETTINGS:
                Intent prefIntent = new Intent(this, Preferences.class);
                startActivity(prefIntent);
                return;
               /*
            case com.magtouch.bluepass.app.DrawerAdapter.ITEM_MAGALERT:

                return;
            case com.magtouch.bluepass.app.DrawerAdapter.ITEM_PUSHTOTALK:
              ;
                loadDrawerFragment(com.magtouch.bluepass.app.DrawerAdapter.ITEM_FAVOURITES);

                return;
            case com.magtouch.bluepass.app.DrawerAdapter.ITEM_QRCODES:
                loadDrawerFragment( DrawerAdapter.ITEM_QRCODES);
               // fragmentClass = BlankFragment.class;
               // Intent qr = new Intent(this, com.blikoon.qrcodescanner.QrCodeActivity.class);
               // startActivityForResult (qr, REQUEST_CODE_QR_SCAN);
                break;
            case com.magtouch.bluepass.app.DrawerAdapter.ITEM_TRIGGER_EVENTS:
                fragmentClass = TriggerEvents.class;
                break;
                //Intent trigger = new Intent(this, com.blikoon.qrcodescanner.QrCodeActivity.class);
                //startActivityForResult (trigger, REQUEST_CODE_QR_SCAN); */


            default:
                return;
        }
        Fragment fragment = Fragment.instantiate(this, fragmentClass.getName(), args);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, fragment, fragmentClass.getName())
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
        //setTitle(mDrawerAdapter.getItemWithId(fragmentId).title);
    }





    private void setStayAwake(boolean stayAwake) {
        if (stayAwake) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }


    /*
     * HERE BE IMPLEMENTATIONS
     */



    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if(Settings.PREF_THEME.equals(key)) {
            // Recreate activity when theme is changed
            if(Build.VERSION.SDK_INT >= 11)
                recreate();
            else {
                Intent intent = new Intent(this, PlumbleActivity.class);
                finish();
                startActivity(intent);
            }
        } else if (Settings.PREF_STAY_AWAKE.equals(key)) {
            setStayAwake(mSettings.shouldStayAwake());
        } else if (Settings.PREF_HANDSET_MODE.equals(key)) {
            setVolumeControlStream(mSettings.isHandsetMode() ?
                    AudioManager.STREAM_VOICE_CALL : AudioManager.STREAM_MUSIC);
        }
    }

    @Override
    public boolean isConnected() {
        return mService != null && mService.isConnected();
    }

    @Override
    public String getConnectedServerName() {
        if(mService != null && mService.isConnected()) {
            Server server = mService.getTargetServer();
            return server.getName().equals("") ? server.getHost() : server.getName();
        }
        if (BuildConfig.DEBUG)
            throw new RuntimeException("getConnectedServerName should only be called if connected!");
        return "";
    }



    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
    @Override
    public void onLocationChanged(Location location) {
        String msg = "" + location.getLatitude()
                + ", " + location.getLongitude();
       /* Toast.makeText(getBaseContext (), msg,
                Toast.LENGTH_SHORT).show();*/
        latitude=location.getLatitude();
        longitude=location.getLongitude();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {


    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public PlumbleDatabase getDatabase() {
        return mDatabase;
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static AlertActivity.PlaceholderFragment newInstance(int sectionNumber) {
            AlertActivity.PlaceholderFragment fragment = new AlertActivity.PlaceholderFragment ();
            Bundle args = new Bundle ();
            args.putInt ( ARG_SECTION_NUMBER, sectionNumber );
            fragment.setArguments ( args );
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate ( R.layout.fragment_alert, container, false );
            TextView textView = (TextView) rootView.findViewById ( R.id.section_label );
            textView.setText ( getString ( R.string.section_format, getArguments ().getInt ( ARG_SECTION_NUMBER ) ) );
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super ( fm );
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return AlertActivity.PlaceholderFragment.newInstance ( position + 1 );
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }
    }
/*
public void gaulo(int number){
        switch(number){
            case 0:
                case 1:
                case 2:
                loadDrawerFragment(com.magtouch.bluepass.app.DrawerAdapter.ITEM_FAVOURITES);
                Toast.makeText ( getBaseContext (),"Favourites", Toast.LENGTH_LONG ).show ();
                break;
            case 3:
                loadDrawerFragment( DrawerAdapter.ITEM_TRIGGER_EVENTS);
                Toast.makeText ( getBaseContext (),"Trigger Events", Toast.LENGTH_LONG ).show ();
                break;
        }

}
*/
    /**
     * The following method is a test method to launch fragment activities
     */
    public void testApp(int item){
        Class<? extends Fragment> fragmentClass = null;
        Bundle args = new Bundle();

        switch(item){
            case 0:
                //device_connect();
                fragmentClass = ChannelFragment.class;

                break;
            case 1:

               // alertStatus ();
                fragmentClass = AlertFragment.class;
                break;
            case 2:
                releaseCam ();
                Intent i = new Intent(this,QrCodeActivity.class);
                startActivityForResult( i,REQUEST_CODE_QR_SCAN);
                break;
            case 3:
                fragmentClass = TriggerEvents.class;
                break;
        }
        //fragmentClass = AlertFragment.class;
        Fragment fragment = Fragment.instantiate(this, fragmentClass.getName(), args);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, fragment, fragmentClass.getName())
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }
    public void showToast(View view) {
        // Set the toast and duration
        int toastDurationInMilliSeconds = 10000;
        mToastToShow = Toast.makeText(this, "Talking....", Toast.LENGTH_LONG);

        // Set the countdown to display the toast
        CountDownTimer toastCountDown;
        toastCountDown = new CountDownTimer (toastDurationInMilliSeconds, 1000 /*Tick duration*/) {
            public void onTick(long millisUntilFinished) {
                mToastToShow.show();
            }
            public void onFinish() {
                mToastToShow.cancel();
            }
        };

        // Show the toast and starts the countdown
        mToastToShow.show();
        toastCountDown.start();
    }
    private boolean hasCameraPermission() {
        PackageManager pm = getPackageManager();
        return PackageManager.PERMISSION_GRANTED == pm.checkPermission("android.permission.CAMERA", getPackageName());
    }

    /**
     * This function is called every time the device is loading...
     */
    public void device_connect(){
        server_url=Constants.connect_url + hashedKey + "/" + md5(IMEI_Number_Holder);
        //Toast.makeText ( getBaseContext (),server_url,Toast.LENGTH_LONG ).show ();
        StringRequest stringRequest= new StringRequest( Request.Method.GET, server_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONArray jsonArray = null;
                    jsonArray = new JSONArray (response);
                    JSONObject jsonObject;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObject = jsonArray.getJSONObject(i);
                        serverhost = jsonObject.getString("server_ip");
                        servername=jsonObject.getString ( "server_name" );
                        serverport = jsonObject.getInt ("server_port");
                        serverpass="";
                        serveruser=jsonObject.getString("name");

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(AlertsFree.this).create();
                    alertDialog.setTitle("Setting Not Found");
                    alertDialog.setMessage(" Settings for your device were not found on this server contact your service privider");

                    alertDialog.setButton( android.support.v7.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();

                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText ( getBaseContext (), "Something went wrong", Toast.LENGTH_LONG ).show ();
                error.printStackTrace();

            }
        });

        MySingleton.getInstance(AlertsFree.this).addToRequestQue(stringRequest);

    }
    private void getDeviceImei() {

        if (ActivityCompat.checkSelfPermission ( this, Manifest.permission.READ_PHONE_STATE ) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        IMEI_Number_Holder = telephonyManager.getDeviceId ();

    }
    public void onPermissionsClick(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)) {
            String[] permissions = {Manifest.permission.CAMERA};
            requestPermissions(permissions, 1337);
        }
        updateStatus();
    }

    public void onFlashOnClick(View view) {
        try {
            NoobCameraManager.getInstance().turnOnFlash();
        } catch (Exception e) {
            e.printStackTrace();
        }
        updateStatus();
    }

    public void onFlashOffClick(View view) {
        try {
            NoobCameraManager.getInstance().turnOffFlash();
        } catch (Exception e) {
            e.printStackTrace();
        }
        updateStatus();
    }

    public void onFlashToggleClick(View view) {
        try {
            NoobCameraManager.getInstance().toggleFlash();
        } catch (Exception e) {
            e.printStackTrace();
        }
        updateStatus();
    }

    public void onReleaseClick(View view) {
        NoobCameraManager.getInstance().release();
        updateStatus();
    }

    private void updateStatus() {
        if (NoobCameraManager.getInstance().isFlashOn()) {

        } else {

        }
    }
    public static void alertStatus(){
        alertStats="alert";

    }
public void hasCamera(){

        if(getApplicationContext ().getPackageManager ().hasSystemFeature ( PackageManager.FEATURE_CAMERA_FLASH )) {

            camera = Camera.open ();
            parameters = camera.getParameters ();
            isFlash = true;
        }
        else{

            isFlash = false;
        }

}
public void releaseCam(){


        if(camera!=null) {
            parameters.setFlashMode ( Camera.Parameters.FLASH_MODE_OFF );
            camera.setParameters ( parameters );
            camera.stopPreview ();
            camera.release ();
            camera = null;
        }
    }
    public static final String md5(final String toEncrypt) {
        try {
            final MessageDigest digest = MessageDigest.getInstance("md5");
            digest.update(toEncrypt.getBytes());
            final byte[] bytes = digest.digest();
            final StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(String.format("%02X", bytes[i]));
            }
            return sb.toString().toUpperCase();
        } catch (Exception exc) {
            return "";
        }
    }

    private boolean haveNetworkConnection() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
