package com.magtouch.bluepass.app;

public class Constants {


    /**
     * @author Gaudencio Solivatore
     * @settings bluepass device settings via online fav2 settings
     */

    public static final String app_name="fav2_blupass";
    public static final String server_url="http://liveguarding.co.za/og/magcell/munch";
    public static final String connect_url="http://liveguarding.co.za/fav2/index.php?api_bluepass/check_connectivity/";
    public static final String device_connect="http://liveguarding.co.za/fav2/index.php?api_bluepass/device_connect/";
    public static final String gps_connect="http://liveguarding.co.za/fav2/";
    public static final String device_authenticate="http://liveguarding.co.za/fav2/index.php?api_bluepass/device_authenticate/";
    public static final String image_logo="http://liveguarding.co.za/fav2/assets/images/";
    public static final String image_request="http://liveguarding.co.za/fav2/imageRequest/";


    /**
     * @author Gaudencio Solivatore
     * @settings bluepass device settings via local fav2 settings
     */
    /*
    public static final String app_name="fav2_blupass";
    public static final String server_url="http://192.168.43.48/core/magcell/munch";
    public static final String connect_url="http://192.168.43.48/fav2/index.php?api_bluepass/check_connectivity/";
    public static final String device_connect="http://192.168.43.48/fav2/index.php?api_bluepass/device_connect/";
    public static final String gps_connect="http://192.168.43.48/fav2/";
    public static final String device_authenticate="http://192.168.43.48/fav2/index.php?api_bluepass/device_authenticate/";
    public static final String image_logo="http://192.168.43.48/fav2/assets/images/";
    public static final String image_request="http://192.168.43.48/fav2/imageRequest/";
*/
    /**
     * @author Gaudencio Solivatore
     * @settings bluepass device settings via localhost liveguarding settings
     */
    /*
     public static final String app_name="onlineguarding_bluepass";
    public static final String server_url="http://192.168.43.48/core/magcell/munch";
    public static final String connect_url="http://192.168.43.48/core/api_bluepass/check_connectivity/";
    public static final String device_connect="http://192.168.43.48/core/api_bluepass/device_connect/";
    public static final String gps_connect="http://192.168.43.48/fav2/";
    public static final String device_authenticate="http://192.168.43.48/core/api_bluepass/device_authenticate/";
    public static final String image_logo="http://192.168.43.48/fav2/assets/images/";
    public static final String image_request="http://192.168.43.48/fav2/imageRequest/";
    */

    /**
     * @author Gaudencio Solivatore
     * @settings bluepass device settings via online liveguarding settings
     */
    /*
    public static final String app_name="onlineguarding_bluepass";
    public static final String server_url="http://liveguarding.co.za/og/magcell/munch";
    public static final String connect_url="http://liveguarding.co.za/og/api_bluepass/check_connectivity/";
    public static final String device_connect="http://liveguarding.co.za/og/api_bluepass/device_connect/";
    public static final String gps_connect="http://liveguarding.co.za/fav2/";
    public static final String device_authenticate="http://liveguarding.co.za/og/api_bluepass/device_authenticate/";
    public static final String image_logo="http://liveguarding.co.za/fav2/assets/images/";
    public static final String image_request="http://liveguarding.co.za/fav2/imageRequest/";
    */
    /**
     * @author Gaudencio Solivatore
     * @settings bluepass device settings via online onlineguarding settings
     */
    /*
    public static final String server_url="http://onlineguarding.co.za/og/magcell/munch";
    public static final String connect_url="http://onlineguarding.co.za/og/api_bluepass/check_connectivity/";
    public static final String device_connect="http://onlineguarding.co.za/og/api_bluepass/device_connect/";
    public static final String gps_connect="http://onlineguarding.co.za/fav2/";
    public static final String device_authenticate="http://onlineguarding.co.za/on/api_bluepass/device_authenticate/";
    public static final String image_logo="http://onlineguarding.co.za/fav2/assets/images/";
    public static final String image_request="http://onlineguarding.co.za/fav2/imageRequest/";
    */
}
