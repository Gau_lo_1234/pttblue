package com.magtouch.bluepass.app;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Vibrator;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.magtouch.bluepass.R;
import com.magtouch.bluepass.db.PlumbleSQLiteDatabase;

import java.util.List;
import java.util.Timer;

import static com.magtouch.bluepass.app.AlertsFree.isSwitchOn;


public class AlertFragment extends Fragment implements LocationListener {
    private RecyclerView rvFeed;
    private FrameLayout rvFrag;
    public String[] gaulo;
    public Vibrator vibrator;
    public String log = "";
    private Button panicBut;
    public LinearLayout AlertLinear;
    public String theMessage = "";
    public Double latitude = 0.0;
    public Double longitude = 0.0;
    private LocationManager locationManager;
    private static final int PERMISSIONS_READ_LOCATION = 100;
    public boolean doubleTap = true;
    private boolean imgState;
    private TextView gestureText;
    private int i = 0;
    public PlumbleSQLiteDatabase db ;
    public TextView testView;
    private final long interval = 1 * 1000;
    private boolean ispressModeOn = true;
    public Boolean stopCounter = true;
    public final Timer timer = new Timer ();
    private final long startTime = 6 * 1000;
    private CountDownTimer countDownTimer;
    private boolean timerStart = false;



    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate ( R.layout.fragment_alert, container, false );
        panicBut = (Button) view.findViewById ( R.id.ButtonImage );
        testView = (TextView) view.findViewById ( R.id.textView5 );
        AlertLinear=(LinearLayout) view.findViewById (R.id.AlertLinear );
        vibrator = (Vibrator) getActivity ().getSystemService ( Context.VIBRATOR_SERVICE );
        db = new PlumbleSQLiteDatabase ( getActivity () );
        //db.updatefreeApp ( new freeApp ( 1,"true","true" ) );
        gestureText = view.findViewById ( R.id.tvGesture );
        locationManager = (LocationManager) getActivity ().getSystemService ( Context.LOCATION_SERVICE );
        ViewAllContacts();

        AlertLinear.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                i++;
                Handler handler = new Handler ();
                handler.postDelayed ( new Runnable () {

                    @Override
                    public void run() {
                        if (i == 2) {

                            if (doubleTap == true) {
                                panicBut.setText ( "" );
                                gestureText.setText ( "ALERT ON RELEASE" );
                                ispressModeOn = false;
                                doubleTap = false;
                                i = 0;
                            } else {
                                panicBut.setText ( "" );
                                gestureText.setText ( "ALERT ON PRESS" );
                                countDownTimer.cancel ();
                                timerStart = false;
                                ispressModeOn = true;
                                doubleTap = true;
                                i = 0;
                            }
                        }
                        i = 0;
                    }
                }, 500 );
            }
        } );



        panicBut.setOnTouchListener ( new View.OnTouchListener () {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction ()) {
                    case MotionEvent.ACTION_DOWN:
                        panicBut.setBackgroundResource ( R.drawable.ic_press_btn );
                        if (timerStart) {
                            if(ispressModeOn == false ){stopCounter=false;
                                if (stopCounter == false) {
                                    panicBut.setText ( "" );
                                    vibrator.vibrate ( 100 );
                                    timerStart = false;
                                    countDownTimer.cancel ();


                                }}

                        }
                        if (!timerStart) {
                            if (ispressModeOn == true) {
                                countDownTimer.start ();
                                timerStart = true;
                                vibrator.vibrate ( 100 );
                            }
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        panicBut.setBackgroundResource ( R.drawable.ic_panic_logo1 );
                        panicBut.setText ( "" );
                        if (ispressModeOn == true) {
                            panicBut.setText ( "" );
                            countDownTimer.cancel ();
                            timerStart = false;

                        }
                        if(ispressModeOn == false ){stopCounter=true;
                            if (stopCounter == true) {
                                countDownTimer.start ();
                                timerStart = true;
                                vibrator.vibrate ( 100 );
                            }}



                        break;
                }
                return true;
            }
        } );

        countDownTimer = new MyCountDown ( startTime, interval );
        if (ActivityCompat.checkSelfPermission ( getContext (), Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission ( getContext (), Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return null;
        }
        locationManager.requestLocationUpdates ( LocationManager.GPS_PROVIDER,
                2000, 1, this );
        return view;
    }

    @Override
    public void onLocationChanged(Location location) {
        String msg = "" + location.getLatitude()
                + ", " + location.getLongitude();
       /* Toast.makeText(getContext (), msg,
                Toast.LENGTH_SHORT).show();*/
        latitude=location.getLatitude();
        longitude=location.getLongitude();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        try{Toast.makeText(getContext (), "Gps is turned on!! ",
                Toast.LENGTH_SHORT).show();}catch(Exception ex){}

    }

    @Override
    public void onProviderDisabled(String provider) {
       try{ Toast.makeText(getContext (), "Gps is turned off!! ",
                Toast.LENGTH_SHORT).show();}catch(Exception ex){}
    }


    /**
     * Class to implement the CountDownTimer
     */
    public class MyCountDown extends CountDownTimer {
        Vibrator vibrator = (Vibrator) getActivity ().getSystemService(Context.VIBRATOR_SERVICE);
        public MyCountDown(long startTime, long interval) {
            super(startTime, interval);
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onFinish() {
            panicBut.setText("");
           for (int i = 0; i < gaulo.length; i++) {
                sendSMS (gaulo[i], panicMessage());
            }

            vibrator.vibrate(100);

        }

        @Override
        public void onTick(long millisUntilFinish) {
            vibrator.vibrate(100);
           panicBut.setText("" + millisUntilFinish / 1000);


        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public String panicMessage(){
        Messages messages = db.getMessage(1);
        theMessage=messages.getMessage();
        String panicMessage =  theMessage + " http://maps.google.com/maps?q=" + latitude + "," + longitude ;
        db.close();
        return panicMessage;
    }
    /**
     * The method receives two parameters, a number and message then send the message
     * @param phoneNo
     * @param msg
     */
    public void sendSMS(String phoneNo, String msg) {
        if(isSwitchOn){
            Toast.makeText(getContext (), "Cant send message in Test mode",
                    Toast.LENGTH_SHORT).show();
        }
        else {
            try {
                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(phoneNo, null, msg, null, null);
                Toast.makeText(getContext (), "Message Sent",
                        Toast.LENGTH_SHORT).show();
            } catch (Exception ex) {
                Toast.makeText(getContext (), ex.getMessage(),
                        Toast.LENGTH_SHORT).show();
                ex.printStackTrace();
            }
        }
    }
    /**
     * ViewAllContacts function List all contacts added to the Contacts database
     */
    public void ViewAllContacts() {
        List<Contact> contacts = db.getAllContacts();
        for (Contact cn : contacts) {
            log = log + cn.getName().trim() + " ";
        }
        gaulo =log.split(" ");
        db.close();
    }

}
