package com.magtouch.bluepass.app;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Button;
import android.widget.ListView;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.widget.AdapterView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.magtouch.bluepass.R;
import com.magtouch.bluepass.db.PlumbleSQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import static android.app.Activity.RESULT_OK;


public class ManageLists extends Fragment {

    private Button btnContacts;
    private static final int REQUEST_CODE = 1;
    public String GroupName ="";
    public PlumbleSQLiteDatabase db;
    private TelephonyManager telephonyManager;
    // private SqlHandler sqlHandler;
    private ListView lvCustomList;
    public String usernumber="3456789";
    public String server_url="http://www.rfnet.co.za/fa/API/groupDetails?group_id=&group_name=&number=" + usernumber + "&X-API-KEY=boguskey&submit=Get+Group+Details";
    private String log="";
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 999;
    private android.support.v7.app.AlertDialog.Builder build;
    public  ArrayList<Contact> contactList = new ArrayList<>();
    public adapter ListAdapter;
    public  int selectee;
    public  String group_id ="";
    private String IMEI_Number_Holder;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate ( R.layout.fragment_manage_lists, container, false );
        btnContacts = view.findViewById(R.id.btn_contacts);
        db= new PlumbleSQLiteDatabase (getActivity ())  ;
        ListAdapter = new adapter(getActivity (), contactList);
        lvCustomList = view.findViewById(R.id.lv_custom_list);
        btnContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                Uri uri = Uri.parse("content://contacts");
                Intent intent = new Intent(Intent.ACTION_PICK, uri);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivityForResult(intent, REQUEST_CODE);

            }

        });

        telephonyManager = (TelephonyManager) getActivity ().getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(getActivity (), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity (), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS},
                    PERMISSIONS_REQUEST_READ_CONTACTS);requestPermissions(new String[]{Manifest.permission.READ_CONTACTS},
                    PERMISSIONS_REQUEST_READ_CONTACTS);
            
        }
        showList();
        ViewAllContacts();

        return view;
    }
    /**
     * ShowList function displays a list all contacts in the database
     */
    private void showList() {
        contactList.clear();
        String query = "SELECT * FROM contacts ";
        SQLiteDatabase eg=db.getReadableDatabase();
        Cursor c1 = eg.rawQuery(query,null);
        if (c1!=null && c1.getCount() != 0) {
            if (c1.moveToFirst()) {
                do {
                    Contact contact = new Contact();
                    contact.setId(Integer.parseInt(c1.getString(0)));
                    contact.setName(c1.getString(1));
                    contact.setPhone_number(c1.getString(2));
                    contactList.add(contact);

                } while (c1.moveToNext());
            }
        }
        assert c1 != null;
        c1.close();


        lvCustomList.setAdapter(ListAdapter);

        lvCustomList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {

                selectee=position+1;
                build = new android.support.v7.app.AlertDialog.Builder(getActivity ());
                build.setMessage("Do you want to Remove this contact ?");
                build.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        db.deleteContact(new Contact(selectee));
                        //String delQuery = "DELETE FROM contacts WHERE id='"+selectee+"' ";

                        //sqlHandler.executeQuery(delQuery);
                        Toast.makeText( getContext (), + 1 +" Record Deleted.",Toast.LENGTH_SHORT).show();
                        showList();

                        dialog.cancel();
                    }
                });
                build.setNegativeButton("No", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                android.support.v7.app.AlertDialog alert = build.create();
                alert.show();
                return true;
            }

        });
        lvCustomList.clearChoices();


    }





    /**
     *
     */
    public void ViewAllContacts(){
        List<Contact> contacts =db.getAllContacts();
        for (Contact cn : contacts) {
            log = log + cn.getName() + ", \n";
        }

    }



    /**
     *
     * @param requestCode
     * @param resultCode
     * @param intent
     */
    public void onActivityResult(int requestCode, int resultCode,
                                    Intent intent) {
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Uri uri = intent.getData();
                Context applicationContext =getActivity ().getApplicationContext ();
                String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME };

                Cursor cursor = applicationContext.getContentResolver().query(uri, projection,
                        null, null, null);
                assert cursor != null;
                cursor.moveToFirst();
                int numberColumnIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                String number = cursor.getString(numberColumnIndex).replace(" ", "");
                int nameColumnIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                String name = cursor.getString(nameColumnIndex);
                db.addContact(new Contact(name,number));
                Toast.makeText(getActivity (),number + " has been added to the group list", Toast.LENGTH_SHORT).show();
android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
ft.detach(this).attach(this).commit();

            }
        }
    }

public void groupRequest(){
    StringRequest stringRequest=new StringRequest( Request.Method.GET, server_url, new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {


            try
            {
                String jsonString=response;
                JSONObject jObject= new JSONObject(jsonString).getJSONObject("data");
                Iterator<String> keys = jObject.keys();
                while( keys.hasNext() )
                {
                    String key = keys.next();

                    JSONObject innerJObject = jObject.getJSONObject(key);

                    GroupName = innerJObject.getString("GroupName");
                    group_id = innerJObject.getString("GroupID");

                }
            }
            catch (JSONException e){
                e.printStackTrace();
            }

            // Toast.makeText(getActivity (), "Record Saved" + GroupName + group_id, Toast.LENGTH_SHORT).show();


        }
    }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Toast.makeText(getActivity (), "error", Toast.LENGTH_SHORT).show();
            error.printStackTrace();
        }
    }){

        @Override
        protected Map<String, String> getParams() throws AuthFailureError {

            Map<String,String> params = new HashMap<> ();
            // params.put("name", name);
            //params.put("email", email);
            params.put("X-API-KEY", "boguskey");
            params.put("number", "0824692527");
            return params;
        }
    };
    MySingleton.getInstance(getActivity ()).addToRequestQue(stringRequest);

}

}
