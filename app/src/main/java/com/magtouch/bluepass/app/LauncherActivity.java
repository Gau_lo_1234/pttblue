package com.magtouch.bluepass.app;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Criteria;
import android.location.LocationManager;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.magtouch.bluepass.R;
import com.magtouch.bluepass.db.PlumbleSQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.Thread.sleep;

public class LauncherActivity extends Activity {
    public ProgressBar pg2;
    public String log="";
    public String appStatus="";
    public TextView Serial;
    public ImageView imageView;
    public Boolean imageState=true;
    public int count=1;
    public String logoImage="ic_launcher_logo.png";
    public String server_url = Constants.server_url;
    public String server_url1=Constants.image_logo + logoImage;
    public String server_url2=Constants.image_logo + logoImage;
    private TelephonyManager mTelephonyManager;
    public TelephonyManager telephonyManager;
    public myPhoneStateListener psListener;
    public String IMEI_Number_Holder;
    public  String encryptedString;
    private static final int PERMISSIONS_REQUEST_READ_PHONE_STATE = 999;
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 201;
    public String batterLevel="";
    public String todaydate="";
    public String temparature="";
    public double batteryLvl;
    private String voltageStr = "";
    public String varSerial="";
    public String SignalStrength="24,0";
    public Double latitude = 0.0;
    public Double longitude = 0.0;
    private LocationManager locationManager;
    private Criteria criteria;
    private static final int PERMISSIONS_READ_LOCATION = 100;
    public String strGmail = "";
    public PlumbleSQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
        imageView=findViewById(R.id.imageView);
        Serial = findViewById ( R.id.txtSerial );
        psListener = new myPhoneStateListener ();
        telephonyManager = (TelephonyManager) this.getSystemService ( Context.TELEPHONY_SERVICE );
        telephonyManager.listen(psListener,PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
        db = new PlumbleSQLiteDatabase(this);
        /**
         * The following terms and conditions  allow you to get go
         */
        List<Terms_Conditions> term_conditons = db.getAllTerms();
        for(Terms_Conditions fl : term_conditons){
            log = fl.getAccept();
        }
        List<freeApp> freeApps = db.getAllfreeApp ();
        for(freeApp fl : freeApps){
            appStatus = fl.getFreeStatus ();
        }
        /**
         * Fetches logo from the database
         */
        List<imageLogo> logos = db.getAllLogos ();
        for(imageLogo fl : logos){
            logoImage = fl.getLogoname ();
        }
       // checkAndRequestPermissions ();
        loadPermissions ();
        batterLevel=String.valueOf (  getVoltage ());
        todaydate=current_date ();
        temparature=batteryTemperature ( getApplicationContext () );
        if(log.equals("true")){
            munchRequest ();
            logoView ();
            batterLevel=String.valueOf (  getVoltage ());
            todaydate=current_date ();
            temparature=batteryTemperature ( getBaseContext () );



        }

        Thread myThread=  new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    sleep(3000);
                    if(log.equals("false")){
                        Intent intent = new Intent(getApplicationContext(), License.class);
                        startActivity(intent);
                    }
                    else if(appStatus.equals("false")) {

                        Intent free = new Intent ( getApplicationContext (), AlertsFree.class  );
                        startActivity ( free );
                        }

                    else if(appStatus.equals("true")){
                        Intent intent = new Intent ( getApplicationContext (), PlumbleActivity.class );
                        startActivity ( intent );

                    }
                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        });
        myThread.start();
    }


    public void munchRequest() {
        logoView ();
        Serial test=db.getSerial ( 1 );
        Serial.setText ("Serial: " +  test.getIMEI () );

    }

    @SuppressLint("MissingPermission")
    private void getDeviceImei() {

        mTelephonyManager = (TelephonyManager) getSystemService( Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

        }
        IMEI_Number_Holder = mTelephonyManager.getDeviceId();
        byte[] bytesOfMessage = new byte[0];
        try {
            bytesOfMessage = IMEI_Number_Holder.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace ();
        }
        encryptedString = md5(IMEI_Number_Holder);
    }
    public static final String md5(final String toEncrypt) {
        try {
            final MessageDigest digest = MessageDigest.getInstance("md5");
            digest.update(toEncrypt.getBytes());
            final byte[] bytes = digest.digest();
            final StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(String.format("%02X", bytes[i]));
            }
            return sb.toString().toUpperCase();
        } catch (Exception exc) {
            return "";
        }
    }
    public void loadPermissions(){
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{android.Manifest.permission.READ_PHONE_STATE},
                        PERMISSIONS_REQUEST_READ_PHONE_STATE);

                startActivity(getIntent());

            }

            return;
        }
        else {
            getDeviceImei();

        }


    }
    public String getVoltage()
    {
        DecimalFormat formatter = new DecimalFormat ();
        IntentFilter ifilter = new IntentFilter( Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = this.registerReceiver(null, ifilter);
        int level = batteryStatus.getIntExtra( BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        int voltage = batteryStatus.getIntExtra(BatteryManager.EXTRA_VOLTAGE,
                0);

        double batteryPct = (level / (float)scale)*100;
        batteryLvl=round ( batteryPct,1 );
        formatter.applyPattern(".##");
        voltageStr = formatter.format( (float)voltage/1000 );
        //batteryPct=Double.parseDouble ( voltageStr );
        return voltageStr;
    }
    private static double round (double value, int precision) {
        int scale = (int) Math.pow(10, precision);
        return (double) Math.round(value * scale) / scale;
    }

    public static String current_date(){
        DateFormat dateFormat = new SimpleDateFormat ("yyyy/MM/dd HH:mm:ss");
        Date date = new Date ();
        return dateFormat.format(date);
    }
    public static String batteryTemperature(Context context)
    {
        Intent intent = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        float  temp   = ((float) intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE,0)) / 10;
        return String.valueOf(temp);
    }

    private boolean checkAndRequestPermissions() {
        int permissionCAMERA = ContextCompat.checkSelfPermission ( this,
                Manifest.permission.CAMERA );


        int storagePermission = ContextCompat.checkSelfPermission ( this,


                Manifest.permission.READ_EXTERNAL_STORAGE );

        int smsPermission = ContextCompat.checkSelfPermission ( this,


                Manifest.permission.READ_SMS );
        int sendPermission = ContextCompat.checkSelfPermission ( this,


                Manifest.permission.SEND_SMS );
        int contactsPermission = ContextCompat.checkSelfPermission ( this,


                Manifest.permission.READ_CONTACTS );
        int locationPermission = ContextCompat.checkSelfPermission ( this,


                Manifest.permission.ACCESS_COARSE_LOCATION );
        int statePermission = ContextCompat.checkSelfPermission ( this,


                Manifest.permission.READ_PHONE_STATE );

        int recordPermission = ContextCompat.checkSelfPermission ( this,


                Manifest.permission.RECORD_AUDIO );
        int writePermission = ContextCompat.checkSelfPermission ( this,

                Manifest.permission.WRITE_CONTACTS );


        List <String> listPermissionsNeeded = new ArrayList<> ();
        if (storagePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add ( Manifest.permission.READ_EXTERNAL_STORAGE );
        }
        if (smsPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add ( Manifest.permission.READ_SMS );
        }
        if (writePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add ( Manifest.permission.WRITE_CONTACTS );
        }
        if (recordPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add ( Manifest.permission.RECORD_AUDIO );
        }
        if (statePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add ( Manifest.permission.READ_PHONE_STATE );
        }
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add ( Manifest.permission.ACCESS_COARSE_LOCATION );
        }
        if (contactsPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add ( Manifest.permission.READ_CONTACTS );
        }
        if (sendPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add ( Manifest.permission.SEND_SMS );
        }
        if (permissionCAMERA != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add ( Manifest.permission.CAMERA );
        }

        if (!listPermissionsNeeded.isEmpty ()) {
            ActivityCompat.requestPermissions ( this,


                    listPermissionsNeeded.toArray ( new String[listPermissionsNeeded.size ()] ), MY_PERMISSIONS_REQUEST_CAMERA );
            return false;
        }

        return true;
    }
    public class  myPhoneStateListener extends PhoneStateListener {

        public int signalSupport = 0;

        @Override
        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            super.onSignalStrengthsChanged(signalStrength);

            signalSupport = signalStrength.getGsmSignalStrength();
            SignalStrength=String.valueOf (signalSupport) +",0";
            Log.d(getClass().getCanonicalName(), "------ gsm signal --> " + signalSupport);

            if (signalSupport > 30) {
                Log.d(getClass().getCanonicalName(), "Signal GSM : Good");


            } else if (signalSupport > 20 && signalSupport < 30) {
                Log.d(getClass().getCanonicalName(), "Signal GSM : Avarage");


            } else if (signalSupport < 20 && signalSupport > 3) {
                Log.d(getClass().getCanonicalName(), "Signal GSM : Week");


            } else if (signalSupport < 3) {
                Log.d(getClass().getCanonicalName(), "Signal GSM : Very week");


            }
        }
    }
public void logoView(){
    logoImage=logoImage.replaceAll(" ","");
   server_url1=logoImage;
        ImageRequest imageRequest = new ImageRequest(server_url1, new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap response) {
                imageView.setImageBitmap(response);

            }
        }, 0, 0, ImageView.ScaleType.CENTER_CROP, null, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        MrSingleton.getmInstance(LauncherActivity.this).addToRequestQueue(imageRequest);
        imageState=false;


}

    public void device_authenticate() {
        final String server_url = Constants.image_request ;
        StringRequest stringRequest = new StringRequest ( Request.Method.GET, server_url, new Response.Listener <String> () {
            @Override
            public void onResponse(String response) {
             logoImage=response.replaceAll(" ","");
             logoView ();
               /* try {
                    JSONArray jsonArray = null;
                    jsonArray = new JSONArray (response);
                    JSONObject jsonObject;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObject = jsonArray.getJSONObject(i);

                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(getBaseContext ()).create();
                    alertDialog.setTitle("IMEI comparison");
                    alertDialog.setMessage(IMEI_Number_Holder + " Settings for your device were not found on this server contact your service privider");

                    alertDialog.setButton( android.support.v7.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                   alertDialog.show();

                    e.printStackTrace();
                }*/

            }
        }, new Response.ErrorListener () {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText ( LauncherActivity.this, "error", Toast.LENGTH_SHORT ).show ();
                error.printStackTrace ();
            }
        } );
        MySingleton.getInstance ( LauncherActivity.this ).addToRequestQue ( stringRequest );


    }
}