package com.magtouch.bluepass.app;

/**
 * Created by android on 2018/02/17.
 */

public class Terms_Conditions {
    private int id;
    private String Accept;

    public Terms_Conditions(){

    }
    public Terms_Conditions(int id){
this.id=id;
    }

    public Terms_Conditions(int i, String string) {
        this.id=i;
        this.Accept=string;
    }

    public Terms_Conditions(String accept_v){

        this.Accept=accept_v;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAccept() {
        return this.Accept;
    }

    public void setAccept(String accept_v) {
        this.Accept = accept_v;
    }
}
