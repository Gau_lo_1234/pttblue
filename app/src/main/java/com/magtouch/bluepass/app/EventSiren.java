package com.magtouch.bluepass.app;


import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.magtouch.bluepass.R;
import com.magtouch.bluepass.db.PlumbleSQLiteDatabase;
import com.magtouch.jumble.model.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static com.magtouch.bluepass.app.AlertsFree.isSwitchOn;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventSiren extends Fragment implements LocationListener {

    public Button siren;
    public boolean doubleTap = true;
    private boolean imgState;
    public LinearLayout AlertLinear;
    public Vibrator vibrator;
    public Boolean stopCounter = true;
    private boolean ispressModeOn = true;
    private TextView gestureText;
    public String[] gaulo;
    public PlumbleSQLiteDatabase db;
    public String theMessage = "";
    public Double latitude = 0.0;
    public Double longitude = 0.0;
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private ShakeDetector mShakeDetector;
    private LocationManager locationManager;
    private int i = 0;
    public String munch_url = Constants.server_url;
    public String log = "";
    public String server_url="";
    private CountDownTimer countDownTimer;
    public String batterLevel = "";
    public String todaydate = "";
    public String temparature = "";
    public String encryptedString;
    public String voltageStr = "";
    public String SignalStrength = "24,0";
    public double batteryLvl;
    private TelephonyManager mTelephonyManager;
    public TelephonyManager telephonyManager;
    public myPhoneStateListener psListener;
    public String IMEI_Number_Holder;
    private static final int PERMISSIONS_READ_LOCATION = 100;
    public final Timer timer = new Timer ();
    private final long startTime = 6 * 1000;
    private boolean timerStart = false;
    public boolean gpsTimer=true;
    private final long interval = 1 * 1000;


    public EventSiren() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate ( R.layout.fragment_event_siren, container, false );
        siren = view.findViewById ( R.id.siren );
        db = new PlumbleSQLiteDatabase ( getActivity () );
        gestureText = view.findViewById ( R.id.tvGesture );
        AlertLinear = (LinearLayout) view.findViewById ( R.id.LinearSiren );
        countDownTimer = new EventSiren.MyCountDown ( startTime, interval );
        vibrator = (Vibrator) getActivity ().getSystemService ( Context.VIBRATOR_SERVICE );
        psListener = new myPhoneStateListener ();
        telephonyManager = (TelephonyManager) getActivity ().getSystemService ( Context.TELEPHONY_SERVICE );
        telephonyManager.listen ( psListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS );
        batterLevel = String.valueOf ( getVoltage () );
        todaydate = current_date ();
        temparature = batteryTemperature ( getActivity () );
        locationManager = (LocationManager) getActivity ().getSystemService ( Context.LOCATION_SERVICE );
        /**
         *ShakeDetector initialization
         */
        mSensorManager = (SensorManager) getActivity ().getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager
                .getDefaultSensor( Sensor.TYPE_ACCELEROMETER);
        mShakeDetector = new ShakeDetector();
        mShakeDetector.setOnShakeListener(new ShakeDetector.OnShakeListener() {


            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onShake(int count) {


                for (int i = 0; i < gaulo.length; i++) {
                    vibrator.vibrate ( 1000 );
                    sendSMS(gaulo[i], panicMessage());
                }
            }
        });

        ViewAllContacts ();
        getDeviceImei ();
        try{
            mSensorManager.registerListener(mShakeDetector, mAccelerometer, SensorManager.SENSOR_DELAY_UI);
        }catch (Exception e){

            Toast.makeText(getContext (),  "Not Available",
                    Toast.LENGTH_SHORT).show();
        }

        AlertLinear.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                i++;
                Handler handler = new Handler ();
                handler.postDelayed ( new Runnable () {

                    @Override
                    public void run() {
                        if (i == 2) {

                            if (doubleTap == true) {
                                siren.setText ( "" );
                                gestureText.setText ( "ALERT ON RELEASE" );
                                ispressModeOn = false;
                                doubleTap = false;
                                i = 0;
                            } else {
                                siren.setText ( "" );
                                gestureText.setText ( "ALERT ON PRESS" );
                                countDownTimer.cancel ();
                                timerStart = false;
                                ispressModeOn = true;
                                doubleTap = true;
                                i = 0;
                            }
                        }
                        i = 0;
                    }
                }, 500 );
            }
        } );


        siren.setOnTouchListener ( new View.OnTouchListener () {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction ()) {
                    case MotionEvent.ACTION_DOWN:
                        siren.setBackgroundResource ( R.drawable.ic_press_btn );
                        if (timerStart) {
                            if(ispressModeOn == false ){stopCounter=false;
                                if (stopCounter == false) {
                                    siren.setText ( "" );
                                    vibrator.vibrate ( 100 );
                                    timerStart = false;
                                    countDownTimer.cancel ();


                                }}

                        }
                        if (!timerStart) {
                            if (ispressModeOn == true) {
                                countDownTimer.start ();
                                timerStart = true;
                                vibrator.vibrate ( 100 );
                            }
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        siren.setBackgroundResource ( R.drawable.ic_panic_logo1 );
                        siren.setText ( "" );
                        if (ispressModeOn == true) {
                            siren.setText ( "" );
                            countDownTimer.cancel ();
                            timerStart = false;

                        }
                        if(ispressModeOn == false ){stopCounter=true;
                            if (stopCounter == true) {
                                countDownTimer.start ();
                                timerStart = true;
                                vibrator.vibrate ( 100 );
                            }}



                        break;
                }
                return true;
            }
        } );

        Timer timer = new Timer ();
        TimerTask hourlyTask = new TimerTask () {
            @Override
            public void run () {
                try{Toast.makeText ( getContext (),latitude + ", " + longitude, Toast.LENGTH_LONG ).show ();}catch(Exception e){}
                if(latitude!=0.0) {add_gps_location ();}
            }
        };

        timer.schedule (hourlyTask, 0l, 600000 * 3);

        if (ActivityCompat.checkSelfPermission ( getContext (), Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission ( getContext (), Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return null;
        }
        locationManager.requestLocationUpdates ( LocationManager.GPS_PROVIDER,
                2000, 1, this );
        return view;
    }

    @Override
    public void onResume() {
        mSensorManager.registerListener ( mShakeDetector, mAccelerometer, SensorManager.SENSOR_DELAY_UI );
        super.onResume ();
    }
    @Override
    public void onPause() {
        mSensorManager.unregisterListener ( mShakeDetector );
        super.onPause ();
    }

        @Override
    public void onLocationChanged(Location location) {
        String msg = "" + location.getLatitude()
                + ", " + location.getLongitude();
        latitude=location.getLatitude();
        longitude=location.getLongitude();

        /*
       try {Toast.makeText(getContext (), msg,
                Toast.LENGTH_SHORT).show();}
                catch(Exception ex){}*/

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        try {
            Toast.makeText ( getContext (), "Gps is turned on!! ",
                    Toast.LENGTH_SHORT ).show ();
        }catch (Exception ex){}
    }

    @Override
    public void onProviderDisabled(String provider) {
        try {
            Toast.makeText ( getContext (), "Gps is turned off!! ",
                    Toast.LENGTH_SHORT ).show ();
        }catch (Exception ex){}


        if(!provider.contains("gps")){ //if gps is disabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData( Uri.parse("3"));
            getActivity ().sendBroadcast(poke);
        }
    }



    /**
     * Class to implement the CountDownTimer
     */
    public class MyCountDown extends CountDownTimer {
        public MyCountDown(long startTime, long interval) {
            super(startTime, interval);
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onFinish() {

            munchRequest();
            siren.setText("");
            for (int i = 0; i < gaulo.length; i++) {
                sendSMS (gaulo[i], panicMessage());
            }
            add_alert ( panicMessage () );
            vibrator.vibrate(100);
           // Toast.makeText ( getContext (),"signal sent", Toast.LENGTH_LONG ).show();


        }


        @Override
        public void onTick(long millisUntilFinish) {
            siren.setText("" + millisUntilFinish / 1000);
            vibrator.vibrate(100);
        }
    }
    public String getVoltage()
    {
        DecimalFormat formatter = new DecimalFormat ();
        IntentFilter ifilter = new IntentFilter( Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = getActivity ().registerReceiver(null, ifilter);
        int level = batteryStatus.getIntExtra( BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        int voltage = batteryStatus.getIntExtra(BatteryManager.EXTRA_VOLTAGE,
                0);

        double batteryPct = (level / (float)scale)*100;
        batteryLvl=round ( batteryPct,1 );
        formatter.applyPattern(".##");
        voltageStr = formatter.format( (float)voltage/1000 );
        //batteryPct=Double.parseDouble ( voltageStr );
        return voltageStr;
    }
    private static double round (double value, int precision) {
        int scale = (int) Math.pow(10, precision);
        return (double) Math.round(value * scale) / scale;
    }
    public static String current_date(){
        DateFormat dateFormat = new SimpleDateFormat ("yyyy/MM/dd HH:mm:ss");
        Date date = new Date ();
        return dateFormat.format(date);
    }
    public static String batteryTemperature(Context context)
    {
        Intent intent = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        float  temp   = ((float) intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE,0)) / 10;
        return String.valueOf(temp);
    }

    public void munchRequest() {

        StringRequest stringRequest = new StringRequest ( Request.Method.POST, munch_url, new Response.Listener <String> () {
            @Override
            public void onResponse(String response) {
                Toast.makeText ( getActivity (), "Signal sent", Toast.LENGTH_SHORT ).show ();
            }

        }, new Response.ErrorListener () {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText ( getActivity (), "error", Toast.LENGTH_SHORT ).show ();
                error.printStackTrace ();
            }
        } ) {

            @Override
            protected Map<String, String> getParams()  {
                Map <String, String> params = new HashMap<> ();
                params.put ( "id", encryptedString.toString ());
                params.put ( "batv", batterLevel);
                params.put ( "temp", temparature);
                params.put ( "signal", SignalStrength);
                params.put ( "sim", "0");
                params.put ( "event", "A " + todaydate);
                return params;
            }
        };
        MySingleton.getInstance ( getActivity () ).addToRequestQue ( stringRequest );
    }


    @SuppressLint("MissingPermission")
    private void getDeviceImei() {

        mTelephonyManager = (TelephonyManager) getActivity ().getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(getActivity (), android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

        }
        IMEI_Number_Holder = mTelephonyManager.getDeviceId();
        byte[] bytesOfMessage = new byte[0];
        try {
            bytesOfMessage = IMEI_Number_Holder.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace ();
        }
        encryptedString = md5(IMEI_Number_Holder);
    }
    public static final String md5(final String toEncrypt) {
        try {
            final MessageDigest digest = MessageDigest.getInstance("md5");
            digest.update(toEncrypt.getBytes());
            final byte[] bytes = digest.digest();
            final StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(String.format("%02X", bytes[i]));
            }
            return sb.toString().toUpperCase();
        } catch (Exception exc) {
            return "";
        }
    }
    public class  myPhoneStateListener extends PhoneStateListener {

        public int signalSupport = 0;

        @Override
        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            super.onSignalStrengthsChanged(signalStrength);

            signalSupport = signalStrength.getGsmSignalStrength();
            SignalStrength=String.valueOf (signalSupport) +",0";
            Log.d(getClass().getCanonicalName(), "------ gsm signal --> " + signalSupport);

            if (signalSupport > 30) {
                Log.d(getClass().getCanonicalName(), "Signal GSM : Good");


            } else if (signalSupport > 20 && signalSupport < 30) {
                Log.d(getClass().getCanonicalName(), "Signal GSM : Avarage");


            } else if (signalSupport < 20 && signalSupport > 3) {
                Log.d(getClass().getCanonicalName(), "Signal GSM : Week");


            } else if (signalSupport < 3) {
                Log.d(getClass().getCanonicalName(), "Signal GSM : Very week");


            }
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public String panicMessage(){
        Messages messages = db.getMessage(1);
        theMessage=messages.getMessage();
        String panicMessage =  theMessage + " http://maps.google.com/maps?q=" + latitude + "," + longitude ;
        db.close();
        return panicMessage;
    }
    /**
     * The method receives two parameters, a number and message then send the message
     * @param phoneNo
     * @param msg
     */
    public void sendSMS(String phoneNo, String msg) {

            try {
                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(phoneNo, null, msg, null, null);
                Toast.makeText(getContext (), "Message Sent",
                        Toast.LENGTH_SHORT).show();
            } catch (Exception ex) {
                Toast.makeText(getContext (), ex.getMessage(),
                        Toast.LENGTH_SHORT).show();
                ex.printStackTrace();
            }

    }

    /**
     * ViewAllContacts function List all contacts added to the Contacts database
     */
    public void ViewAllContacts() {
        List<Contact> contacts = db.getAllContacts();
        for (Contact cn : contacts) {
            log = log + cn.getName().trim() + " ";
        }
        gaulo =log.split(" ");
        db.close();
    }


    public void add_gps_location() {
        server_url=Constants.gps_connect.trim() + "add_gps_location";
        StringRequest stringRequest = new StringRequest ( Request.Method.POST, server_url, new Response.Listener <String> () {
            @Override
            public void onResponse(String response) {
               //try{ Toast.makeText ( getActivity (), "Signal sent", Toast.LENGTH_SHORT ).show ();}catch(Exception ex){}
            }

        }, new Response.ErrorListener () {
            @Override
            public void onErrorResponse(VolleyError error) {
                //try{Toast.makeText ( getActivity (), "error", Toast.LENGTH_SHORT ).show ();}catch(Exception ex){}
                error.printStackTrace ();
            }
        } ) {

            @Override
            protected Map<String, String> getParams()  {
                Map <String, String> params = new HashMap<> ();
                params.put ( "device_id", encryptedString.toString ());
                params.put ( "long", longitude.toString ());
                params.put ( "lati", latitude.toString ());

                return params;
            }
        };
        MySingleton.getInstance ( getActivity () ).addToRequestQue ( stringRequest );
    }


    public void add_alert(final String message) {
        server_url=Constants.gps_connect.trim() + "add_alert";
        StringRequest stringRequest = new StringRequest ( Request.Method.POST, server_url, new Response.Listener <String> () {
            @Override
            public void onResponse(String response) {
               // Toast.makeText ( getActivity (), "Signal sent", Toast.LENGTH_SHORT ).show ();
            }

        }, new Response.ErrorListener () {
            @Override
            public void onErrorResponse(VolleyError error) {
               // Toast.makeText ( getActivity (), "error", Toast.LENGTH_SHORT ).show ();
                error.printStackTrace ();
            }
        } ) {

            @Override
            protected Map<String, String> getParams()  {
                Map <String, String> params = new HashMap<> ();
                params.put ( "device_id", encryptedString.toString ());
                params.put ( "message", message);
                return params;
            }
        };
        MySingleton.getInstance ( getActivity () ).addToRequestQue ( stringRequest );
    }

}