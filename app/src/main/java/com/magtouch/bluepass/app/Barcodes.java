package com.magtouch.bluepass.app;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.app.AlertDialog;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.blikoon.qrcodescanner.QrCodeActivity;
import com.magtouch.bluepass.R;
import com.magtouch.bluepass.db.PlumbleDatabase;
import com.magtouch.bluepass.db.PlumbleSQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Barcodes extends Fragment {
    private Button button;
    private TextView txtSkip;
    private ImageView scan_now;
    public Camera camera;
    public String message="";
    public String varSerial="";
    public String SignalStrength="24,0";
    public Camera.Parameters parameters;
    private static final int REQUEST_CODE_QR_SCAN = 101;
    private static final int REQUEST_INTERNET = 200;
    private final String LOGTAG = "QRCScanner-MainActivity";
    public EditText qrcode;
    public Button qrbut;
    public String user_exists="";
    public String passkey;
    final  String server_url = Constants.server_url;
    private PlumbleDatabase mDatabase;
    public String batterLevel="";
    public String todaydate="";
    public String temparature="";
    public  String encryptedString;
    private TelephonyManager mTelephonyManager;
    public TelephonyManager telephonyManager;;
    public myPhoneStateListener psListener;
    public String IMEI_Number_Holder;
    private static final int MY_PERMISSIONS_REQUEST_ACCOUNTS = 1;
    private static final int MY_PERMISSIONS_REQUEST_CAMERA=201;


    public Barcodes() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate ( R.layout.fragment_barcodes, container, false );
        scan_now=view.findViewById ( R.id.scan_now );
        checkAndRequestPermissions();
        psListener = new myPhoneStateListener ();
        telephonyManager = (TelephonyManager)getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(psListener,PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
        mDatabase = new PlumbleSQLiteDatabase (getActivity ()); // TODO add support for cloud storage
        mDatabase.open();
        batterLevel=String.valueOf (  getVoltage ());
        todaydate=current_date ();
        temparature=batteryTemperature ( getActivity () );
        getDeviceImei ();
        releaseCam ();
       // device_authenticate();
        try{
            releaseCam ();

        }catch (Exception e){
            e.printStackTrace ();
        }
        scan_now.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View view) {
                releaseCam ();
                //hasCamera ();
                Vibrator vibrator = (Vibrator) getActivity ().getSystemService( Context.VIBRATOR_SERVICE);
                vibrator.vibrate(100);
                Intent i = new Intent(getActivity (),QrCodeActivity.class);
                startActivityForResult( i,REQUEST_CODE_QR_SCAN);
            }
        } );
launch_scanner ();
        return view;
    }
public void launch_scanner(){
    releaseCam ();
    //hasCamera ();
    Vibrator vibrator = (Vibrator) getActivity ().getSystemService( Context.VIBRATOR_SERVICE);
    vibrator.vibrate(100);
    Intent i = new Intent(getActivity (),QrCodeActivity.class);
    startActivityForResult( i,REQUEST_CODE_QR_SCAN);
}
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(resultCode != Activity.RESULT_OK)
        {
            Log.d(LOGTAG,"COULD NOT GET A GOOD RESULT.");
            if(data==null)
                return;
            //Getting the passed result
            String result = data.getStringExtra("com.blikoon.qrcodescanner.error_decoding_image");
            if( result!=null)
            {
                AlertDialog alertDialog = new AlertDialog.Builder(getActivity ()).create();
                alertDialog.setTitle("Scan Error");
                alertDialog.setMessage("QR Code could not be scanned");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
            return;

        }
        if(requestCode == REQUEST_CODE_QR_SCAN)
        {
            if(data==null)
                return;
            //Getting the passed result
            String result = data.getStringExtra("com.blikoon.qrcodescanner.got_qr_scan_relult");
            Log.d(LOGTAG,"Have scan result in your app activity :"+ result);
            AlertDialog alertDialog = new AlertDialog.Builder(getActivity ()).create();
            alertDialog.setTitle("Scan result");
            alertDialog.setMessage(result);
            passkey=result;
            munch_request (result);
            alertDialog.setButton( AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            // alertDialog.show();

        }
    }

    private boolean checkAndRequestPermissions() {
        int permissionCAMERA = ContextCompat.checkSelfPermission(getActivity (),
                Manifest.permission.CAMERA);


        int storagePermission = ContextCompat.checkSelfPermission(getActivity (),


                Manifest.permission.READ_EXTERNAL_STORAGE);

        int smsPermission = ContextCompat.checkSelfPermission(getActivity (),


                Manifest.permission.READ_SMS);
        int sendPermission = ContextCompat.checkSelfPermission(getActivity (),


                Manifest.permission.SEND_SMS);
        int contactsPermission = ContextCompat.checkSelfPermission(getActivity (),


                Manifest.permission.READ_CONTACTS);
        int locationPermission = ContextCompat.checkSelfPermission(getActivity (),


                Manifest.permission.ACCESS_COARSE_LOCATION);
        int statePermission = ContextCompat.checkSelfPermission(getActivity (),


                Manifest.permission.READ_PHONE_STATE);

        int recordPermission = ContextCompat.checkSelfPermission(getActivity (),


                Manifest.permission.RECORD_AUDIO);
        int writePermission = ContextCompat.checkSelfPermission(getActivity (),

                Manifest.permission.WRITE_CONTACTS);




        List<String> listPermissionsNeeded = new ArrayList<> ();
        if (storagePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (smsPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_SMS);
        }
        if (writePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_CONTACTS);
        }
        if (recordPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECORD_AUDIO);
        }
        if (statePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
        }
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (contactsPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_CONTACTS);
        }
        if (sendPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.SEND_SMS);
        }
        if (permissionCAMERA != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity (),


                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MY_PERMISSIONS_REQUEST_CAMERA);
            return false;
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCOUNTS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    //Permission Granted Successfully. Write working code here.
                } else {
                    //You did not accept the request can not use the functionality.
                }
                break;
        }
    }

    /**
     * This function is called every time the device is loading...
     */
    public void device_authenticate(){

        StringRequest stringRequest = new StringRequest ( Request.Method.POST, server_url, new Response.Listener <String> () {
            @Override
            public void onResponse(String response) {
                Toast.makeText ( getActivity (), response, Toast.LENGTH_SHORT ).show ();
               try{ varSerial= (response.substring(response.indexOf("sn=") + 3, 10));

                Toast.makeText ( getActivity (), varSerial, Toast.LENGTH_SHORT ).show ();}catch(Exception ex){
                   ex.printStackTrace ();
               }
            }

        }, new Response.ErrorListener () {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText ( getActivity (), "error", Toast.LENGTH_SHORT ).show ();
                error.printStackTrace ();
            }
        } ) {

            @Override
            protected Map<String, String> getParams()  {
                Map <String, String> params = new HashMap<> ();
                params.put ( "id", encryptedString.toString ());
                params.put ( "batv", batterLevel);
                params.put ( "temp", temparature);
                params.put ( "signal", SignalStrength );
                params.put ( "sim", "0");
                params.put ( "event", "S " + todaydate);
                return params;
            }
        };
        MySingleton.getInstance ( getActivity () ).addToRequestQue ( stringRequest );

    }
    /**
     * This function is called every time the device is loading...
     */
    public void munch_request(final String QRcode){

        StringRequest stringRequest = new StringRequest ( Request.Method.POST, server_url, new Response.Listener <String> () {
            @Override
            public void onResponse(String response) {
                Toast.makeText ( getActivity (), "QR signal Message sent", Toast.LENGTH_SHORT ).show ();


            }

        }, new Response.ErrorListener () {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText ( getActivity (), "error", Toast.LENGTH_SHORT ).show ();
                error.printStackTrace ();
            }
        } ) {

            @Override
            protected Map<String, String> getParams()  {
                Map <String, String> params = new HashMap<> ();
                params.put ( "id", encryptedString.toString ());
                params.put ( "batv", batterLevel);
                params.put ( "temp", temparature);
                params.put ( "signal", SignalStrength);
                params.put ( "sim", "0");
                params.put ( "event", "S " + todaydate + " " + QRcode);
                return params;
            }
        };
        MySingleton.getInstance ( getActivity () ).addToRequestQue ( stringRequest );

    }


    public void hasCamera(){

        if(getActivity ().getPackageManager ().hasSystemFeature ( PackageManager.FEATURE_CAMERA_FLASH )) {

            camera = Camera.open ();
            parameters = camera.getParameters ();

        }
        else{

        }

    }
    public void releaseCam(){
        if(camera!=null) {
            camera.stopPreview ();
            camera.release ();
            camera = null;
        }
    }
    public double getVoltage()
    {
        IntentFilter ifilter = new IntentFilter( Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = getActivity ().registerReceiver(null, ifilter);
        int level = batteryStatus.getIntExtra( BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        double batteryPct = (level / (float)scale)*4.7;
        batteryPct=round ( batteryPct,1 );
        return batteryPct;
    }
    private static double round (double value, int precision) {
        int scale = (int) Math.pow(10, precision);
        return (double) Math.round(value * scale) / scale;
    }

    public static String current_date(){
        DateFormat dateFormat = new SimpleDateFormat ("yyyy/MM/dd HH:mm:ss");
        Date date = new Date ();
        return dateFormat.format(date);
    }
    public static String batteryTemperature(Context context)
    {
        Intent intent = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        float  temp   = ((float) intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE,0)) / 10;
        return String.valueOf(temp);
    }
    public static final String md5(final String toEncrypt) {
        try {
            final MessageDigest digest = MessageDigest.getInstance("md5");
            digest.update(toEncrypt.getBytes());
            final byte[] bytes = digest.digest();
            final StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(String.format("%02X", bytes[i]));
            }
            return sb.toString().toUpperCase();
        } catch (Exception exc) {
            return "";
        }
    }
    @SuppressLint("MissingPermission")
    private void getDeviceImei() {

        mTelephonyManager = (TelephonyManager) getActivity ().getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(getActivity (), android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

        }
        IMEI_Number_Holder = mTelephonyManager.getDeviceId();
        byte[] bytesOfMessage = new byte[0];
        try {
            bytesOfMessage = IMEI_Number_Holder.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace ();
        }
        encryptedString = md5(IMEI_Number_Holder);
    }
    public class  myPhoneStateListener extends PhoneStateListener {

        public int signalSupport = 0;

        @Override
        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            super.onSignalStrengthsChanged(signalStrength);

            signalSupport = signalStrength.getGsmSignalStrength();
            SignalStrength=String.valueOf (signalSupport) +",0";
            Log.d(getClass().getCanonicalName(), "------ gsm signal --> " + signalSupport);

            if (signalSupport > 30) {
                Log.d(getClass().getCanonicalName(), "Signal GSM : Good");


            } else if (signalSupport > 20 && signalSupport < 30) {
                Log.d(getClass().getCanonicalName(), "Signal GSM : Avarage");


            } else if (signalSupport < 20 && signalSupport > 3) {
                Log.d(getClass().getCanonicalName(), "Signal GSM : Week");


            } else if (signalSupport < 3) {
                Log.d(getClass().getCanonicalName(), "Signal GSM : Very week");


            }
        }
    }
}
