package com.magtouch.bluepass.app;

public class freeApp {
    private int id;
    private String freeStatus;
    private String alertMenu;

    public freeApp(){

    }
    public freeApp(int id){
        this.id=id;
    }

    public freeApp(int i, String freeStatus, String alertMenu) {
        this.id=i;
        this.freeStatus=freeStatus;
        this.alertMenu=alertMenu;
    }

    public freeApp(String freeStatus, String alertMenu){

        this.freeStatus=freeStatus;
        this.alertMenu=alertMenu;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFreeStatus() {
        return this.freeStatus;
    }
    public String getAlertMenu() {
        return this.alertMenu;
    }

    public void setFreeStatus(String freeStatus) {
        this.freeStatus = freeStatus;
    }
    public void setAlertMenu(String alertMenu) {
        this.alertMenu = alertMenu;
    }
}
