/*
 * Copyright (C) 2014 Andrew Comminos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.magtouch.bluepass.app;

import android.content.Context;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.magtouch.bluepass.R;
import com.magtouch.bluepass.db.PlumbleSQLiteDatabase;

import java.util.List;

/**
 * Created by andrew on 01/08/13.
 */
public class freeDrawerAdapter extends ArrayAdapter<freeDrawerAdapter.DrawerRow> {
    private  Context context;
    /**
     * Provides context for the drawer module.
     */
    public interface DrawerDataProvider {

        /**
         * @return true if connected, false otherwise.
         */
        public boolean isConnected();
        /**
         * @return The name of the connected server. If not connected, then null.
         */
        public String getConnectedServerName();
    }

    // Drawer rows, integer value is id
    public static final int HEADER_MODULES = 0;
    public static final int ITEM_BLANK = 1;
    public static final int HEADER_GENERAL = 2;
    public static final int ITEM_SETTINGS = 3;
    public static final int HEADER_CONNECTED_SERVER =4;
    public static final int ITEM_SERVER = 5;
    public static final int ITEM_PINNED_CHANNELS = 6;
    public static final int ITEM_INFO = 7;
    public static final int ITEM_ACCESS_TOKENS = 8;
    public static final int HEADER_SERVERS = 9;
    public static final int ITEM_FAVOURITES = 10;
    public static final int ITEM_PUBLIC = 11;
    public static final int ITEM_TESTMODE = 12;
    public static final int ITEM_EXIT = 13;
    public static final int ITEM_INSTRUCT = 14;
    public static final int ITEM_LAN = 15;
    public static final int ITEM_ALERTS= 16;
    public static final int ITEM_PROFILE= 17;
    public static final int ITEM_MANAGE= 18;
    public static final int ITEM_NOTIFY= 19;
    public static final int ITEM_CUSTOM= 20;
    public static final int ITEM_PREFS= 21;
    public static final int ITEM_PREMIUM= 22;









    private static final int HEADER_TYPE = 0;
    private static final int ITEM_TYPE = 1;

    // TODO clean this up.

    public static class DrawerRow {
        int id;
        String title;

        private DrawerRow(int id, String title) {
            this.id = id;
            this.title = title;
        }
    }

    public static class DrawerHeader extends DrawerRow {

        public DrawerHeader(int id, String title) {
            super(id, title);
        }
    }

    public static class DrawerItem extends DrawerRow {
        int icon;

        public DrawerItem(int id, String title, int icon) {
            super(id, title);
            this.icon = icon;
        }
    }

    private DrawerDataProvider mProvider;

    public freeDrawerAdapter(Context context, DrawerDataProvider provider) {

        super(context, 0);
        mProvider = provider;
         add(new freeDrawerAdapter.DrawerItem(ITEM_BLANK, context.getString(R.string.m_blank),  R.drawable.ic_home_white_24dp));
        add(new freeDrawerAdapter.DrawerHeader(HEADER_GENERAL, context.getString(R.string.general)));

        add(new freeDrawerAdapter.DrawerItem(ITEM_PROFILE, context.getString(R.string.action_profile), R.drawable.ic_profile_icon));
        add(new freeDrawerAdapter.DrawerItem(ITEM_MANAGE, context.getString(R.string.action_manage), R.drawable.ic_manage_icon));
        add(new freeDrawerAdapter.DrawerItem(ITEM_NOTIFY, context.getString(R.string.action_notify), R.drawable.ic_notify_icon));
        add(new freeDrawerAdapter.DrawerItem(ITEM_CUSTOM, context.getString(R.string.action_custom), R.drawable.ic_custom_icon));
        add(new freeDrawerAdapter.DrawerItem(ITEM_INSTRUCT, context.getString(R.string.action_instruct), R.drawable.ic_info_outline_black_24dp));
        add(new freeDrawerAdapter.DrawerItem(ITEM_TESTMODE, context.getString(R.string.drawer_testmode), R.drawable.ic_testmode_white));
        //add(new freeDrawerAdapter.DrawerItem(ITEM_PREFS, context.getString(R.string.action_prefs), R.drawable.ic_pref_icon));
        add(new freeDrawerAdapter.DrawerItem(ITEM_EXIT, context.getString(R.string.drawer_exit), R.drawable.ic_switch_white));



    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        int viewType = getItemViewType(position);

        if(v == null) {
            if(viewType == HEADER_TYPE)
                v = LayoutInflater.from(getContext()).inflate(R.layout.list_drawer_header, parent, false);
            else if(viewType == ITEM_TYPE)
                v = LayoutInflater.from(getContext()).inflate(R.layout.list_drawer_item, parent, false);

        }

        if(viewType == HEADER_TYPE) {
            DrawerHeader header = (DrawerHeader) getItem(position);
            TextView title = (TextView) v.findViewById(R.id.drawer_header_title);

            switch((int) getItemId(position)) {
                case HEADER_CONNECTED_SERVER:
                    if(mProvider.isConnected()) {
                        title.setText(mProvider.getConnectedServerName());
                        break;
                    }

                default:
                    title.setText(header.title);
                    break;
            }
        } else if(viewType == ITEM_TYPE) {
            DrawerItem item = (DrawerItem) getItem(position);
            TextView title = (TextView) v.findViewById(R.id.drawer_item_title);
            ImageView icon = (ImageView) v.findViewById(R.id.drawer_item_icon);
            title.setText(item.title);
            icon.setImageResource(item.icon);

            boolean enabled = isEnabled(position);

            // Set text and icon color+alpha based on enabled/disabled state
            int textColor = title.getCurrentTextColor();
            textColor &= 0x00FFFFFF; // Clear alpha bits
            textColor |= enabled ? 0xFF000000 : 0x55000000; // Set alpha bits depending on whether the state is enabled or disabled
            title.setTextColor(textColor);
            icon.setColorFilter(textColor, PorterDuff.Mode.MULTIPLY);

        }

        return v;
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).id;
    }

    public DrawerRow getItemWithId(int id) {
        for(int x=0;x<getCount();x++) {
            DrawerRow row = getItem(x);
            if(row.id == id) return row;
        }
        return null;
    }

    @Override
    public boolean isEnabled(int position) {
        int viewType = getItemViewType(position);
        if(viewType == ITEM_TYPE) {
            switch ((int) getItemId(position)) {
                case ITEM_SERVER:
                case ITEM_INFO:
                case ITEM_ACCESS_TOKENS:
                case ITEM_PINNED_CHANNELS:
                    return mProvider.isConnected();
                case ITEM_LAN:

                    return false;
                default:
                    return true;
            }
        }
        return false; // Default false for headers
    }

    @Override
    public int getItemViewType(int position) {
        Object item = getItem(position);
        if(item instanceof DrawerHeader)
            return HEADER_TYPE;
        else if(item instanceof DrawerItem)
            return ITEM_TYPE;
        return ITEM_TYPE;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    private void hideItem()
    {


    }
}
