package com.magtouch.bluepass.app;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.magtouch.bluepass.R;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;

public class CustomEvent extends Fragment {
    public Button msgsend;
    public String server_url = Constants.server_url;
    public String log = "";
    private CountDownTimer countDownTimer;
    public String CustomSMS;
    public EditText theMessage;
    public String batterLevel="";
    public String todaydate="";
    public String temparature="";
    public  String encryptedString;
    public String voltageStr="";
    public String SignalStrength="24,0";
    public double batteryLvl;
    private TelephonyManager mTelephonyManager;
    public TelephonyManager telephonyManager;
    public myPhoneStateListener psListener;
    public String IMEI_Number_Holder;
    private static final int PERMISSIONS_READ_LOCATION = 100;
    public final Timer timer = new Timer ();
    private final long startTime = 1000;
    private boolean timerStart = false;
    private final long interval = 1 * 1000;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate ( R.layout.fragment_custom_event, container, false );
        msgsend =view.findViewById ( R.id.buttonSend);
        theMessage=view.findViewById ( R.id.themessage );
        countDownTimer = new MyCountDown (startTime, interval);
        psListener = new myPhoneStateListener();
        telephonyManager = (TelephonyManager)getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(psListener,PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
        batterLevel=String.valueOf (  getVoltage ());
        todaydate=current_date ();
        temparature=batteryTemperature ( getActivity () );
        getDeviceImei ();

        msgsend.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                munchRequest ();
            }
        } );
/*
        msgsend.setOnTouchListener ( new View.OnTouchListener () {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch ( event.getAction() ) {
                    case MotionEvent.ACTION_DOWN:
                        munchRequest ();

                        if (!timerStart) {
                            countDownTimer.start();
                            timerStart = true;
                            CustomSMS=theMessage.getText ().toString ();
                            Vibrator vibrator = (Vibrator) getActivity ().getSystemService( Context.VIBRATOR_SERVICE);
                            vibrator.vibrate(100);
                        }
                        break;
                    case MotionEvent.ACTION_UP:

                        countDownTimer.cancel();
                        timerStart = false;
                        break;
                }
                return true;
            }
        } );*/
        return  view;
    }

    /**
     * Class to implement the CountDownTimer
     */
    public class MyCountDown extends CountDownTimer {
        Vibrator vibrator = (Vibrator) getActivity ().getSystemService(Context.VIBRATOR_SERVICE);
        public MyCountDown(long startTime, long interval) {
            super(startTime, interval);
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onFinish() {
            munchRequest ();
            vibrator.vibrate(100);

            // Toast.makeText ( getContext (),"signal sent", Toast.LENGTH_LONG ).show();


        }


        @Override
        public void onTick(long millisUntilFinish) {
            vibrator.vibrate(100);



        }
    }
    public String getVoltage()
    {
        DecimalFormat formatter = new DecimalFormat ();
        IntentFilter ifilter = new IntentFilter( Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = getActivity ().registerReceiver(null, ifilter);
        int level = batteryStatus.getIntExtra( BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        int voltage = batteryStatus.getIntExtra(BatteryManager.EXTRA_VOLTAGE,
                0);

        double batteryPct = (level / (float)scale)*100;
        batteryLvl=round ( batteryPct,0 );
        formatter.applyPattern(".##");
        voltageStr = formatter.format( (float)voltage/1000 );
        //batteryPct=Double.parseDouble ( voltageStr );
        return voltageStr;
    }
    private static double round (double value, int precision) {
        int scale = (int) Math.pow(10, precision);
        return (double) Math.round(value * scale) / scale;
    }

    public static String current_date(){
        DateFormat dateFormat = new SimpleDateFormat ("yyyy/MM/dd HH:mm:ss");
        Date date = new Date ();
        return dateFormat.format(date);
    }
    public static String batteryTemperature(Context context)
    {
        Intent intent = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        float  temp   = ((float) intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE,0)) / 10;
        return String.valueOf(temp);
    }

    public void munchRequest() {

        StringRequest stringRequest = new StringRequest ( Request.Method.POST, server_url, new Response.Listener <String> () {
            @Override
            public void onResponse(String response) {


                Toast.makeText ( getActivity (), "Custom Message Sent", Toast.LENGTH_SHORT ).show ();
                // Toast.makeText ( getActivity (), response, Toast.LENGTH_SHORT ).show ();


            }

        }, new Response.ErrorListener () {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText ( getActivity (), "error", Toast.LENGTH_SHORT ).show ();
                error.printStackTrace ();
            }
        } ) {

            @Override
            protected Map<String, String> getParams()  {
                Map <String, String> params = new HashMap<> ();
                params.put ( "id", encryptedString.toString ());
                params.put ( "batv", batterLevel);
                params.put ( "temp", temparature);
                params.put ( "signal", SignalStrength );
                params.put ( "sim", "0");
                params.put ( "event", "Z6 " + todaydate + " " + CustomSMS);
                return params;
            }
        };
        MySingleton.getInstance ( getActivity () ).addToRequestQue ( stringRequest );
    }

    @SuppressLint("MissingPermission")
    private void getDeviceImei() {

        mTelephonyManager = (TelephonyManager) getActivity ().getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(getActivity (), android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

        }
        IMEI_Number_Holder = mTelephonyManager.getDeviceId();
        byte[] bytesOfMessage = new byte[0];
        try {
            bytesOfMessage = IMEI_Number_Holder.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace ();
        }
        encryptedString = md5(IMEI_Number_Holder);
    }
    public static final String md5(final String toEncrypt) {
        try {
            final MessageDigest digest = MessageDigest.getInstance("md5");
            digest.update(toEncrypt.getBytes());
            final byte[] bytes = digest.digest();
            final StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(String.format("%02X", bytes[i]));
            }
            return sb.toString().toUpperCase();
        } catch (Exception exc) {
            return "";
        }
    }
    /*public class myPhoneStateListener extends PhoneStateListener {
        public int signalStrengthValue;

        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            super.onSignalStrengthsChanged(signalStrength);
            if (signalStrength.isGsm()) {
                if (signalStrength.getGsmSignalStrength() != 99)
                    signalStrengthValue = signalStrength.getGsmSignalStrength() * 2 - 113;
                else
                    signalStrengthValue = signalStrength.getGsmSignalStrength();
            } else {
                signalStrengthValue = signalStrength.getCdmaDbm();
            }
            SignalStrength=String.valueOf ( signalStrengthValue );
        }
    }
    */
    public class  myPhoneStateListener extends PhoneStateListener {

        public int signalSupport = 0;

        @Override
        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            super.onSignalStrengthsChanged(signalStrength);

            signalSupport = signalStrength.getGsmSignalStrength();
            SignalStrength=String.valueOf (signalSupport) +",0";
            Log.d(getClass().getCanonicalName(), "------ gsm signal --> " + signalSupport);

            if (signalSupport > 30) {
                Log.d(getClass().getCanonicalName(), "Signal GSM : Good");


            } else if (signalSupport > 20 && signalSupport < 30) {
                Log.d(getClass().getCanonicalName(), "Signal GSM : Avarage");


            } else if (signalSupport < 20 && signalSupport > 3) {
                Log.d(getClass().getCanonicalName(), "Signal GSM : Week");


            } else if (signalSupport < 3) {
                Log.d(getClass().getCanonicalName(), "Signal GSM : Very week");


            }
        }
    }

}