package com.magtouch.bluepass.app;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by android on 2018/02/16.
 */

public class MrSingleton {

    private static MrSingleton mInstance;
    private RequestQueue requestQueue;
    private static Context mCtx;


    private MrSingleton(Context context){
    mCtx=context;
    requestQueue=getRequestQueue();
    }

    public RequestQueue getRequestQueue(){
        if(requestQueue==null){
            requestQueue= Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return  requestQueue;
    }
public static synchronized MrSingleton getmInstance(Context context){

        if(mInstance==null){
            mInstance=new MrSingleton(context);
        }
        return mInstance;
}

public <T> void addToRequestQueue(Request<T> request){

requestQueue.add(request);
}
}
