package com.magtouch.bluepass.app;

/**
 * Created by android on 2018/01/21.
 */

public class Messages {

    int id;
    String Message;
    String Type;

    public Messages(){

    }

    public Messages(int id, String msg){
        this.id=id;
        this.Message=msg;
    }

    public Messages(int id, String msg, String type){
        this.id=id;
        this.Message=msg;
        this.Type=type;


    }
    public Messages(String msg, String type){

        this.Message=msg;
        this.Message=type;


    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setMessage(String msg) {this.Message=msg;};
    public String getMessage() {
        return this.Message;
    }

    public String getType() {
        return this.Type;
    }
    public void setType(String type) {
        this.Type = type;
    }


}