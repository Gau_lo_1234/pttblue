package com.magtouch.bluepass.app;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.magtouch.bluepass.R;

import java.util.ArrayList;
import java.util.List;

public class FreeAppFrag extends Fragment {
    private int[] tabIcons = {
            R.drawable.common_full_open_on_phone,
            R.drawable.ic_action_favourite_on,
            R.drawable.ic_action_headphones
    };
public  TabLayout tabs;

    public FreeAppFrag() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        // Inflate the layout for this fragment
        View view= inflater.inflate ( R.layout.fragment_freeapp, container, false );
        // Setting ViewPager for each Tabs
        ViewPager viewPager = (FCViewPager) view.findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        // Set Tabs inside Toolbar
        TabLayout tabs = (TabLayout) view.findViewById(R.id.result_tabs);

        tabs.setupWithViewPager(viewPager);

        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
           // setupTabIcons ();

        }
        return view;
    }

    private void setupTabIcons() {

        tabs.getTabAt(0).setIcon(tabIcons[0]);
        //tabs.getTabAt(0).setText ("Alerts");
        tabs.getTabAt(1).setIcon(tabIcons[1]);
        tabs.getTabAt(2).setIcon(tabIcons[2]);
    }
    // Add Fragments to Tabs
    private void setupViewPager(ViewPager viewPager) {



        Adapter adapter = new Adapter (getChildFragmentManager());
        adapter.addFragment(new AlertFragment (), "Panic Mode");
        adapter.addFragment(new TestModeFrag (), "Test Mode");
        adapter.addFragment(new Activation (), "Account Upgrade");
        // adapter.addFragment(new EventArmed (), "Armed");
        //adapter.addFragment(new EventAmbulance(), "Ambulance");


        viewPager.setAdapter(adapter);





    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<> ();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }




}
