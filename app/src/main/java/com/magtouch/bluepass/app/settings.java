package com.magtouch.bluepass.app;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.magtouch.bluepass.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class settings extends Fragment {
    public TextView gpsView;
    public TextView shakeView;
    public SeekBar gpsSeek;
    public SeekBar shakeSeek;
    public int gpsprog=10;
    public int shakeprog=10;

    public settings() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view=inflater.inflate ( R.layout.fragment_settings, container, false );
        gpsView=view.findViewById ( R.id.gpsView );
        shakeView=view.findViewById ( R.id.shakeView );
        gpsSeek=view.findViewById ( R.id.gpsSeek );
        shakeSeek=view.findViewById ( R.id.shakeSeek );
        gpsSeek.setOnSeekBarChangeListener ( new SeekBar.OnSeekBarChangeListener () {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                gpsprog=progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Toast.makeText ( getContext (),gpsprog + "m Accuracy",Toast.LENGTH_LONG ).show ();
            }
        } );

        shakeSeek.setOnSeekBarChangeListener ( new SeekBar.OnSeekBarChangeListener () {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                shakeprog=progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {


                if (shakeprog <= 5) {
                    Toast.makeText ( getContext (), "Low intensity set",Toast.LENGTH_LONG ).show ();

                } else if (shakeprog > 5 && shakeprog <= 6) {

                    Toast.makeText ( getContext (), "Medium intensity set",Toast.LENGTH_LONG ).show ();


                } else {
                    Toast.makeText ( getContext (), "High intensity set",Toast.LENGTH_LONG ).show ();

                }
            }
        } );
        return  view;
    }

}
