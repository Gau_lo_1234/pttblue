/*
 * Copyright (C) 2014 Andrew Comminos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.magtouch.bluepass.app;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.blikoon.qrcodescanner.QrCodeActivity;
import com.googlecode.javacpp.annotation.Platform;
import com.mag_touch.alertmodule.AlertActivity;
import com.magtouch.bluepass.channel.ChannelListAdapter;
import com.magtouch.jumble.IJumbleService;
import com.magtouch.jumble.IJumbleSession;
import com.magtouch.jumble.model.IChannel;
import com.magtouch.jumble.model.IUser;
import com.magtouch.jumble.model.Server;
import com.magtouch.jumble.protobuf.Mumble;
import com.magtouch.jumble.util.JumbleException;
import com.magtouch.jumble.util.JumbleObserver;
import com.magtouch.jumble.util.MumbleURLParser;
import com.magtouch.bluepass.BuildConfig;
import com.magtouch.bluepass.R;
import com.magtouch.bluepass.Settings;
import com.magtouch.bluepass.channel.AccessTokenFragment;
import com.magtouch.bluepass.channel.ChannelFragment;
import com.magtouch.bluepass.channel.ServerInfoFragment;
import com.magtouch.bluepass.db.DatabaseCertificate;
import com.magtouch.bluepass.db.DatabaseProvider;
import com.magtouch.bluepass.db.PlumbleDatabase;
import com.magtouch.bluepass.db.PlumbleSQLiteDatabase;
import com.magtouch.bluepass.db.PublicServer;
import com.magtouch.bluepass.preference.PlumbleCertificateGenerateTask;
import com.magtouch.bluepass.preference.Preferences;
import com.magtouch.bluepass.servers.FavouriteServerListFragment;
import com.magtouch.bluepass.servers.PublicServerListFragment;
import com.magtouch.bluepass.servers.ServerAdapter;
import com.magtouch.bluepass.servers.ServerEditFragment;
import com.magtouch.bluepass.service.IPlumbleService;
import com.magtouch.bluepass.service.PlumbleService;
import com.magtouch.bluepass.util.JumbleServiceFragment;
import com.magtouch.bluepass.util.JumbleServiceProvider;
import com.magtouch.bluepass.util.PlumbleTrustStore;
import com.noob.lumberjack.LogLevel;
import com.noob.noobcameraflash.managers.NoobCameraManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.spongycastle.util.encoders.Hex;
import org.w3c.dom.Node;

import java.net.MalformedURLException;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import info.guardianproject.netcipher.proxy.OrbotHelper;
public class PlumbleActivity extends AppCompatActivity implements ListView.OnItemClickListener,
        FavouriteServerListFragment.ServerConnectHandler, JumbleServiceProvider, DatabaseProvider,
        SharedPreferences.OnSharedPreferenceChangeListener, DrawerAdapter.DrawerDataProvider,
        ServerEditFragment.ServerEditListener {

    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private FavouriteServerListFragment.ServerConnectHandler mConnectHandler;
    public boolean isFlash;
    private List <Node> mNodes;
    public user_info userInfo;
    public static String AlertFrag = "";
    private Toast mToastToShow;
    public Camera camera;
    public Camera.Parameters parameters;
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private ShakeDetector mShakeDetector;
    public String theMessage = "";
    public double longitude = 0.0;
    public double latitude = 0.0;
    public String[] gaulo;
    public static final String EXTRA_DRAWER_FRAGMENT = "drawer_fragment";
    private IPlumbleService mService;
    private PlumbleDatabase mDatabase;
    private Settings mSettings;
    private static final int REQUEST_CODE_QR_SCAN = 101;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private com.magtouch.bluepass.app.DrawerAdapter mDrawerAdapter;
    private FloatingActionButton fab;
    private ProgressDialog mConnectingDialog;
    private AlertDialog mErrorDialog;
    private AlertDialog.Builder mDisconnectPromptBuilder;
    private Boolean isTorchOn = false;
    public ChannelFragment ptt = new ChannelFragment ();
    public PlumbleSQLiteDatabase db = new PlumbleSQLiteDatabase ( this );
    public String servername;
    public String serverhost;
    public String serveruser;
    public int serverport;
    boolean showPinnedOnly=false;
    public Vibrator vibrator;
    public String serverpass;
    public String server_url = "";
    public String IMEI_Number_Holder;
    public String hashedKey = "";
    public static String alertStats = "";
    public TelephonyManager telephonyManager;
    public IJumbleService magService;
    public IChannel magChannel;
    private List<Integer> mRootChannels;
    public static final long CHANNEL_ID_MASK = (0x1L << 32);
    public static final long USER_ID_MASK = (0x1L << 33);

    /**
     * List of fragments to be notified about service state changes.
     */
    private List <JumbleServiceFragment> mServiceFragments = new ArrayList <JumbleServiceFragment> ();

    private ServiceConnection mConnection = new ServiceConnection () {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = ((PlumbleService.PlumbleBinder) service).getService ();
            mService.setSuppressNotifications ( true );
            mService.registerObserver ( mObserver );
            mService.clearChatNotifications (); // Clear chat notifications on resume.
            mDrawerAdapter.notifyDataSetChanged ();

            for (JumbleServiceFragment fragment : mServiceFragments)
                fragment.setServiceBound ( true );

            // Re-show server list if we're showing a fragment that depends on the service.
            if (getSupportFragmentManager ().findFragmentById ( R.id.content_frame ) instanceof JumbleServiceFragment &&
                    !mService.isConnected ()) {
                loadDrawerFragment ( com.magtouch.bluepass.app.DrawerAdapter.ITEM_FAVOURITES );
            }
            updateConnectionState ( getService () );
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }
    };

    private JumbleObserver mObserver = new JumbleObserver () {
        @Override
        public void onConnected() {
            if (mSettings.shouldStartUpInPinnedMode ()) {
                loadDrawerFragment ( com.magtouch.bluepass.app.DrawerAdapter.ITEM_PINNED_CHANNELS );
            } else {
                loadDrawerFragment ( DrawerAdapter.ITEM_BLANK );
            }

            mDrawerAdapter.notifyDataSetChanged ();
            supportInvalidateOptionsMenu ();

            updateConnectionState ( getService () );
        }

        @Override
        public void onConnecting() {
            updateConnectionState ( getService () );
        }

        @Override
        public void onDisconnected(JumbleException e) {
            // Re-show server list if we're showing a fragment that depends on the service.
            if (getSupportFragmentManager ().findFragmentById ( R.id.content_frame ) instanceof JumbleServiceFragment) {
                loadDrawerFragment ( com.magtouch.bluepass.app.DrawerAdapter.ITEM_FAVOURITES );

            }
            mDrawerAdapter.notifyDataSetChanged ();
            supportInvalidateOptionsMenu ();

            updateConnectionState ( getService () );
        }

        @Override
        public void onTLSHandshakeFailed(X509Certificate[] chain) {
            final Server lastServer = getService ().getTargetServer ();

            if (chain.length == 0)
                return;

            try {
                final X509Certificate x509 = chain[0];

                AlertDialog.Builder adb = new AlertDialog.Builder ( PlumbleActivity.this );
                adb.setTitle ( R.string.untrusted_certificate );
                try {
                    MessageDigest digest = MessageDigest.getInstance ( "SHA-1" );
                    byte[] certDigest = digest.digest ( x509.getEncoded () );
                    String hexDigest = new String ( Hex.encode ( certDigest ) );
                    adb.setMessage ( getString ( R.string.certificate_info,
                            x509.getSubjectDN ().getName (),
                            x509.getNotBefore ().toString (),
                            x509.getNotAfter ().toString (),
                            hexDigest ) );
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace ();
                    adb.setMessage ( x509.toString () );
                }
                adb.setPositiveButton ( R.string.allow, new DialogInterface.OnClickListener () {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Try to add to trust store
                        try {
                            String alias = lastServer.getHost ();
                            KeyStore trustStore = PlumbleTrustStore.getTrustStore ( PlumbleActivity.this );
                            trustStore.setCertificateEntry ( alias, x509 );
                            PlumbleTrustStore.saveTrustStore ( PlumbleActivity.this, trustStore );
                            Toast.makeText ( PlumbleActivity.this, R.string.trust_added, Toast.LENGTH_LONG ).show ();
                            connectToServer ( lastServer );
                        } catch (Exception e) {
                            e.printStackTrace ();
                            Toast.makeText ( PlumbleActivity.this, R.string.trust_add_failed, Toast.LENGTH_LONG ).show ();
                        }
                    }
                } );
                adb.setNegativeButton ( R.string.wizard_cancel, null );
                adb.show ();
            } catch (CertificateException e) {
                e.printStackTrace ();
            }
        }

        @Override
        public void onPermissionDenied(String reason) {
            AlertDialog.Builder adb = new AlertDialog.Builder ( PlumbleActivity.this );
            adb.setTitle ( R.string.perm_denied );
            adb.setMessage ( reason );
            adb.show ();
        }
    };


    /**
     * An arbitrary node in the channel-user hierarchy.
     * Can be either a channel or user.
     */

    private static class Node {
        private Node mParent;
        private IChannel mChannel;
        private IUser mUser;
        private int mDepth;
        private boolean mExpanded;

        public Node(Node parent, int depth, IChannel channel) {
            mParent = parent;
            mChannel = channel;
            mDepth = depth;
            mExpanded = true;
        }

        public Node(Node parent, int depth, IUser user) {
            mParent = parent;
            mUser = user;
            mDepth = depth;
        }
        public boolean isChannel() {
            return mChannel != null;
        }

        public boolean isUser() {
            return mUser != null;
        }

        public Node getParent() {
            return mParent;
        }

        public IChannel getChannel() {
            return mChannel;
        }

        public IUser getUser() {
            return mUser;
        }

        public Long getId() throws RemoteException {
            // Apply flags to differentiate integer-length identifiers
            if (isChannel()) {
                return CHANNEL_ID_MASK | mChannel.getId();
            } else if (isUser()) {
                return USER_ID_MASK | mUser.getSession();
            }
            return null;
        }

        public int getDepth() {
            return mDepth;
        }

        public boolean isExpanded() {
            return mExpanded;
        }

        public void setExpanded(boolean expanded) {
            mExpanded = expanded;
        }
    }
    /**
     * Recursively creates a list of {@link ChannelListAdapter.Node}s representing the channel hierarchy.
     *
     * @param parent  The parent node to propagate under.
     * @param channel The parent channel.
     * @param depth   The current depth of the subtree.
     * @param nodes   An accumulator to store generated nodes into.
     */

    private void constructNodes(Node parent, IChannel channel, int depth,
                                List <Node> nodes) {
        Node channelNode = new Node ( parent, depth, channel );

        userInfo = db.get_UserInfo ( 1 );
        String thechannel = channel.getName ();


        if (thechannel.equals ( userInfo.getChannel_name () )) {
            nodes.add ( channelNode );
            mService.getSession ().joinChannel ( channel.getId () );
        }

            Boolean expandSetting = mExpandedChannels.get ( channel.getId () );
            if ((expandSetting == null && channel.getSubchannelUserCount () == 0)
                    || (expandSetting != null && !expandSetting)) {
                channelNode.setExpanded ( false );
                return; // Skip adding children of contracted/empty channels.
            }


        for (IUser user : (List <IUser>) channel.getUsers ()) {
            if (user == null) {
                continue;
            }
            String theuser = channel.getName ();
            if (theuser.equals ( userInfo.getChannel_name () )) {
                nodes.add ( new Node ( channelNode, depth, user ) );
            }
        }
        for (IChannel subc : (List <IChannel>) channel.getSubchannels ()) {
            constructNodes ( channelNode, subc, depth + 1, nodes );
        }
    }

    public void updateChannels() {
        if (!magService.isConnected())
            return;

        IJumbleSession session = magService.getSession();
        mNodes.clear();
        for (int cid : mRootChannels) {
            IChannel channel = session.getChannel(cid);
            if (channel != null) {
                constructNodes(null, channel, 0, mNodes);
            }
        }
    }
    private HashMap<Integer, Boolean> mExpandedChannels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mSettings = Settings.getInstance ( this );
        setTheme ( mSettings.getTheme () );

        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_main );

        setStayAwake ( mSettings.shouldStayAwake () );

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences ( this );
        preferences.registerOnSharedPreferenceChangeListener ( this );
        telephonyManager = (TelephonyManager) this.getSystemService ( Context.TELEPHONY_SERVICE );
        mDatabase = new PlumbleSQLiteDatabase ( this ); // TODO add support for cloud storage
        mDatabase.open ();
        vibrator = (Vibrator) getSystemService ( Context.VIBRATOR_SERVICE );
        mDrawerLayout = (DrawerLayout) findViewById ( R.id.drawer_layout );
        mDrawerList = (ListView) findViewById ( R.id.left_drawer );
        mDrawerList.setOnItemClickListener ( this );
        mDrawerAdapter = new com.magtouch.bluepass.app.DrawerAdapter ( this, this );
        mDrawerList.setAdapter ( mDrawerAdapter );
        fab = (FloatingActionButton) findViewById ( R.id.floatingActionButton );
        /**
         *ShakeDetector initialization
         */
        mSensorManager = (SensorManager) getSystemService ( Context.SENSOR_SERVICE );
        mAccelerometer = mSensorManager
                .getDefaultSensor ( Sensor.TYPE_ACCELEROMETER );
        mShakeDetector = new ShakeDetector ();
        mShakeDetector.setOnShakeListener ( new ShakeDetector.OnShakeListener () {


            @Override
            public void onShake(int count) {


                for (String aGaulo : gaulo) {
                    vibrator.vibrate ( 100 );
                    sendSMS ( aGaulo, panicMessage () );
                }
            }
        } );
        /**
         * This following is a timer functio to start events
         */
        /*
        Timer timer = new Timer ();
        TimerTask hourlyTask = new TimerTask () {
            @Override
            public void run () {
                try{Toast.makeText ( getApplicationContext (),"Timer working" , Toast.LENGTH_LONG ).show ();}catch(Exception e){}
                //Log.i("Gaulo", "Gaudencio Solivatore off2" + mService.getConnectionState ().toString ()  );

                if(haveNetworkConnection ()) {
                    int i=0;
                    PlumbleSQLiteDatabase db = new PlumbleSQLiteDatabase(getApplicationContext ());
                    List<freeApp> freeAppList = db.getAllfreeApp ();
                    for(freeApp fl : freeAppList){
                        AlertFrag = fl.getAlertMenu ();
                    }

                    if(AlertFrag.equals("false")) {
                        Intent home = new Intent ( getBaseContext (), AlertsFree.class );
                        getService().disconnect();
                        startActivity ( home );
                        finish();
                    }

                }
                else{
                    try{if(mService != null) mService.disconnect();}catch(Exception e){};
                    Intent home = new Intent ( getBaseContext (), AlertsFree.class );
                    startActivity ( home );

                    finish();

                    Log.i("Gaulo", "Gaudencio Solivatore off");}
            }
        };

        timer.schedule (hourlyTask, 0l, 2000);
        */

        mDrawerToggle = new ActionBarDrawerToggle ( this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close ) {
            @Override
            public void onDrawerClosed(View drawerView) {
                supportInvalidateOptionsMenu ();
            }


            @Override
            public void onDrawerStateChanged(int newState) {
                super.onDrawerStateChanged ( newState );
                // Prevent push to talk from getting stuck on when the drawer is opened.
                if (getService () != null && getService ().isConnected ()) {
                    IJumbleSession session = getService ().getSession ();
                    if (session.isTalking () && !mSettings.isPushToTalkToggle ()) {
                        session.setTalkingState ( false );
                    }
                }
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                supportInvalidateOptionsMenu ();
            }
        };

        mDrawerLayout.setDrawerListener ( mDrawerToggle );
        getSupportActionBar ().setDisplayHomeAsUpEnabled ( true );
        getSupportActionBar ().setHomeButtonEnabled ( true );
        // Tint logo to theme
        int iconColor = getTheme ().obtainStyledAttributes ( new int[]{android.R.attr.textColorPrimaryInverse} ).getColor ( 0, -1 );
        Drawable logo = getResources ().getDrawable ( R.drawable.ic_home );
        logo.setColorFilter ( iconColor, PorterDuff.Mode.MULTIPLY );
        getSupportActionBar ().setLogo ( logo );

        AlertDialog.Builder dadb = new AlertDialog.Builder ( this );
        dadb.setMessage ( R.string.disconnectSure );
        dadb.setPositiveButton ( R.string.confirm, new DialogInterface.OnClickListener () {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (mService != null) mService.disconnect ();
                loadDrawerFragment ( com.magtouch.bluepass.app.DrawerAdapter.ITEM_FAVOURITES );

            }
        } );
        dadb.setNegativeButton ( android.R.string.cancel, null );
        mDisconnectPromptBuilder = dadb;

        if (savedInstanceState == null) {
            if (getIntent () != null && getIntent ().hasExtra ( EXTRA_DRAWER_FRAGMENT )) {
                loadDrawerFragment ( getIntent ().getIntExtra ( EXTRA_DRAWER_FRAGMENT,
                        DrawerAdapter.ITEM_BLANK ) );
            } else {
                loadDrawerFragment ( com.magtouch.bluepass.app.DrawerAdapter.ITEM_BLANK );

            }
        }

        // If we're given a Mumble URL to show, open up a server edit fragment.
        if (getIntent () != null &&
                Intent.ACTION_VIEW.equals ( getIntent ().getAction () )) {
            String url = getIntent ().getDataString ();
            try {
                Server server = MumbleURLParser.parseURL ( url );

                // Open a dialog prompting the user to connect to the Mumble server.
                DialogFragment fragment = (DialogFragment) ServerEditFragment.createServerEditDialog (
                        PlumbleActivity.this, server, ServerEditFragment.Action.CONNECT_ACTION, true );
                fragment.show ( getSupportFragmentManager (), "url_edit" );
            } catch (MalformedURLException e) {
                Toast.makeText ( this, getString ( R.string.mumble_url_parse_failed ), Toast.LENGTH_LONG ).show ();
                e.printStackTrace ();
            }
        }

        /**
         * Connect to server and start ppt connection
         */
        getDeviceImei ();
        try {
            // releaseCam ();
            hasCameraPermission ();
            hasCamera ();

        } catch (Exception ex) {

        }
        if (mService == null) {
            fab.setVisibility ( View.GONE );
        }

        /**
         * The following method is called when the fab button is clicked
         */
        fab.setOnTouchListener ( new View.OnTouchListener () {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction ()) {
                    case MotionEvent.ACTION_DOWN:
                        fab.setBackgroundTintList ( getResources ().getColorStateList ( R.color.red ) );
                        getService ().onTalkKeyDown ();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            fab.setImageDrawable ( getResources ().getDrawable ( R.drawable.ic_action_microphone, getBaseContext ().getTheme () ) );
                        }
                        Toast.makeText ( getBaseContext (), "Talking", Toast.LENGTH_SHORT ).show ();
                        //showToast ( v );

                        break;
                    case MotionEvent.ACTION_UP:
                        fab.setBackgroundTintList ( getResources ().getColorStateList ( R.color.solarized_base01 ) );
                        getService ().onTalkKeyUp ();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            fab.setImageDrawable ( getResources ().getDrawable ( R.drawable.ic_action_microphone_muted, getBaseContext ().getTheme () ) );
                        }
                        Toast.makeText ( getBaseContext (), "Call Ended", Toast.LENGTH_SHORT ).show ();
                        break;
                }
                return true;
            }
        } );
        // hasCamera();
        setVolumeControlStream ( mSettings.isHandsetMode () ?
                AudioManager.STREAM_VOICE_CALL : AudioManager.STREAM_MUSIC );

        if (mSettings.isFirstRun ()) showSetupWizard ();
        //mDatabaseProvider.getDatabase().getServers();

        //connectToServer (mDatabase.getServer (new Server(1))  );
        Pass_Code pass_code = db.getPassCode ( 1 );
        hashedKey = pass_code.getPass_key ().trim ();
        //hashedKey="276dc5e297e730bfb274069079cc208e";
        //Toast.makeText ( getBaseContext (),hashedKey,Toast.LENGTH_SHORT ).show ();
        device_connect ();
        try {
            NoobCameraManager.getInstance ().init ( this, LogLevel.Verbose );
        } catch (Exception e) {
            e.printStackTrace ();
        }
        releaseCam ();
        try {
            mSensorManager.registerListener ( mShakeDetector, mAccelerometer, SensorManager.SENSOR_DELAY_UI );
        } catch (Exception e) {

            Toast.makeText ( getApplicationContext (), "Not Available",
                    Toast.LENGTH_SHORT ).show ();
        }

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate ( savedInstanceState );
        mDrawerToggle.syncState ();

    }

    @Override
    protected void onResume() {
        mSensorManager.registerListener ( mShakeDetector, mAccelerometer, SensorManager.SENSOR_DELAY_UI );
        super.onResume ();

        Intent connectIntent = new Intent ( this, PlumbleService.class );
        bindService ( connectIntent, mConnection, 0 );

    }

    @Override
    protected void onPause() {
        mSensorManager.unregisterListener ( mShakeDetector );
        super.onPause ();

        if (mErrorDialog != null)
            mErrorDialog.dismiss ();
        if (mConnectingDialog != null)
            mConnectingDialog.dismiss ();

        if (mService != null) {
            for (JumbleServiceFragment fragment : mServiceFragments) {
                fragment.setServiceBound ( false );
            }
            mService.unregisterObserver ( mObserver );
            mService.setSuppressNotifications ( false );
        }
        unbindService ( mConnection );
    }

    @Override
    protected void onDestroy() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences ( this );
        preferences.unregisterOnSharedPreferenceChangeListener ( this );
        mDatabase.close ();
        super.onDestroy ();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem disconnectButton = menu.findItem ( R.id.action_disconnect );
        MenuItem connectButton = menu.findItem ( R.id.action_connect );
        MenuItem torchSwitch = menu.findItem ( R.id.menu_turn_flash );
        connectButton.setVisible ( false );
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            if (isFlash == false) {
                torchSwitch.setVisible ( false );
            }
        } else if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            if (hasFlash () == false) {
                torchSwitch.setVisible ( true );
            }
        }
        disconnectButton.setVisible ( mService != null && mService.isConnected () );


        // Color the action bar icons to the primary text color of the theme.
        int foregroundColor = getSupportActionBar ().getThemedContext ()
                .obtainStyledAttributes ( new int[]{android.R.attr.textColor} )
                .getColor ( 0, -1 );
        for (int x = 0; x < menu.size (); x++) {
            MenuItem item = menu.getItem ( x );
            if (item.getIcon () != null) {
                Drawable icon = item.getIcon ().mutate (); // Mutate the icon so that the color filter is exclusive to the action bar
                icon.setColorFilter ( foregroundColor, PorterDuff.Mode.MULTIPLY );
            }
        }

        return super.onPrepareOptionsMenu ( menu );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater ().inflate ( R.menu.plumble, menu );
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected ( item ))
            return true;

        switch (item.getItemId ()) {
            case R.id.action_disconnect:
                fab.setVisibility ( View.GONE );
                getService ().disconnect ();
                item.setTitle ( "connect" );
                return true;
            case R.id.action_settings:
                Bundle args = new Bundle ();
                args.putInt ( "someInt", 1 );
                args.putString ( "someString", "settings" );
                Fragment fragment = Fragment.instantiate ( this, settings.class.getName (), args );
                getSupportFragmentManager ().beginTransaction ()
                        .replace ( R.id.content_frame, fragment, "settings" )
                        .setTransition ( FragmentTransaction.TRANSIT_FRAGMENT_FADE )
                        .commit ();
                return true;
            case R.id.menu_turn_flash:

                if (isTorchOn == false) {


                    item.setIcon ( R.drawable.ic_flash_on );
                    isTorchOn = true;

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        try {

                            NoobCameraManager.getInstance ().turnOnFlash ();
                        } catch (Exception e) {
                            e.printStackTrace ();
                        }
                        updateStatus ();
                    } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                        try {
                            hasCamera ();
                            Camera.Parameters parameters = camera.getParameters ();
                            parameters.setFlashMode ( Camera.Parameters.FLASH_MODE_TORCH );
                            camera.setParameters ( parameters );
                            camera.startPreview ();
                        } catch (Exception ex) {

                        }

                    }

                } else {
                    item.setIcon ( R.drawable.ic_flash_off );
                    isTorchOn = false;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        try {
                            NoobCameraManager.getInstance ().turnOffFlash ();
                        } catch (Exception e) {
                            e.printStackTrace ();
                        }
                        updateStatus ();

                    }
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {

                    }
                    try {

                        releaseCam ();
                    } catch (Exception ex) {

                    }

                }

                return true;
        }

        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged ( newConfig );
        mDrawerToggle.onConfigurationChanged ( newConfig );
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (mService != null && keyCode == mSettings.getPushToTalkKey ()) {
            mService.onTalkKeyDown ();
            return true;
        }
        return super.onKeyDown ( keyCode, event );
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (mService != null && keyCode == mSettings.getPushToTalkKey ()) {
            mService.onTalkKeyUp ();
            return true;
        }
        return super.onKeyUp ( keyCode, event );
    }

    @Override
    public void onBackPressed() {

        if (mService != null && mService.isConnected ()) {
            mDisconnectPromptBuilder.show ();
            return;
        }

        super.onBackPressed ();
    }

    @Override
    public void onItemClick(AdapterView <?> parent, View view, int position, long id) {
        mDrawerLayout.closeDrawers ();
        try {
            loadDrawerFragment ( (int) id );
        } catch (Exception e) {
            e.printStackTrace ();
        }
    }

    /**
     * Shows a nice looking setup wizard to guide the user through the app's settings.
     * Will do nothing if it isn't the first launch.
     */
    private void showSetupWizard() {
        // Prompt the user to generate a certificate.
        if (mSettings.isUsingCertificate ()) return;
        AlertDialog.Builder adb = new AlertDialog.Builder ( this );
        adb.setTitle ( R.string.first_run_generate_certificate_title );
        adb.setMessage ( R.string.first_run_generate_certificate );
        adb.setPositiveButton ( R.string.generate, new DialogInterface.OnClickListener () {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                @SuppressLint("StaticFieldLeak") PlumbleCertificateGenerateTask generateTask = new PlumbleCertificateGenerateTask ( PlumbleActivity.this ) {
                    @Override
                    protected void onPostExecute(DatabaseCertificate result) {
                        super.onPostExecute ( result );
                        if (result != null) mSettings.setDefaultCertificateId ( result.getId () );
                    }
                };
                generateTask.execute ();
            }
        } );
        //adb.show();
        mSettings.setFirstRun ( false );

        // TODO: finish wizard
//        Intent intent = new Intent(this, WizardActivity.class);
//        startActivity(intent);
    }

    /**
     * Loads a fragment from the drawer.
     */
    private void loadDrawerFragment(int fragmentId) {
        Class <? extends Fragment> fragmentClass = null;
        Bundle args = new Bundle ();
        switch (fragmentId) {

            case DrawerAdapter.ITEM_BLANK:
                //db.updatefreeApp ( new freeApp ( 1,"false","false" ) );
                fragmentClass = SignalEvents.class;
                break;
            case DrawerAdapter.ITEM_SERVER:
                //device_connect();
                fragmentClass = ChannelFragment.class;
                break;
            case DrawerAdapter.ITEM_INFO:
                // fragmentClass = ServerInfoFragment.class;
                Intent qr2 = new Intent ( this, AlertsFree.class );
                startActivityForResult ( qr2, REQUEST_CODE_QR_SCAN );
                finish ();
                break;

            case DrawerAdapter.ITEM_ACCESS_TOKENS:
                fragmentClass = AccessTokenFragment.class;
                Server connectedServer = getService ().getTargetServer ();
                args.putLong ( "server", connectedServer.getId () );
                args.putStringArrayList ( "access_tokens", (ArrayList <String>) mDatabase.getAccessTokens ( connectedServer.getId () ) );
                break;
            case DrawerAdapter.ITEM_PINNED_CHANNELS:
                fragmentClass = ChannelFragment.class;
                args.putBoolean ( "pinned", true );
                break;
            case DrawerAdapter.ITEM_FAVOURITES:
                fragmentClass = SignalEvents.class;
                //fragmentClass = FavouriteServerListFragment.class;
                break;
            case DrawerAdapter.ITEM_PUBLIC:
                fragmentClass = PublicServerListFragment.class;
                break;
            case DrawerAdapter.ITEM_INSTRUCT:
                fragmentClass = InstructionsFragment.class;
                break;
            case DrawerAdapter.ITEM_EXIT:

                finish ();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    finishAffinity ();
                }
                System.exit ( 0 );
                break;
            case DrawerAdapter.ITEM_MANAGE:
                fragmentClass = ManageLists.class;
                break;
            case DrawerAdapter.ITEM_TESTMODE:
                //fragmentClass = TestModeFrag.class;
                fragmentClass = SignalEvents.class;
                break;
            case DrawerAdapter.ITEM_QRCODES:
                fragmentClass = Barcodes.class;

                break;
            case DrawerAdapter.ITEM_PUSHTOTALK:
                //fragmentClass = TestModeFrag.class;
                fragmentClass = ChannelFragment.class;
                break;
            case DrawerAdapter.ITEM_SETTINGS:
                fragmentClass = ProfileSettings.class;
                //Intent prefIntent = new Intent(this, Preferences.class);
                //startActivity(prefIntent);
                break;

            case DrawerAdapter.ITEM_MAGALERT:
                Intent qr1 = new Intent ( this, AlertsFree.class );
                startActivityForResult ( qr1, REQUEST_CODE_QR_SCAN );
                return;

                  /*
            case com.magtouch.bluepass.app.DrawerAdapter.ITEM_PUSHTOTALK:
              ;
                loadDrawerFragment(com.magtouch.bluepass.app.DrawerAdapter.ITEM_FAVOURITES);

                return;

            case com.magtouch.bluepass.app.DrawerAdapter.ITEM_TRIGGER_EVENTS:
                fragmentClass = TriggerEvents.class;
                break;
                //Intent trigger = new Intent(this, com.blikoon.qrcodescanner.QrCodeActivity.class);
                //startActivityForResult (trigger, REQUEST_CODE_QR_SCAN); */


            default:
                return;
        }
        Fragment fragment = Fragment.instantiate ( this, fragmentClass.getName (), args );
        getSupportFragmentManager ().beginTransaction ()
                .replace ( R.id.content_frame, fragment, fragmentClass.getName () )
                .setTransition ( FragmentTransaction.TRANSIT_FRAGMENT_FADE )
                .commit ();
        //setTitle(mDrawerAdapter.getItemWithId(fragmentId).title);
    }

    public void connectToServer(final Server server) {

        // Check if we're already connected to a server; if so, inform user.
        if (mService != null && mService.isConnected ()) {
            fab.setVisibility ( View.VISIBLE );
            AlertDialog.Builder adb = new AlertDialog.Builder ( this );
            adb.setMessage ( R.string.reconnect_dialog_message );

            adb.setPositiveButton ( R.string.connect, new DialogInterface.OnClickListener () {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // Register an observer to reconnect to the new server once disconnected.
                    mService.registerObserver ( new JumbleObserver () {
                        @Override
                        public void onDisconnected(JumbleException e) {
                            connectToServer ( server );
                            mService.unregisterObserver ( this );
                        }
                    } );
                    mService.disconnect ();
                }
            } );
            adb.setNegativeButton ( android.R.string.cancel, null );
            adb.show ();
            return;
        }

        // Prompt to start Orbot if enabled but not running
        // TODO(acomminos): possibly detect onion address before connecting?
        if (mSettings.isTorEnabled ()) {
            if (!OrbotHelper.isOrbotRunning ( this )) {
                OrbotHelper.requestShowOrbotStart ( this );
                return;
            }
        }


        try {
            com.magtouch.bluepass.app.ServerConnectTask connectTask = new com.magtouch.bluepass.app.ServerConnectTask ( this, mDatabase );
            connectTask.execute ( server );
        } catch (Exception e) {
            e.printStackTrace ();

        }
    }

    public void connectToPublicServer(final PublicServer server) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder ( this );

        final Settings settings = Settings.getInstance ( this );

        // Allow username entry
        final EditText usernameField = new EditText ( this );
        usernameField.setHint ( settings.getDefaultUsername () );
        alertBuilder.setView ( usernameField );

        alertBuilder.setTitle ( R.string.connectToServer );

        alertBuilder.setPositiveButton ( R.string.connect, new DialogInterface.OnClickListener () {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                PublicServer newServer = server;
                if (!usernameField.getText ().toString ().equals ( "" ))
                    newServer.setUsername ( usernameField.getText ().toString () );
                else
                    newServer.setUsername ( settings.getDefaultUsername () );
                connectToServer ( newServer );
            }
        } );

        alertBuilder.show ();
    }

    private void setStayAwake(boolean stayAwake) {
        if (stayAwake) {
            getWindow ().addFlags ( WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON );
        } else {
            getWindow ().clearFlags ( WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON );
        }
    }

    /**
     * Updates the activity to represent the connection state of the given service.
     * Will show reconnecting dialog if reconnecting, dismiss otherwise, etc.
     * Basically, this service will do catch-up if the activity wasn't bound to receive
     * connection state updates.
     *
     * @param service A bound IJumbleService.
     */
    @SuppressLint("StringFormatMatches")
    private void updateConnectionState(IJumbleService service) {
        magService = service;
        if (mConnectingDialog != null)
            mConnectingDialog.dismiss ();
        if (mErrorDialog != null)
            mErrorDialog.dismiss ();

        switch (mService.getConnectionState ()) {

            case CONNECTED:

                final Handler handler = new Handler ();
                handler.postDelayed ( new Runnable () {
                    @Override
                    public void run() {
                        //magService.getSession ().joinChannel ( 4);
                        mRootChannels = new ArrayList<Integer>();
                        if(showPinnedOnly) {
                            mRootChannels = mDatabase.getPinnedChannels(magService.getTargetServer().getId());
                        } else {
                            mRootChannels.add(0);
                        }

                        // Construct channel tree
                        mNodes = new LinkedList<PlumbleActivity.Node> ();
                       mExpandedChannels = new HashMap<Integer, Boolean> ();
                       updateChannels();

                    }
                }, 1000 );
                break;

            case CONNECTING:
                Server server = service.getTargetServer ();
                mConnectingDialog = new ProgressDialog ( this );
                mConnectingDialog.setIndeterminate ( true );
                mConnectingDialog.setCancelable ( true );
                mConnectingDialog.setOnCancelListener ( new DialogInterface.OnCancelListener () {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        mService.disconnect ();
                        Toast.makeText ( PlumbleActivity.this, R.string.cancelled,
                                Toast.LENGTH_SHORT ).show ();
                    }
                } );
                mConnectingDialog.setMessage ( getString ( R.string.connecting_to_server, server.getHost (),
                        server.getPort () ) );
                fab.setVisibility ( View.VISIBLE );
                mConnectingDialog.show ();
                break;
            case CONNECTION_LOST:
              /*
                try {
                    mService.disconnect ();
                } catch (Exception ex) {
                }
                // Only bother the user if the error hasn't already been shown.
                connectToServer ( new Server ( servername, serverhost, serverport, serveruser, serverpass ) );
                */

                fab.setVisibility ( View.GONE );
                if (!getService().isErrorShown()) {
                    JumbleException error = getService().getConnectionError();
                    AlertDialog.Builder ab = new AlertDialog.Builder(PlumbleActivity.this);
                    ab.setTitle(R.string.connectionRefused);
                    if (mService.isReconnecting()) {
                       try{ab.setMessage(getString(R.string.attempting_reconnect, error.getMessage()));}catch(Exception ex){}


                        ab.setPositiveButton(R.string.cancel_reconnect, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (getService() != null) {
                                    getService().cancelReconnect();
                                    getService().markErrorShown();
                                }
                            }
                        });

                    } else if (error.getReason() == JumbleException.JumbleDisconnectReason.REJECT &&
                               (error.getReject().getType() == Mumble.Reject.RejectType.WrongUserPW ||
                                error.getReject().getType() == Mumble.Reject.RejectType.WrongServerPW)) {
                        // FIXME(acomminos): Long conditional.
                        final EditText passwordField = new EditText(this);
                        passwordField.setInputType(InputType.TYPE_CLASS_TEXT |
                                InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        passwordField.setHint(R.string.password);
                        ab.setTitle(R.string.invalid_password);
                        ab.setMessage(error.getMessage());
                        ab.setView(passwordField);
                        ab.setPositiveButton(R.string.reconnect, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Server server = getService().getTargetServer();
                                if (server == null)
                                    return;
                                String password = passwordField.getText().toString();
                                server.setPassword(password);
                                if (server.isSaved())
                                    mDatabase.updateServer(server);
                                connectToServer(server);
                            }
                        });
                        ab.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (getService() != null)
                                    getService().markErrorShown();
                            }
                        });
                    } else {
                        ab.setMessage(error.getMessage());
                        ab.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (getService() != null)
                                    getService().markErrorShown();
                            }
                        });
                    }
                    ab.setCancelable(false);
                    mErrorDialog = ab.show();
                }


                break;


        }
    }

    /*
     * HERE BE IMPLEMENTATIONS
     */

    @Override
    public IPlumbleService getService() {
        return mService;
    }

    @Override
    public PlumbleDatabase getDatabase() {
        return mDatabase;
    }

    @Override
    public void addServiceFragment(JumbleServiceFragment fragment) {
        mServiceFragments.add ( fragment );
    }

    @Override
    public void removeServiceFragment(JumbleServiceFragment fragment) {
        mServiceFragments.remove ( fragment );
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (Settings.PREF_THEME.equals ( key )) {
            // Recreate activity when theme is changed
            if (Build.VERSION.SDK_INT >= 11)
                recreate ();
            else {
                Intent intent = new Intent ( this, PlumbleActivity.class );
                finish ();
                startActivity ( intent );
            }
        } else if (Settings.PREF_STAY_AWAKE.equals ( key )) {
            setStayAwake ( mSettings.shouldStayAwake () );
        } else if (Settings.PREF_HANDSET_MODE.equals ( key )) {
            setVolumeControlStream ( mSettings.isHandsetMode () ?
                    AudioManager.STREAM_VOICE_CALL : AudioManager.STREAM_MUSIC );
        }
    }

    @Override
    public boolean isConnected() {

        return mService != null && mService.isConnected ();
    }

    @Override
    public String getConnectedServerName() {
        if (mService != null && mService.isConnected ()) {
            Server server = mService.getTargetServer ();
            return server.getName ().equals ( "" ) ? server.getHost () : server.getName ();
        }
        if (BuildConfig.DEBUG)
            throw new RuntimeException ( "getConnectedServerName should only be called if connected!" );
        return "";
    }

    @Override
    public void onServerEdited(ServerEditFragment.Action action, Server server) {
        switch (action) {
            case ADD_ACTION:
                mDatabase.addServer ( server );
                loadDrawerFragment ( com.magtouch.bluepass.app.DrawerAdapter.ITEM_FAVOURITES );
                break;
            case EDIT_ACTION:
                mDatabase.updateServer ( server );
                loadDrawerFragment ( com.magtouch.bluepass.app.DrawerAdapter.ITEM_FAVOURITES );
                break;
            case CONNECT_ACTION:
                connectToServer ( server );
                break;
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static AlertActivity.PlaceholderFragment newInstance(int sectionNumber) {
            AlertActivity.PlaceholderFragment fragment = new AlertActivity.PlaceholderFragment ();
            Bundle args = new Bundle ();
            args.putInt ( ARG_SECTION_NUMBER, sectionNumber );
            fragment.setArguments ( args );
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate ( R.layout.fragment_alert, container, false );
            TextView textView = (TextView) rootView.findViewById ( R.id.section_label );
            textView.setText ( getString ( R.string.section_format, getArguments ().getInt ( ARG_SECTION_NUMBER ) ) );
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super ( fm );
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return AlertActivity.PlaceholderFragment.newInstance ( position + 1 );
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }
    }
/*
public void gaulo(int number){
        switch(number){
            case 0:
                case 1:
                case 2:
                loadDrawerFragment(com.magtouch.bluepass.app.DrawerAdapter.ITEM_FAVOURITES);
                Toast.makeText ( getBaseContext (),"Favourites", Toast.LENGTH_LONG ).show ();
                break;
            case 3:
                loadDrawerFragment( DrawerAdapter.ITEM_TRIGGER_EVENTS);
                Toast.makeText ( getBaseContext (),"Trigger Events", Toast.LENGTH_LONG ).show ();
                break;
        }

}
*/

    /**
     * The following method is a test method to launch fragment activities
     */
    public void testApp(int item) {
        Class <? extends Fragment> fragmentClass = null;
        Bundle args = new Bundle ();

        switch (item) {
            case 0:
                device_connect ();
                fragmentClass = ChannelFragment.class;

                break;
            case 1:

                //alertStatus ();
                Intent alertsFree = new Intent ( this, AlertsFree.class );
                startActivity ( alertsFree );
                finish ();
                break;
            case 2:
                Intent i = new Intent ( this, QrCodeActivity.class );
                startActivityForResult ( i, REQUEST_CODE_QR_SCAN );
                break;
            case 3:
                //fragmentClass = TriggerEvents.class;
                fragmentClass = SignalEvents.class;
                break;
        }
        //fragmentClass = AlertFragment.class;
        Fragment fragment = Fragment.instantiate ( this, fragmentClass.getName (), args );
        getSupportFragmentManager ().beginTransaction ()
                .replace ( R.id.content_frame, fragment, fragmentClass.getName () )
                .setTransition ( FragmentTransaction.TRANSIT_FRAGMENT_FADE )
                .commit ();
    }

    public void showToast(View view) {
        // Set the toast and duration
        int toastDurationInMilliSeconds = 10000;
        mToastToShow = Toast.makeText ( this, "Talking....", Toast.LENGTH_LONG );

        // Set the countdown to display the toast
        CountDownTimer toastCountDown;
        toastCountDown = new CountDownTimer ( toastDurationInMilliSeconds, 1000 /*Tick duration*/ ) {
            public void onTick(long millisUntilFinished) {
                mToastToShow.show ();
            }

            public void onFinish() {
                mToastToShow.cancel ();
            }
        };

        // Show the toast and starts the countdown
        mToastToShow.show ();
        toastCountDown.start ();
    }

    private boolean hasCameraPermission() {
        PackageManager pm = getPackageManager ();
        return PackageManager.PERMISSION_GRANTED == pm.checkPermission ( "android.permission.CAMERA", getPackageName () );
    }

    /**
     * This function is called every time the device is loading...
     */
    public void device_connect() {
        Serial serial = db.getSerial ( 1 );
        String magcell_code = serial.getIMEI ();
        if(Constants.app_name=="onlineguarding_bluepass")
        {
            server_url = Constants.connect_url.trim () + hashedKey.trim () + "/" + md5 ( IMEI_Number_Holder ) + "/" + magcell_code;
        }
        else{
            server_url = Constants.device_authenticate.trim () + hashedKey.trim () + "/" + md5 ( IMEI_Number_Holder ) + "/" + magcell_code;
        }


       //Toast.makeText ( getBaseContext (),server_url,Toast.LENGTH_LONG ).show ();
        StringRequest stringRequest = new StringRequest ( Request.Method.GET, server_url, new Response.Listener <String> () {
            @Override
            public void onResponse(String response) {
               //Toast.makeText ( getBaseContext (), response,Toast.LENGTH_LONG ).show ();
                try {

                    JSONArray jsonArray = null;
                    jsonArray = new JSONArray ( response );
                    JSONObject jsonObject;
                    for (int i = 0; i < jsonArray.length (); i++) {
                        jsonObject = jsonArray.getJSONObject ( i );
                        serverhost = jsonObject.getString ( "server_ip" );
                        servername = jsonObject.getString ( "server_name" );
                        serverport = jsonObject.getInt ( "server_port" );
                        serverpass = "";
                        //serveruser=jsonObject.getString("distributor_name");
                        //String usercompany=serveruser=jsonObject.getString("distributor_name");
                        //String ExpiryDate=jsonObject.getString("expiry");
                        // String userphone=jsonObject.getString("distributor_phone");
                        //String email= jsonObject.getString ("email");
                        //String AccountID=jsonObject.getString ("device_id");
                        String usercompany = serveruser = jsonObject.getString ( "firstname" );
                        String ExpiryDate = jsonObject.getString ( "exp_date" );
                        String userphone = jsonObject.getString ( "cellnumber" );
                        //String pttmodule=jsonObject.getString("ptt");
                        String pttmodule = module_status ( jsonObject.getString ( "ptt" ) );
                        String smsmodule = module_status ( jsonObject.getString ( "sms" ) );
                        String alerts = module_status ( jsonObject.getString ( "alerts" ) );
                        String channel_name = jsonObject.getString ( "channel" );
                        imageUpdate ( jsonObject.getString ( "client_id" ) );
                        db.updateUserInfo ( new user_info ( 1, usercompany, ExpiryDate, userphone, pttmodule, smsmodule, alerts, channel_name ) );

                        connectToServer ( new Server ( servername, serverhost, serverport, serveruser, serverpass ) );


                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder ( PlumbleActivity.this ).create ();
                    alertDialog.setTitle ( "Setting Not Found" );
                    alertDialog.setMessage ( " Settings for your device were not found on this server please check your internet setting first, otherwise contact your service provider" );

                    alertDialog.setButton ( android.support.v7.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener () {
                                public void onClick(DialogInterface dialog, int which) {
                                    /*
                                    Intent intent = new Intent ( getApplicationContext (), AlertsFree.class );
                                    startActivity ( intent );
                                    */
                                    dialog.dismiss ();
                                }
                            } );
                    alertDialog.show ();

                    e.printStackTrace ();
                }


            }
        }, new Response.ErrorListener () {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText ( getBaseContext (), "Failed to communicate with the server", Toast.LENGTH_LONG ).show ();
                error.printStackTrace ();

            }
        } );

        MySingleton.getInstance ( PlumbleActivity.this ).addToRequestQue ( stringRequest );

    }

    private void getDeviceImei() {

        if (ActivityCompat.checkSelfPermission ( this, Manifest.permission.READ_PHONE_STATE ) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        IMEI_Number_Holder = telephonyManager.getDeviceId ();

    }

    public void onPermissionsClick(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                (ActivityCompat.checkSelfPermission ( this, Manifest.permission.CAMERA ) == PackageManager.PERMISSION_GRANTED)) {
            String[] permissions = {Manifest.permission.CAMERA};
            requestPermissions ( permissions, 1337 );
        }
        updateStatus ();
    }

    public void onFlashOnClick(View view) {
        try {
            NoobCameraManager.getInstance ().turnOnFlash ();
        } catch (Exception e) {
            e.printStackTrace ();
        }
        updateStatus ();
    }

    public void onFlashOffClick(View view) {
        try {
            NoobCameraManager.getInstance ().turnOffFlash ();
        } catch (Exception e) {
            e.printStackTrace ();
        }
        updateStatus ();
    }

    public void onFlashToggleClick(View view) {
        try {
            NoobCameraManager.getInstance ().toggleFlash ();
        } catch (Exception e) {
            e.printStackTrace ();
        }
        updateStatus ();
    }

    public void onReleaseClick(View view) {
        NoobCameraManager.getInstance ().release ();
        updateStatus ();
    }

    private void updateStatus() {
        if (NoobCameraManager.getInstance ().isFlashOn ()) {

        } else {

        }
    }

    public static void alertStatus() {
        alertStats = "alert";

    }

    public void hasCamera() {

        if (getApplicationContext ().getPackageManager ().hasSystemFeature ( PackageManager.FEATURE_CAMERA_FLASH )) {

            camera = Camera.open ();
            parameters = camera.getParameters ();
            isFlash = true;
        } else {
            isFlash = false;
        }

    }

    public boolean hasFlash() {
        if (camera == null) {
            return false;
        }

        Camera.Parameters parameters = camera.getParameters ();

        if (parameters.getFlashMode () == null) {
            return false;
        }

        List <String> supportedFlashModes = parameters.getSupportedFlashModes ();
        if (supportedFlashModes == null || supportedFlashModes.isEmpty () || supportedFlashModes.size () == 1 && supportedFlashModes.get ( 0 ).equals ( Camera.Parameters.FLASH_MODE_OFF )) {
            return false;
        }

        return true;
    }

    public void releaseCam() {


        if (camera != null) {
            parameters.setFlashMode ( Camera.Parameters.FLASH_MODE_OFF );
            camera.setParameters ( parameters );
            camera.stopPreview ();
            camera.release ();
            camera = null;
        }
    }

    public static final String md5(final String toEncrypt) {
        try {
            final MessageDigest digest = MessageDigest.getInstance ( "md5" );
            digest.update ( toEncrypt.getBytes () );
            final byte[] bytes = digest.digest ();
            final StringBuilder sb = new StringBuilder ();
            for (int i = 0; i < bytes.length; i++) {
                sb.append ( String.format ( "%02X", bytes[i] ) );
            }
            return sb.toString ().toUpperCase ();
        } catch (Exception exc) {
            return "";
        }
    }

    public void imageUpdate(final String distributor_id) {

        final String server_url = Constants.image_request + distributor_id;
        StringRequest stringRequest = new StringRequest ( Request.Method.GET, server_url, new Response.Listener <String> () {
            @Override
            public void onResponse(String response) {
                db.updateLogo ( new imageLogo ( 1, response ) );


            }
        }, new Response.ErrorListener () {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText ( PlumbleActivity.this, "could not fetch company image", Toast.LENGTH_SHORT ).show ();
                error.printStackTrace ();
            }
        } );
        MySingleton.getInstance ( PlumbleActivity.this ).addToRequestQue ( stringRequest );


    }

    public String panicMessage() {
        Messages messages = db.getMessage ( 1 );
        theMessage = messages.getMessage ();
        String panicMessage = theMessage + " http://maps.google.com/maps?q=" + latitude + "," + longitude;
        db.close ();
        return panicMessage;
    }

    /**
     * The method receives two parameters, a number and message then send the message
     *
     * @param phoneNo
     * @param msg
     */
    public void sendSMS(String phoneNo, String msg) {

        try {
            SmsManager smsManager = SmsManager.getDefault ();
            smsManager.sendTextMessage ( phoneNo, null, msg, null, null );
            Toast.makeText ( getApplicationContext (), "Message Sent",
                    Toast.LENGTH_SHORT ).show ();
        } catch (Exception ex) {
            Toast.makeText ( getApplicationContext (), ex.getMessage (),
                    Toast.LENGTH_SHORT ).show ();
            ex.printStackTrace ();
        }

    }


    public String module_status(String module) {
        String status = "Not Active";
        if (module.equals ( "1" )) {
            status = "Active";
        } else {
            status = "Not Active";
        }

        return status;
    }



    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService ( Context.CONNECTIVITY_SERVICE );
        NetworkInfo[] netInfo = cm.getAllNetworkInfo ();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName ().equalsIgnoreCase ( "WIFI" ))
                if (ni.isConnected ())
                    haveConnectedWifi = true;
            if (ni.getTypeName ().equalsIgnoreCase ( "MOBILE" ))
                if (ni.isConnected ())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }


    }
