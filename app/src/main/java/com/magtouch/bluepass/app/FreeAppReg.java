package com.magtouch.bluepass.app;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.magtouch.bluepass.R;
import com.magtouch.bluepass.db.PlumbleSQLiteDatabase;

import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class FreeAppReg extends AppCompatActivity implements LocationListener {
public PlumbleSQLiteDatabase db;
    String server_url="http://www.rfnet.co.za/fa/API/setup_data";
    private String text = "";
    private String log = "";
    public TelephonyManager telephonyManager;
    public String IMEI_Number_Holder;
    public String possibleEmail = "";
    public String Gmail = "test@test.com";
    public String Group = "Test@test.com";
    private static final int PERMISSIONS_REQUEST_READ_PHONE_STATE = 999;
    private static final int PERMISSIONS_REQUEST_GET_ACCOUNTS = 666;
    private TelephonyManager mTelephonyManager;
    public Double latitude=0.0;
    public Double longitude=0.0;
    private LocationManager locationManager;
    private Criteria criteria;
    private static final int PERMISSIONS_READ_LOCATION = 100;
    public String  strGmail="";

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        final EditText userName = findViewById(R.id.editName);
        final EditText userPhone = findViewById(R.id.editPhone);
        final EditText userEmail = findViewById(R.id.editEmail);
        Button button = findViewById(R.id.button3);
        db =new PlumbleSQLiteDatabase ( getBaseContext () );
        telephonyManager = (TelephonyManager) this.getSystemService( Context.TELEPHONY_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.GET_ACCOUNTS},
                        PERMISSIONS_REQUEST_GET_ACCOUNTS);
            }


            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }


        //

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            int permissionCheck = ContextCompat.checkSelfPermission(getBaseContext (),
                    Manifest.permission.CAMERA);
            if (permissionCheck == PackageManager.PERMISSION_GRANTED)
            {
                //showing dialog to select image
                String possibleEmail=null;

                Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
                Account[] accounts = AccountManager.get(getBaseContext ()).getAccounts();
                for (Account account : accounts) {
                    if (emailPattern.matcher(account.name).matches()) {
                        possibleEmail = account.name;
                        strGmail=possibleEmail;
                        userEmail.setText(possibleEmail);
                    }
                }



            } else {                        ActivityCompat.requestPermissions(getParent (),
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.READ_PHONE_STATE,
                            Manifest.permission.GET_ACCOUNTS,
                            Manifest.permission.CAMERA}, 1);
            }
        } else {

            String possibleEmail=null;

            Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
            Account[] accounts = AccountManager.get(FreeAppReg.this).getAccounts();
            for (Account account : accounts) {
                if (emailPattern.matcher(account.name).matches()) {
                    possibleEmail = account.name;
                    strGmail=possibleEmail;
                    userEmail.setText(possibleEmail);
                }




            }
        }
        button.setOnClickListener(
                new Button.OnClickListener() {


                    public void onClick(View v) {


                        final String fullName = userName.getText().toString();
                        final String PhoneNumber = userPhone.getText().toString();
                        final String Email = userEmail.getText().toString();
                        text = fullName + " " + PhoneNumber + " " + Email + "\n";


                        StringRequest stringRequest=new StringRequest( Request.Method.POST, server_url, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Toast.makeText(getBaseContext (), "Record Saved", Toast.LENGTH_SHORT).show();

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getBaseContext (), "error", Toast.LENGTH_SHORT).show();
                                error.printStackTrace();
                            }
                        }){

                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                String the_location=latitude + ", " + longitude;
                                Map<String,String> params = new HashMap<> ();
                                // params.put("name", name);
                                //params.put("email", email);
                                params.put("imei", md5(IMEI_Number_Holder));
                                params.put("real_email", strGmail);
                                params.put("user_email", Email);
                                params.put("name", fullName);
                                params.put("number", PhoneNumber);
                                params.put("contacts", "1,2,3,4");
                                params.put("gps", the_location);
                                params.put("X-API-KEY", "boguskey");
                                params.put("contactPostFormat", "");
                                params.put("csvDelimiter", "");
                                params.put("namePairDelimiter", "");

                                return params;
                            }
                        };
                        MySingleton.getInstance(getBaseContext ()).addToRequestQue(stringRequest);
                        register(IMEI_Number_Holder, fullName, PhoneNumber, Email, Gmail, Group);


                    }

                }
        );
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE},
                        PERMISSIONS_REQUEST_READ_PHONE_STATE);
            }

            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        else {
            getDeviceImei();

        }



        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        // Define the criteria how to select the location provider
        criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_COARSE);   //default
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestUpdates();
        }

    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void requestUpdates() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_READ_LOCATION);
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                2000, 1, this);
    }

    public void register(String IMEI, String fullName, String PhoneNumber, String Email, String Gmail, String Group) {


        db.addOwner(new OwnerList(IMEI, fullName, PhoneNumber, Email, Gmail, Group));
        /*String query = "INSERT INTO OwnerTable(imei,owner_name,owner_phone,owner_email,owner_gmail,owner_group) values ('"
                + IMEI + "','" + fullName + "','" + PhoneNumber + "','" + Email + "','" + Gmail + "','" + Group + "')";
        sqlHandler.executeQuery(query);*/
        db.updateTerms(new Terms_Conditions(1, "true"));



        Toast.makeText(getApplicationContext(), possibleEmail + "Record to Database Saved" + IMEI_Number_Holder,
                Toast.LENGTH_LONG).show();
        // Refresh main activity upon close of dialog box
        Intent refresh = new Intent(this, AlertsFree.class);
        startActivity(refresh);
        finish();

    }


    private void getDeviceImei() {

        mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        IMEI_Number_Holder = mTelephonyManager.getDeviceId();

    }

    @Override
    public void onPause() {
        // Add the following line to unregister the Sensor Manager onPause
        super.onPause();

        //locationManager.removeUpdates(this);
    }
    @Override
    public void onLocationChanged(Location location) {
        String msg = "" + location.getLatitude()
                + ", " + location.getLongitude();
        Toast.makeText(getApplicationContext(), msg,
                Toast.LENGTH_SHORT).show();
        latitude=location.getLatitude();
        longitude=location.getLongitude();
    }




    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_PHONE_STATE
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            getDeviceImei();
        }
    }




    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

        Toast.makeText(getBaseContext(), "Gps is turned on!! ",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderDisabled(String provider) {

        Toast.makeText(getBaseContext(), "Gps is turned off!! ",
                Toast.LENGTH_SHORT).show();
    }

    public static final String md5(final String toEncrypt) {
        try {
            final MessageDigest digest = MessageDigest.getInstance("md5");
            digest.update(toEncrypt.getBytes());
            final byte[] bytes = digest.digest();
            final StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(String.format("%02X", bytes[i]));
            }
            return sb.toString().toUpperCase();
        } catch (Exception exc) {
            return "";
        }
    }
}
