package com.magtouch.bluepass.app;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.magtouch.bluepass.R;
import com.magtouch.bluepass.db.PlumbleSQLiteDatabase;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CustomMessage extends Fragment {

    public Button buttonSend;
    public EditText textPhoneNo;
    public String[] gaulo;
    public String log="";
    public EditText textSMS;
    public Button saveMessage;

    public PlumbleSQLiteDatabase db ;
    public   Messages messages;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        db=new PlumbleSQLiteDatabase(getActivity ());
        View view =inflater.inflate ( R.layout.fragment_custom_message, container, false );
        saveMessage =view.findViewById ( R.id.saveMsg );
        textSMS=view.findViewById ( R.id.customLong );
        buttonSend = view.findViewById(R.id.sendcustom);
        ViewRecord();

        buttonSend.setOnClickListener(new View.OnClickListener () {

            @Override
            public void onClick(View v) {

                ViewAllContacts();
                String sms = textSMS.getText().toString();
/*
                for (int i = 0; i < gaulo.length; i++) {
                    sendSMS(gaulo[i], sms);
                }
                */
            }
        });
        saveMessage.setOnClickListener(new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                String Message = textSMS.getText().toString();
                db.updateMessages(new Messages(1, Message));
                db.close();
                Toast.makeText(getActivity (), "Record Saved",
                        Toast.LENGTH_SHORT).show();

            }
        });
        // Inflate the layout for this fragment

        return  view;
    }


    public void ViewAllContacts() {
        List<Contact> contacts = db.getAllContacts();
        for (Contact cn : contacts) {
            log = log + cn.getName().trim() + " ";
        }
        gaulo =log.split(" ");
        db.close();
    }


    public void ViewRecord() {
        try{
            messages = db.getMessage(1);
            textSMS.setText(messages.getMessage());
        }catch(Exception e){

        }

        db.close();

    }
    public void sendSMS(String phoneNo, String msg) {
        if(AlertsFree.isSwitchOn){
            Toast.makeText(getActivity (), "Cant send message in Test mode",
                    Toast.LENGTH_SHORT).show();
        }
        else {
            try {
                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(phoneNo, null, msg, null, null);
                Toast.makeText(getActivity (), "Message Sent",
                        Toast.LENGTH_SHORT).show();
            } catch (Exception ex) {
                Toast.makeText(getActivity (), ex.getMessage(),
                        Toast.LENGTH_SHORT).show();
                ex.printStackTrace();
            }
        }
    }


}
