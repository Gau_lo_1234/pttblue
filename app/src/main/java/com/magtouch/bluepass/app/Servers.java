package com.magtouch.bluepass.app;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.blikoon.qrcodescanner.QrCodeActivity;
import com.magtouch.jumble.model.Server;
import com.magtouch.bluepass.db.PlumbleDatabase;
import com.magtouch.bluepass.R;
import com.magtouch.bluepass.db.PlumbleSQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Servers extends Activity {
    private Button button;
    private TextView txtSkip;
    private ImageView scan_now;
    private static final int REQUEST_CODE_QR_SCAN = 101;
    private static final int REQUEST_INTERNET = 200;
    private final String LOGTAG = "QRCScanner-MainActivity";
    public EditText qrcode;
    public Button qrbut;
    public String passkey;
    public String server_url="";
    private PlumbleDatabase mDatabase;
    public String IMEI_Number_Holder;
    public TelephonyManager telephonyManager;
    private static final int MY_PERMISSIONS_REQUEST_ACCOUNTS = 1;
    private static final int MY_PERMISSIONS_REQUEST_CAMERA=201;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_qr__registration );
        scan_now=(ImageView)  findViewById ( R.id.scan_now );
        qrcode=(EditText) findViewById ( R.id.qr_edit);
        qrbut=(Button) findViewById ( R.id.Activated );
        txtSkip=findViewById ( R.id.txtSkip );
        /*

         */
        txtSkip.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                PlumbleSQLiteDatabase rs = new PlumbleSQLiteDatabase (getBaseContext () );
                rs.updateTerms ( new Terms_Conditions ( 1, "true" ) );
                mDatabase.addPassCode ( new Pass_Code (1,"none","false"  ) );
                Intent free = new Intent ( getBaseContext (),AlertsFree.class );
                startActivity ( free );
            }
        } );
        checkAndRequestPermissions();
        telephonyManager = (TelephonyManager) this.getSystemService ( Context.TELEPHONY_SERVICE );
        mDatabase = new PlumbleSQLiteDatabase (this); // TODO add support for cloud storage
        mDatabase.open();
        getDeviceImei ();
        scan_now.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Servers.this,QrCodeActivity.class);
                startActivityForResult( i,REQUEST_CODE_QR_SCAN);
            }
        } );

        qrbut.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                Toast.makeText ( getBaseContext (),passkey,Toast.LENGTH_SHORT ).show ();
                mDatabase.addPassCode ( new Pass_Code (1,passkey,"true"  ) );
                device_authenticate();
                Intent home =new Intent(getBaseContext (), PlumbleActivity.class);
                startActivity(home);
                finish();
            }

        } );

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(resultCode != Activity.RESULT_OK)
        {
            Log.d(LOGTAG,"COULD NOT GET A GOOD RESULT.");
            if(data==null)
                return;
            //Getting the passed result
            String result = data.getStringExtra("com.blikoon.qrcodescanner.error_decoding_image");
            if( result!=null)
            {
                AlertDialog alertDialog = new AlertDialog.Builder(Servers.this).create();
                alertDialog.setTitle("Scan Error");
                alertDialog.setMessage("QR Code could not be scanned");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
            return;

        }
        if(requestCode == REQUEST_CODE_QR_SCAN)
        {
            if(data==null)
                return;
            //Getting the passed result
            String result = data.getStringExtra("com.blikoon.qrcodescanner.got_qr_scan_relult");
            Log.d(LOGTAG,"Have scan result in your app activity :"+ result);
            AlertDialog alertDialog = new AlertDialog.Builder(Servers.this).create();
            alertDialog.setTitle("Scan result");
            alertDialog.setMessage(result);
            qrcode.setText ( result );
            passkey=result;
            device_authenticate ();
            PlumbleSQLiteDatabase rs = new PlumbleSQLiteDatabase ( this );
            rs.updateTerms ( new Terms_Conditions ( 1, "true" ) );

            alertDialog.setButton( AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            // alertDialog.show();

        }
    }
    private void getDeviceImei() {

        if (ActivityCompat.checkSelfPermission ( this, Manifest.permission.READ_PHONE_STATE ) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        IMEI_Number_Holder = telephonyManager.getDeviceId ();

    }
    private boolean checkAndRequestPermissions() {
        int permissionCAMERA = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA);


        int storagePermission = ContextCompat.checkSelfPermission(this,


                Manifest.permission.READ_EXTERNAL_STORAGE);

        int smsPermission = ContextCompat.checkSelfPermission(this,


                Manifest.permission.READ_SMS);
        int sendPermission = ContextCompat.checkSelfPermission(this,


                Manifest.permission.SEND_SMS);
        int contactsPermission = ContextCompat.checkSelfPermission(this,


                Manifest.permission.READ_CONTACTS);
        int locationPermission = ContextCompat.checkSelfPermission(this,


                Manifest.permission.ACCESS_COARSE_LOCATION);
        int statePermission = ContextCompat.checkSelfPermission(this,


                Manifest.permission.READ_PHONE_STATE);

        int recordPermission = ContextCompat.checkSelfPermission(this,


                Manifest.permission.RECORD_AUDIO);
        int writePermission = ContextCompat.checkSelfPermission(this,

                Manifest.permission.WRITE_CONTACTS);




        List<String> listPermissionsNeeded = new ArrayList<> ();
        if (storagePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (smsPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_SMS);
        }
        if (writePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_CONTACTS);
        }
        if (recordPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECORD_AUDIO);
        }
        if (statePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
        }
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (contactsPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_CONTACTS);
        }
        if (sendPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.SEND_SMS);
        }
        if (permissionCAMERA != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,


                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MY_PERMISSIONS_REQUEST_CAMERA);
            return false;
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCOUNTS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    //Permission Granted Successfully. Write working code here.
                } else {
                    //You did not accept the request can not use the functionality.
                }
                break;
        }
    }

    public void device_authenticate(){
        String server_url = Constants.device_authenticate + passkey;
        StringRequest stringRequest = new StringRequest ( Request.Method.POST, server_url, new Response.Listener <String> () {
            @Override
            public void onResponse(String response) {

                android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder ( Servers.this ).create ();

                alertDialog.setTitle ( "Device Information" );
                alertDialog.setMessage ( response );

                alertDialog.setButton ( android.support.v7.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener () {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss ();
                            }
                        } );
                alertDialog.show ();



            }
        }, new Response.ErrorListener () {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText ( Servers.this, "error", Toast.LENGTH_SHORT ).show ();
                error.printStackTrace ();
            }
        } );
        MySingleton.getInstance ( Servers.this ).addToRequestQue ( stringRequest );

    }
}
