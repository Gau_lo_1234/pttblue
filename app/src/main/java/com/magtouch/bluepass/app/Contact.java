package com.magtouch.bluepass.app;

/**
 * Created by android on 2018/01/20.
 */

public class Contact {
    int id;
    public String name;
    public String phone_number;
    public String Email;
    public String IMEI;

    public Contact(){

    }

    public Contact( int id){

        this.id=id;
    }
    public Contact( String name){

        this.name=name;
    }
    public Contact(String name, String phone_number){

        this.name=name;
        this.phone_number=phone_number;
    }
    public Contact(String IMEI, String name, String phone_number, String Email){
        this.IMEI=IMEI;
        this.name=name;
        this.phone_number=phone_number;
        this.Email=Email;
    }
    public Contact(int id, String name, String phone_number){
        this.id=id;
        this.name=name;
        this.phone_number=phone_number;

    }

    public int getId() {
        return this.id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getIMEI(){ return this.IMEI; }
    public void setIMEI(String IMEI) {
        this.IMEI = IMEI;
    }

    public String getName() {
        return this.name;
    }

    public String getEmail() {
        return this.Email;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getPhone_number() {
        return this.phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }
}