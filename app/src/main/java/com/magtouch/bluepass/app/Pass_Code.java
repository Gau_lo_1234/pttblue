package com.magtouch.bluepass.app;

public class Pass_Code {
    private int id;
    private String pass_key;
    private String pass_state;


    public Pass_Code(){

    }
    public Pass_Code(int id){
        this.id=id;
    }

    public Pass_Code(int i, String passkey) {
        this.id=i;
        this.pass_key=passkey;
    }

    public Pass_Code(String passkey){

        this.pass_key=passkey;
    }
    public Pass_Code(String passkey, String passstate){

        this.pass_key=passkey;
        this.pass_state=passstate;
    }
    public Pass_Code(int id,String passkey, String passstate){
        this.id=id;
        this.pass_key=passkey;
        this.pass_state=passstate;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPass_key() {
        return this.pass_key;
    }

    public String getPass_state() {
        return this.pass_state;
    }

    public void setPass_key(String passkey) {
        this.pass_key = passkey;
    }
    public void setPass_state(String passstate) {
        this.pass_state = passstate;
    }
}
