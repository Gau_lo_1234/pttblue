package com.magtouch.bluepass.app;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.magtouch.bluepass.R;
import com.magtouch.bluepass.db.PlumbleSQLiteDatabase;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class Registration extends AppCompatActivity implements LocationListener {



    private String text = "";
    private String log = "";
    public TelephonyManager telephonyManager;
    public String IMEI_Number_Holder;
    private PlumbleSQLiteDatabase db;
    public String possibleEmail = "";
    public String Gmail = "test@test.com";
    public String Group = "Test@test.com";
    private static final int PERMISSIONS_REQUEST_READ_PHONE_STATE = 999;
    private static final int PERMISSIONS_REQUEST_GET_ACCOUNTS = 666;
    private TelephonyManager mTelephonyManager;
    public Double latitude = 0.0;
    public Double longitude = 0.0;
    private LocationManager locationManager;
    private Criteria criteria;
    private static final int PERMISSIONS_READ_LOCATION = 100;
    public String company = "";
    public String strGmail = "";


    @SuppressLint("ObsoleteSdkInt")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_registration );
        Button button = (Button) findViewById ( R.id.button3 );
        final EditText userName = (EditText) findViewById ( R.id.editName );
        final EditText userPhone = (EditText) findViewById ( R.id.editPhone );
        final EditText userEmail = (EditText) findViewById ( R.id.editEmail );
        final Spinner spinner = (Spinner) findViewById ( R.id.company_spinner );
        spinner.setOnItemSelectedListener ( new CustomOnItemSelectedListener () );
        db = new PlumbleSQLiteDatabase ( this );
        telephonyManager = (TelephonyManager) this.getSystemService ( Context.TELEPHONY_SERVICE );


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {

            //showing dialog to select image
            String possibleEmail = null;

            Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
            Account[] accounts = AccountManager.get ( Registration.this ).getAccounts ();
            for (Account account : accounts) {
                if (emailPattern.matcher ( account.name ).matches ()) {
                    possibleEmail = account.name;
                    strGmail = possibleEmail;
                    userEmail.setText ( possibleEmail );
                }
            }


        } else {

            String possibleEmail = null;

            Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
            Account[] accounts = AccountManager.get ( Registration.this ).getAccounts ();
            for (Account account : accounts) {
                if (emailPattern.matcher ( account.name ).matches ()) {
                    possibleEmail = account.name;
                    strGmail = possibleEmail;
                    userEmail.setText ( possibleEmail );
                }


            }
        }
        getDeviceImei ();


        button.setOnClickListener (
                new Button.OnClickListener () {


                    public void onClick(View v) {


                        final String fullName = userName.getText ().toString ();
                        final String PhoneNumber = userPhone.getText ().toString ();
                        final String Email = userEmail.getText ().toString ();
                        text = fullName + " " + PhoneNumber + " " + Email + "\n";
                        register ( IMEI_Number_Holder, fullName, PhoneNumber, Email, Gmail, Group, company );

                    }


                }
        );

        locationManager = (LocationManager) getSystemService ( Context.LOCATION_SERVICE );
        // Define the criteria how to select the location provider
        criteria = new Criteria ();
        criteria.setAccuracy ( Criteria.ACCURACY_COARSE );   //default
        requestUpdates ();

    }

    @SuppressLint("MissingPermission")
    public void requestUpdates() {

      /* locationManager.requestLocationUpdates ( LocationManager.GPS_PROVIDER,
         2000, 1, (LocationListener) this );*/
    }

    public void register(String IMEI, String fullName, String PhoneNumber, String Email, String Gmail, String Group, String company) {

        PlumbleSQLiteDatabase rs = new PlumbleSQLiteDatabase ( this );
        rs.updateOwnerList ( new OwnerList (1, IMEI, fullName, PhoneNumber, Email, Gmail, Group, company ) );
        /*Toast.makeText ( getApplicationContext (), possibleEmail + "Record to Database Saved" + IMEI_Number_Holder,
                Toast.LENGTH_LONG ).show ();*/
        // Refresh main activity upon close of dialog box
        Intent refresh = new Intent ( this, AlertsFree.class );
        startActivity ( refresh );
        finish ();

    }


    private void getDeviceImei() {

        if (ActivityCompat.checkSelfPermission ( this, Manifest.permission.READ_PHONE_STATE ) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        IMEI_Number_Holder = telephonyManager.getDeviceId ();

    }

    @Override
    public void onPause() {
        // Add the following line to unregister the Sensor Manager onPause
        super.onPause ();

        locationManager.removeUpdates(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        String msg = "" + location.getLatitude ()
                + ", " + location.getLongitude ();
        Toast.makeText ( getApplicationContext (), msg,
                Toast.LENGTH_SHORT ).show ();
        latitude = location.getLatitude ();
        longitude = location.getLongitude ();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

        Toast.makeText ( getBaseContext (), "Gps is turned on!! ",
                Toast.LENGTH_SHORT ).show ();
    }

    @Override
    public void onProviderDisabled(String provider) {

        Toast.makeText ( getBaseContext (), "Gps is turned off!! ",
                Toast.LENGTH_SHORT ).show ();
    }

    public class CustomOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView <?> parent, View view, int pos, long id) {

            company=parent.getItemAtPosition ( pos ).toString ();
        }

        @Override
        public void onNothingSelected(AdapterView <?> arg0) {
            // TODO Auto-generated method stub
        }
    }

}
