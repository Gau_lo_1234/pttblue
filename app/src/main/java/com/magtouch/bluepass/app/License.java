package com.magtouch.bluepass.app;

import android.content.Intent;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.magtouch.bluepass.R;

public class License extends AppCompatActivity {

    private Button button;
    private Button exitB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_license);
        exitB = (Button) findViewById ( R.id.licReject );
        button = (Button) findViewById ( R.id.licAccept );
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                startActivity(new Intent(License.this, QR_Registration.class));
                finish();
            }
        });

        exitB.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
                System.exit(0);
            }
        });

    }

}
