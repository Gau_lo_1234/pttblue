package com.magtouch.bluepass.app;

public class imageLogo {
    int id;
    public String logoname;
    public imageLogo(){

    }
    public imageLogo(int id, String logoname){

        this.id=id;
        this.logoname=logoname;
    }

    public int getId() {
        return id;
    }

    public String getLogoname() {
        return logoname;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLogoname(String logoname) {
        this.logoname = logoname;
    }
}
