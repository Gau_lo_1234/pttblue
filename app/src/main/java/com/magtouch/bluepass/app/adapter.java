package com.magtouch.bluepass.app;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.magtouch.bluepass.R;

import java.util.ArrayList;

/**
 * Created by android on 2018/02/06.
 */

public class adapter extends BaseAdapter {
    Context context;
    ArrayList<Contact> contactList;
    private  SparseBooleanArray mSelectedItemsIds;

    public adapter(Context context, ArrayList<Contact> list) {

        this.context = context;
        contactList = list;
    }

    @Override
    public int getCount() {

        return contactList.size();
    }

    @Override
    public Object getItem(int position) {

        return contactList.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup arg2) {
        Contact contact = contactList.get(position);

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.contact_list_row, null);

        }
        TextView tvSlNo = convertView.findViewById( R.id.tv_slno);
        tvSlNo.setText(String.valueOf(contact.getId()));
        TextView tvName = convertView.findViewById(R.id.tv_name);
        tvName.setText(contact.getName());
        TextView tvPhone = convertView.findViewById(R.id.tv_phone);
        tvPhone.setText(contact.getPhone_number());

        return convertView;
    }
    public int id;




}
