package com.magtouch.bluepass.app;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.gsm.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.magtouch.bluepass.R;
import com.magtouch.bluepass.db.PlumbleSQLiteDatabase;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class Notifylist extends Fragment {

    public Button SendSMS;
    public String[] gaulo;
    public String log="";
    public PlumbleSQLiteDatabase db;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =inflater.inflate ( R.layout.fragment_notifylist, container, false );
        SendSMS=view.findViewById(R.id.buttonsms);
        db=new PlumbleSQLiteDatabase(getActivity ());
        SendSMS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ViewAllContacts();
                String sms = "you have been setup to be notified in the case of emergency. Please communicate with Gaudencio if you would not like to be on his alert list";

                for (int i = 0; i < gaulo.length; i++) {
                    sendSMS(gaulo[i], sms);
                }

            }
        });
        // Inflate the layout for this fragment
        return view;
    }

    public void ViewAllContacts() {
        try{
            List<Contact> contacts = db.getAllContacts();
            for (Contact cn : contacts) {
                log = log + cn.getName().trim() + " ";
            }
            gaulo =log.split(" ");
            db.close();
        }catch(Exception Ex){
            Ex.printStackTrace ();
        }

    }



    public void sendSMS(String phoneNo, String msg) {
        if(AlertsFree.isSwitchOn){
            Toast.makeText(getContext (), "Cant send message in Test mode",
                    Toast.LENGTH_SHORT).show();
        }
        else {
            try {
                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(phoneNo, null, msg, null, null);
                Toast.makeText(getContext (), "Message Sent",
                        Toast.LENGTH_SHORT).show();
            } catch (Exception ex) {
                Toast.makeText(getContext (), ex.getMessage(),
                        Toast.LENGTH_SHORT).show();
                ex.printStackTrace();
            }
        }
    }

}
