package com.magtouch.bluepass.app;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.magtouch.bluepass.R;
import com.magtouch.bluepass.db.PlumbleSQLiteDatabase;

/**
 * A simple {@link Fragment} subclass.
 */
public class Profile extends Fragment {

    public EditText firstName;
    public EditText groupName;
    public EditText userEmail;
    public EditText phoneNumber;
    public Button btnAdd;
    public String log,log1,log2;
    public PlumbleSQLiteDatabase db;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate ( R.layout.fragment_profile, container, false );
        db = new PlumbleSQLiteDatabase(getActivity ());
        firstName=view.findViewById(R.id.firstName);
        groupName=view.findViewById(R.id.GroupName);
        btnAdd=view.findViewById(R.id.btnadd);
        userEmail=view.findViewById(R.id.emailAddress);
        phoneNumber=view.findViewById(R.id.phoneNumber);
        ViewRecord ();
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String PhoneNumber = phoneNumber.getText().toString();
                String fullName=firstName.getText().toString();
                String Email = userEmail.getText().toString();
                db.updateOwnerList(new OwnerList(1, fullName, PhoneNumber, Email));
                Toast.makeText(getActivity (), "Record Saved",
                        Toast.LENGTH_SHORT).show();
            }
        });
        return  view;
    }
    public void ViewRecord() {
        OwnerList ownerList = db.getOwnerList(1);
        firstName.setText(ownerList.getName());
        userEmail.setText(ownerList.getEmail());
        phoneNumber.setText(ownerList.getPhone_number());
        groupName.setText(ownerList.getGroup());
        groupName.setEnabled(false);

    }

}
