package com.magtouch.bluepass.app;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.magtouch.jumble.model.Server;
import com.magtouch.bluepass.R;
import com.magtouch.bluepass.Settings;
import com.magtouch.bluepass.channel.ChannelFragment;
import com.magtouch.bluepass.db.PlumbleDatabase;
import com.magtouch.bluepass.db.PlumbleSQLiteDatabase;
import com.magtouch.bluepass.servers.ServerAdapter;
import com.magtouch.bluepass.service.IPlumbleService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;


public class ProfileSettings extends Fragment {
    private ServerAdapter mServerAdapter;
    private ViewPager mViewPager;
    private Toast mToastToShow;
    public Camera camera;
    public ImageView imageView;
    public ImageView comlogo;
    public Boolean imageState=true;
    public int count=1;
    public String logoImage="ic_launcher_logo.png";
    public String server_url1=Constants.image_logo + logoImage;
    public Camera.Parameters parameters;
    public ChannelFragment ptt =new ChannelFragment ();
    public PlumbleSQLiteDatabase db;
    public String serverhost;
    public String serveruser;
    public String servername;
    public String email;
    public String AccountID;
    public String ExpiryDate;
    public int serverport;
    public String serverpass;
    public String server_url="";
    public String IMEI_Number_Holder;
    public String hashedKey="";
    public TelephonyManager telephonyManager;
    public   TextView clientname;
    public   TextView expirydate;
    public   TextView accID;
    public TextView Userphone;
    public TextView Usercompany;
    public TextView Pttmodule;
    public TextView Smsmodule;
    public TextView Controlroom;
    public String userphone;
    public String usercompany;
    public String pttmodule;
    public String smsmodule;
    public String controlroom;
    public String channel_name;
    private List<Movie> movieList = new ArrayList<> ();
    private RecyclerView recyclerView;
    private MoviesAdapter mAdapter;

    public ProfileSettings() {

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view= inflater.inflate ( R.layout.fragment_profile_settings, container, false );
        telephonyManager = (TelephonyManager) getActivity ().getSystemService ( Context.TELEPHONY_SERVICE );
        db = new PlumbleSQLiteDatabase (getActivity ());
       comlogo=view.findViewById ( R.id.comlogo );
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        mAdapter = new MoviesAdapter(movieList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager (getActivity ());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator ());
        recyclerView.setAdapter(mAdapter);



        List<imageLogo> logos = db.getAllLogos ();
        for(imageLogo fl : logos){
            logoImage = fl.getLogoname ();
        }
        //connectToServer (mDatabase.getServer (new Server(1))  );
        getDeviceImei();

        // Required empty public constructor
        Pass_Code pass_code =db.getPassCode ( 1 );
        hashedKey=pass_code.getPass_key ();
        //device_connect();
        logoView ();
        prepareMovieData();
        return view;
    }

    /**
     * This function is called every time the device is loading...
     */
    public void device_connect(){
        server_url=Constants.connect_url + hashedKey + "/" + IMEI_Number_Holder;
        //Toast.makeText ( getActivity (),server_url,Toast.LENGTH_LONG ).show ();
        StringRequest stringRequest= new StringRequest( Request.Method.GET, server_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONArray jsonArray = null;
                    jsonArray = new JSONArray (response);
                    JSONObject jsonObject;
                    for (int i = 0; i < jsonArray.length(); i++) {


                        /**
                         * Jason objects
                         */
                        jsonObject = jsonArray.getJSONObject(i);
                        serverhost = jsonObject.getString("server_ip");
                        servername=jsonObject.getString ( "server_name" );
                        serverport = jsonObject.getInt ("server_port");
                        serverpass="";
                        email= jsonObject.getString ("email");
                        AccountID=jsonObject.getString ("device_id");
                        usercompany=serveruser=jsonObject.getString("distributor_name");
                        ExpiryDate=jsonObject.getString("expiry");
                        userphone=jsonObject.getString("distributor_phone");
                        pttmodule=module_status (jsonObject.getString("ptt"));
                        smsmodule=module_status (jsonObject.getString("sms"));
                        controlroom=module_status (jsonObject.getString("control_room"));
                        channel_name=jsonObject.getString("channel_name");
                        db.updateUserInfo (new user_info (1,usercompany,ExpiryDate,userphone,pttmodule,smsmodule,"qrscans","",controlroom, channel_name));

                        //connectToServer ( new Server (servername,serverhost, serverport, serveruser,serverpass) );
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(getActivity ()).create();
                    alertDialog.setTitle("No settings found");
                    alertDialog.setMessage(" Settings for your device were not found on this server contact your service privider" + response);

                    alertDialog.setButton( android.support.v7.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();

                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText ( getActivity (), "Check your network settings", Toast.LENGTH_LONG ).show ();
                error.printStackTrace();

            }
        });

        MySingleton.getInstance(getActivity ()).addToRequestQue(stringRequest);
        prepareMovieData();

    }
    private void getDeviceImei() {

        if (ActivityCompat.checkSelfPermission (getActivity (), Manifest.permission.READ_PHONE_STATE ) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        IMEI_Number_Holder = telephonyManager.getDeviceId ();

    }
    public String module_status(String module){
        String status="Not Active";
        if(module.equals ( "0" )){
            status="Active";
        }
        else{
            status="Not Active";
        }

        return status;
    }
    public void logoView(){
        logoImage=logoImage.replaceAll(" ","");
       // server_url1="http://liveguarding.co.za/fa/assets/images/" + logoImage;
       server_url1=logoImage;

        ImageRequest imageRequest = new ImageRequest(server_url1, new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap response) {
                comlogo.setImageBitmap(response);

            }
        }, 0, 0, ImageView.ScaleType.CENTER_CROP, null, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        MrSingleton.getmInstance(getActivity ()).addToRequestQueue(imageRequest);
        imageState=false;


    }
    private void prepareMovieData() {
        user_info userInfo = db.getUserInfo ( 1 );

        Movie movie = new Movie("Company Name", userInfo.getUsercompany (), "2018");
        movieList.add(movie);

        long unix_seconds = Long.parseLong(userInfo.getExpiryDate ());
        //convert seconds to milliseconds
        Date date = new Date(unix_seconds*1000L);
        // format of the date
        SimpleDateFormat jdf = new SimpleDateFormat ("yyyy-MM-dd");
        //jdf.setTimeZone( TimeZone.getTimeZone("GMT-4"));
        String java_date = jdf.format(date);

        movie = new Movie("Registration Date", java_date, "");
        movieList.add(movie);
        if(AccountID!=null) {
        movie = new Movie ( "Account ID", AccountID, "" );
        movieList.add ( movie );
        }
        movie = new Movie("Phone Number", userInfo.getUserphone (), "");
        movieList.add(movie);

        movie = new Movie("PTT Module", userInfo.getPttmodule (), "");
        movieList.add(movie);

        movie = new Movie("SMS Module", userInfo.getSmsmodule (), "");
        movieList.add(movie);

        movie = new Movie("Alert Events", userInfo.getAlerts (), "");
        movieList.add(movie);
        movie = new Movie("", "", "");
        movieList.add(movie);

        mAdapter.notifyDataSetChanged();
    }
}
