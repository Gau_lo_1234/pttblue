package com.magtouch.bluepass.app;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.blikoon.qrcodescanner.QrCodeActivity;
import com.magtouch.bluepass.R;
import com.magtouch.bluepass.db.PlumbleDatabase;
import com.magtouch.bluepass.db.PlumbleSQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;


public class Activation extends Fragment {
    private Button button;
    private TextView txtSkip;
    private ImageView scan_now;
    public Camera camera;
    public String message="";

    public Camera.Parameters parameters;
    private static final int REQUEST_CODE_QR_SCAN = 101;
    private static final int REQUEST_INTERNET = 200;
    private final String LOGTAG = "QRCScanner-MainActivity";
    public EditText qrcode;
    public Button qrbut;
    public String user_exists="";
    public String passkey;
    public String server_url="";
    private PlumbleDatabase mDatabase;
    public String IMEI_Number_Holder;
    public TelephonyManager telephonyManager;
    private static final int MY_PERMISSIONS_REQUEST_ACCOUNTS = 1;
    private static final int MY_PERMISSIONS_REQUEST_CAMERA=201;
    public Activation() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate ( R.layout.fragment_activation, container, false );
        scan_now=view.findViewById ( R.id.scan_now );
        checkAndRequestPermissions();
        telephonyManager = (TelephonyManager) getActivity ().getSystemService ( Context.TELEPHONY_SERVICE );
        mDatabase = new PlumbleSQLiteDatabase (getActivity ()); // TODO add support for cloud storage
        mDatabase.open();
        getDeviceImei ();

        try{
            releaseCam ();

        }catch (Exception e){
            e.printStackTrace ();
        }
        scan_now.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View view) {
                releaseCam ();

                Vibrator vibrator = (Vibrator) getActivity ().getSystemService( Context.VIBRATOR_SERVICE);
                vibrator.vibrate(100);
                Intent i = new Intent(getActivity (),QrCodeActivity.class);
                startActivityForResult( i,REQUEST_CODE_QR_SCAN);
            }
        } );

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(resultCode != Activity.RESULT_OK)
        {
            Log.d(LOGTAG,"COULD NOT GET A GOOD RESULT.");
            if(data==null)
                return;
            //Getting the passed result
            String result = data.getStringExtra("com.blikoon.qrcodescanner.error_decoding_image");
            if( result!=null)
            {
                AlertDialog alertDialog = new AlertDialog.Builder(getActivity ()).create();
                alertDialog.setTitle("Scan Error");
                alertDialog.setMessage("QR Code could not be scanned");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
            return;

        }
        if(requestCode == REQUEST_CODE_QR_SCAN)
        {
            if(data==null)
                return;
            //Getting the passed result
            String result = data.getStringExtra("com.blikoon.qrcodescanner.got_qr_scan_relult");
            Log.d(LOGTAG,"Have scan result in your app activity :"+ result);
            AlertDialog alertDialog = new AlertDialog.Builder(getActivity ()).create();
            alertDialog.setTitle("Scan result");
            alertDialog.setMessage(result);
            passkey=result;
            PlumbleSQLiteDatabase rs = new PlumbleSQLiteDatabase ( getActivity () );
            rs.updatePassKey ( new Pass_Code (1,passkey,"true"  ) );
            rs.updatefreeApp ( new freeApp (1,"true","true"  ) );
            device_authenticate();
            if(user_exists.equals ( "yes" )){

            }
            if(user_exists.equals ( "no" )){
                Intent home =new Intent(getActivity (), PlumbleActivity.class);
                startActivity(home);

            }

            alertDialog.setButton( AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            // alertDialog.show();

        }
    }
    private void getDeviceImei() {

        if (ActivityCompat.checkSelfPermission ( getActivity (), Manifest.permission.READ_PHONE_STATE ) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        IMEI_Number_Holder = telephonyManager.getDeviceId ();

    }
    private boolean checkAndRequestPermissions() {
        int permissionCAMERA = ContextCompat.checkSelfPermission(getActivity (),
                Manifest.permission.CAMERA);


        int storagePermission = ContextCompat.checkSelfPermission(getActivity (),


                Manifest.permission.READ_EXTERNAL_STORAGE);

        int smsPermission = ContextCompat.checkSelfPermission(getActivity (),


                Manifest.permission.READ_SMS);
        int sendPermission = ContextCompat.checkSelfPermission(getActivity (),


                Manifest.permission.SEND_SMS);
        int contactsPermission = ContextCompat.checkSelfPermission(getActivity (),


                Manifest.permission.READ_CONTACTS);
        int locationPermission = ContextCompat.checkSelfPermission(getActivity (),


                Manifest.permission.ACCESS_COARSE_LOCATION);
        int statePermission = ContextCompat.checkSelfPermission(getActivity (),


                Manifest.permission.READ_PHONE_STATE);

        int recordPermission = ContextCompat.checkSelfPermission(getActivity (),


                Manifest.permission.RECORD_AUDIO);
        int writePermission = ContextCompat.checkSelfPermission(getActivity (),

                Manifest.permission.WRITE_CONTACTS);




        List<String> listPermissionsNeeded = new ArrayList<> ();
        if (storagePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (smsPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_SMS);
        }
        if (writePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_CONTACTS);
        }
        if (recordPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECORD_AUDIO);
        }
        if (statePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
        }
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (contactsPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_CONTACTS);
        }
        if (sendPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.SEND_SMS);
        }
        if (permissionCAMERA != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity (),


                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MY_PERMISSIONS_REQUEST_CAMERA);
            return false;
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCOUNTS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    //Permission Granted Successfully. Write working code here.
                } else {
                    //You did not accept the request can not use the functionality.
                }
                break;
        }
    }

    /**
     * This function is called every time the device is loading...
     */
    public void device_authenticate(){
        final String server_url = Constants.device_connect + passkey + "/" + md5 (  IMEI_Number_Holder);
        //Toast.makeText ( getBaseContext (),server_url,Toast.LENGTH_LONG ).show ();
        StringRequest stringRequest= new StringRequest( Request.Method.GET, server_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONArray jsonArray = null;
                    jsonArray = new JSONArray (response);
                    JSONObject jsonObject;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObject = jsonArray.getJSONObject(i);
                        message = jsonObject.getString("message");

                    }

                    if (message.equals ( "no" )) {

                        android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder ( getActivity () ).create ();
                        alertDialog.setTitle ( "Alert !!" );
                        alertDialog.setMessage ( "Please contact your provider, you cant utilize our services at this moment" );
                        alertDialog.setButton ( android.support.v7.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener () {
                                    public void onClick(DialogInterface dialog, int which) {

                                        dialog.dismiss ();

                                    }
                                } );
                        alertDialog.show ();

                    }
                    if (message.equals ( "yes" )) {
                        android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder ( getActivity () ).create ();
                        alertDialog.setTitle ( "Alert !!" );
                        alertDialog.setMessage ( "Congraulations you can now enjoy all services" );
                        alertDialog.setButton ( android.support.v7.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener () {
                                    public void onClick(DialogInterface dialog, int which) {
                                        PlumbleSQLiteDatabase rs = new PlumbleSQLiteDatabase ( getActivity () );
                                        mDatabase.addPassCode ( new Pass_Code ( 1, passkey, "true" ) );
                                        rs.updatefreeApp ( new freeApp ( 1, "true", "true" ) );
                                        Intent home = new Intent ( getContext (), PlumbleActivity.class );
                                        startActivity ( home );
                                        dialog.dismiss ();
                                        getActivity ().finish ();
                                    }
                                } );
                        alertDialog.show ();

                    }
                    if (message.equals ( "wel" )) {
                        android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder ( getActivity ()).create ();
                        alertDialog.setTitle ( "Alert !!" );
                        alertDialog.setMessage ( "Welcome back you can now enjoy all services" );
                        alertDialog.setButton ( android.support.v7.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener () {
                                    public void onClick(DialogInterface dialog, int which) {
                                        PlumbleSQLiteDatabase rs = new PlumbleSQLiteDatabase ( getActivity () );
                                        mDatabase.addPassCode ( new Pass_Code ( 1, passkey, "true" ) );
                                        rs.updatefreeApp ( new freeApp ( 1, "true", "true" ) );
                                        Intent home = new Intent ( getActivity (), PlumbleActivity.class );
                                        startActivity ( home );
                                        dialog.dismiss ();
                                        //finish ();
                                    }
                                } );
                        alertDialog.show ();

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder ( getActivity () ).create ();

                    alertDialog.setTitle ( "Device Information" );
                    alertDialog.setMessage ( response );

                    alertDialog.setButton ( android.support.v7.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener () {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss ();
                                }
                            } );
                    // alertDialog.show ();

                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText ( getActivity (), "Something went wrong", Toast.LENGTH_LONG ).show ();
                error.printStackTrace();

            }
        });

        MySingleton.getInstance(getContext ()).addToRequestQue(stringRequest);

    }


    public void hasCamera(){

        if(getActivity ().getPackageManager ().hasSystemFeature ( PackageManager.FEATURE_CAMERA_FLASH )) {

            camera = Camera.open ();
            parameters = camera.getParameters ();

        }
        else{

        }

    }
    public void releaseCam(){
        if(camera!=null) {
            camera.stopPreview ();
            camera.release ();
            camera = null;
        }
    }
    public static final String md5(final String toEncrypt) {
        try {
            final MessageDigest digest = MessageDigest.getInstance("md5");
            digest.update(toEncrypt.getBytes());
            final byte[] bytes = digest.digest();
            final StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(String.format("%02X", bytes[i]));
            }
            return sb.toString().toUpperCase();
        } catch (Exception exc) {
            return "";
        }
    }
}
