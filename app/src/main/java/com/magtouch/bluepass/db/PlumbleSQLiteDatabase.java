/*
 * Copyright (C) 2014 Andrew Comminos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.magtouch.bluepass.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.magtouch.jumble.model.Server;
import com.magtouch.bluepass.Constants;
import com.magtouch.bluepass.app.Contact;
import com.magtouch.bluepass.app.Messages;
import com.magtouch.bluepass.app.OwnerList;
import com.magtouch.bluepass.app.Pass_Code;
import com.magtouch.bluepass.app.Serial;
import com.magtouch.bluepass.app.Settingz;
import com.magtouch.bluepass.app.Terms_Conditions;
import com.magtouch.bluepass.app.freeApp;
import com.magtouch.bluepass.app.imageLogo;
import com.magtouch.bluepass.app.user_info;

import java.util.ArrayList;
import java.util.List;

public class PlumbleSQLiteDatabase extends SQLiteOpenHelper implements PlumbleDatabase {
    public static final String DATABASE_NAME = "mumble.db";

    public static final String TABLE_SERVER = "server";
    public static final String SERVER_ID = "_id";
    public static final String SERVER_NAME = "name";
    public static final String SERVER_HOST = "host";
    public static final String SERVER_PORT = "port";
    public static final String SERVER_USERNAME = "username";
    public static final String SERVER_PASSWORD = "password";
    public static final String TABLE_SERVER_CREATE_SQL = "CREATE TABLE IF NOT EXISTS `" + TABLE_SERVER + "` ("
            + "`" + SERVER_ID + "` INTEGER PRIMARY KEY AUTOINCREMENT,"
            + "`" + SERVER_NAME + "` TEXT NOT NULL,"
            + "`" + SERVER_HOST + "` TEXT NOT NULL,"
            + "`" + SERVER_PORT + "` INTEGER,"
            + "`" + SERVER_USERNAME + "` TEXT,"
            + "`" + SERVER_PASSWORD + "` TEXT "
            + ");";

    /**
     *
     *Table Serials
     *
     */
    public static final String TABLE_SERIALS="serial";
    public static final String SERIAL_ID="id";
    public static final String SERIAL_NAME="name";
    public static final String SERIAL_PHONE="phone";
    public static final String SERIAL_EMAIL="email";
    public static final String SERIAL_IMEI="IMEI";
    public String CREATE_SERIALS_TABLE="CREATE TABLE IF NOT EXISTS " + TABLE_SERIALS + "(" + SERIAL_ID + " INTEGER PRIMARY KEY, " + SERIAL_NAME + " TEXT," + SERIAL_PHONE + " TEXT," + SERIAL_EMAIL + " TEXT," + SERIAL_IMEI + " TEXT" + ")";
    public static final String INSERT_TABLE_SERIALS="INSERT INTO " + TABLE_SERIALS + "(name, phone, email, IMEI) VALUES('none', 'none','none', 'none')";




    /**
     *
     * Table settings
     */
    public static final String TABLE_SETTINGS="settings";
    public static final String SET_ID="set_id";
    public static final String SET_VALUE="set_value";
    public static final String SET_TYPE="set_type";
    String CREATE_SETTINGS_TABLE="CREATE TABLE IF NOT EXISTS " + TABLE_SETTINGS + "(" + SET_ID + " INTEGER PRIMARY KEY, " + SET_VALUE + " TEXT," + SET_TYPE + " TEXT" + ")";
    String INSERT_SETTINGS_TABLE1="INSERT INTO " + TABLE_SETTINGS + "(set_value, set_type) VALUES('true', 'TESTMODE')";
    String INSERT_SETTINGS_TABLE2="INSERT INTO " + TABLE_SETTINGS + "(set_value, set_type) VALUES('30','ACCURACY')";
    String INSERT_SETTINGS_TABLE3="INSERT INTO " + TABLE_SETTINGS + "(set_value, set_type) VALUES('High', 'SHAKE')";
    String INSERT_SETTINGS_TABLE4="INSERT INTO " + TABLE_SETTINGS + "(set_value, set_type) VALUES('off','GPS_SWITCH')";


    /**
     * Details for the owner of the phone device
     */
    public static final String TABLE_OWNER="OwnerTable";
    public static final String OWNER_ID="owner_id";
    public static final String OWNER_IMEI="imei";
    public static final String OWNER_NAME="owner_name";
    public static final String OWNER_PHONE="owner_phone";
    public static final String OWNER_EMAIL="owner_email";
    public static final String OWNER_GMAIL="owner_gmail";
    public static final String OWNER_GROUP="owner_group";
    public static final String OWNER_COMPANY="owner_company";
    public String CREATE_OWNER_TABLE="CREATE TABLE IF NOT EXISTS " + TABLE_OWNER + "(" + OWNER_ID + " INTEGER PRIMARY KEY, " + OWNER_IMEI + " TEXT," +  OWNER_NAME + " TEXT," +  OWNER_PHONE + " TEXT," +  OWNER_EMAIL + " TEXT," +  OWNER_GMAIL + " TEXT," +  OWNER_GROUP + " TEXT," +  OWNER_COMPANY + " TEXT" +")";
    public String INSERT_OWNER_TABLE="INSERT INTO " + TABLE_OWNER + "(owner_id, imei, owner_name, owner_phone, owner_email, owner_gmail, owner_group, owner_company) VALUES('1', 'none', 'none','none','none','none','none','none')";


    /**
     * user information obtained from the server
     */
    public static final String TABLE_USER_INFO="user_info";
    public static final String USER_ID="id";
    public static final String EXPIRY_DATE="expiry_date";
    public static final String DISTRIBUTOR_NAME="distributor_name";
    public static final String DISTRIBUTOR_PHONE="distributor_phone";
    public static final String ACCOUNT_ID="account_id";
    public static final String DISTRIBUTOR_EMAIL="distributor_email";
    public static final String PTT_MODULE="ptt_module";
    public static final String SMS_MODULE="sms_module";
    public static final String ALERT_EVENTS="alert_events";
    public static final String CHANNEL_NAME="channel_name";
    public String CREATE_INFO_TABLE="CREATE TABLE IF NOT EXISTS " + TABLE_USER_INFO + "(" + USER_ID + " INTEGER PRIMARY KEY, " + DISTRIBUTOR_NAME + " TEXT," + EXPIRY_DATE + " TEXT,"  +  DISTRIBUTOR_PHONE + " TEXT," + DISTRIBUTOR_EMAIL +
            " TEXT," +  PTT_MODULE + " TEXT," +  SMS_MODULE + " TEXT,"  + ALERT_EVENTS + " TEXT," + CHANNEL_NAME + " TEXT" + ")";
    public String INSERT_INFO_TABLE="INSERT INTO " + TABLE_USER_INFO +
            "(id,  distributor_name, expiry_date, distributor_phone,  distributor_email, ptt_module, sms_module,  alert_events, channel_name) " +
            "VALUES('1', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null')";


    /**
     * Details for accepting terms and conditions
     */
    public static final String TABLE_CONDITIONS="Terms_Conditions";
    public static final String TERMS_ID="id";
    public static final String TERMS_ACCEPT="Accept";
    public String CREATE_CONDITIONS_TABLE="CREATE TABLE IF NOT EXISTS " + TABLE_CONDITIONS + "(" + TERMS_ID + " INTEGER PRIMARY KEY, " + TERMS_ACCEPT + " TEXT" + ")";
    public String INSERT_CONDITIONS_TABLE="INSERT INTO " + TABLE_CONDITIONS + "(Accept) VALUES('false')";
    /**
     * Details for setting the company logo
     */
    public static final String TABLE_LOGO="logo";
    public static final String LOGO_ID="id";
    public static final String LOGO_NAME="logoname";
    public String CREATE_LOGO_TABLE="CREATE TABLE IF NOT EXISTS " + TABLE_LOGO + "(" + LOGO_ID + " INTEGER PRIMARY KEY, " + LOGO_NAME + " TEXT" + ")";
    public String INSERT_LOGO_TABLE="INSERT INTO " + TABLE_LOGO + "(logoname) VALUES('ic_launcher_logo.png')";


    /**
     * Details for group members
     */
    public static final String TABLE_CONTACTS="contacts";
    public static final String KEY_ID="id";
    public static final String KEY_NAME="name";
    public static final String KEY_PHONE="phone";
    public static final String KEY_EMAIL="email";
    public static final String KEY_IMEI="IMEI";
    public String CREATE_CONTACTS_TABLE="CREATE TABLE IF NOT EXISTS " + TABLE_CONTACTS + "(" + KEY_ID + " INTEGER PRIMARY KEY, " + KEY_NAME + " TEXT," + KEY_PHONE + " TEXT," + KEY_EMAIL + " TEXT," + KEY_IMEI + " TEXT" + ")";

    /**
     * Details for accepting terms and conditions
     */
    public static final String TABLE_FREEAPP="freeApp";
    public static final String FREE_ID="id";
    public static final String FREE_STATUS="freeStatus";
    public static final String FREE_MENU="alertMenu";
    public String CREATE_FREEAPP_TABLE="CREATE TABLE IF NOT EXISTS " + TABLE_FREEAPP + "(" + FREE_ID + " INTEGER PRIMARY KEY, " + FREE_STATUS + " TEXT," + FREE_MENU + " TEXT" + ")";
    public String INSERT_FREEAPP_TABLE="INSERT INTO " + TABLE_FREEAPP + "(freeStatus,alertMenu) VALUES('false','false')";


    /**
     * Details for accepting terms and conditions
     */
    public static final String TABLE_PASSKEY="Pass_Code";
    public static final String PASS_ID="id";
    public static final String PASS_VALUE="pass_key";
    public static final String KEY_STATE="pass_state";
    String CREATE_PASSKEY_TABLE="CREATE TABLE IF NOT EXISTS " + TABLE_PASSKEY + "(" + PASS_ID + " INTEGER PRIMARY KEY, " + PASS_VALUE + " TEXT," + KEY_STATE + " TEXT" + ")";


    public static final String TABLE_SERVER_INSERT_SQL="INSERT INTO " + TABLE_SERVER + "(name, host, port, username, password) VALUES('MagTouch Server', 'server-applications.com','64738', 'Gaudencio','' )";
    public static final String TABLE_FAVOURITES = "favourites";
    public static final String FAVOURITES_ID = "_id";
    public static final String FAVOURITES_CHANNEL = "channel";
    public static final String FAVOURITES_SERVER = "server";
    public static final String TABLE_FAVOURITES_CREATE_SQL = "CREATE TABLE IF NOT EXISTS `" + TABLE_FAVOURITES + "` ("
            + "`" + FAVOURITES_ID + "` INTEGER PRIMARY KEY AUTOINCREMENT,"
            + "`" + FAVOURITES_CHANNEL + "` TEXT NOT NULL,"
            + "`" + FAVOURITES_SERVER + "` INTEGER NOT NULL"
            + ");";

    public static final String TABLE_TOKENS = "tokens";
    public static final String TOKENS_ID = "_id";
    public static final String TOKENS_VALUE = "value";
    public static final String TOKENS_SERVER = "server";
    public static final String TABLE_TOKENS_CREATE_SQL = "CREATE TABLE IF NOT EXISTS `" + TABLE_TOKENS + "` ("
            + "`" + TOKENS_ID + "` INTEGER PRIMARY KEY AUTOINCREMENT,"
            + "`" + TOKENS_VALUE + "` TEXT NOT NULL,"
            + "`" + TOKENS_SERVER + "` INTEGER NOT NULL"
            + ");";

    public static final String TABLE_COMMENTS = "comments";
    public static final String COMMENTS_WHO = "who";
    public static final String COMMENTS_COMMENT = "comment";
    public static final String COMMENTS_SEEN = "seen";
    public static final String TABLE_COMMENTS_CREATE_SQL = "CREATE TABLE IF NOT EXISTS `" + TABLE_COMMENTS + "` ("
            + "`" + COMMENTS_WHO + "` TEXT NOT NULL,"
            + "`" + COMMENTS_COMMENT + "` TEXT NOT NULL,"
            + "`" + COMMENTS_SEEN + "` DATE NOT NULL"
            + ");";

    public static final String TABLE_LOCAL_MUTE = "local_mute";
    public static final String LOCAL_MUTE_SERVER = "server";
    public static final String LOCAL_MUTE_USER = "user";
    public static final String TABLE_LOCAL_MUTE_CREATE_SQL = "CREATE TABLE IF NOT EXISTS " + TABLE_LOCAL_MUTE + " ("
            + "`" + LOCAL_MUTE_SERVER +"` INTEGER NOT NULL,"
            + "`" + LOCAL_MUTE_USER + "` INTEGER NOT NULL,"
            + "CONSTRAINT server_user UNIQUE(" + LOCAL_MUTE_SERVER + "," + LOCAL_MUTE_USER + ")"
            + ");";

    public static final String TABLE_LOCAL_IGNORE = "local_ignore";
    public static final String LOCAL_IGNORE_SERVER = "server";
    public static final String LOCAL_IGNORE_USER = "user";
    public static final String TABLE_LOCAL_IGNORE_CREATE_SQL = "CREATE TABLE IF NOT EXISTS " + TABLE_LOCAL_IGNORE + " ("
            + "`" + LOCAL_IGNORE_SERVER +"` INTEGER NOT NULL,"
            + "`" + LOCAL_IGNORE_USER + "` INTEGER NOT NULL,"
            + "CONSTRAINT server_user UNIQUE(" + LOCAL_IGNORE_SERVER + "," + LOCAL_IGNORE_USER + ")"
            + ");";

    public static final String TABLE_CERTIFICATES = "certificates";
    public static final String COLUMN_CERTIFICATES_ID = "_id";
    public static final String COLUMN_CERTIFICATES_DATA = "data";
    public static final String COLUMN_CERTIFICATES_NAME = "name";
    public static final String TABLE_CERTIFICATES_CREATE_SQL = "CREATE TABLE IF NOT EXISTS " + TABLE_CERTIFICATES + " ("
            + "`" + COLUMN_CERTIFICATES_ID + "` INTEGER PRIMARY KEY AUTOINCREMENT,"
            + "`" + COLUMN_CERTIFICATES_DATA + "` BLOB NOT NULL,"
            + "`" + COLUMN_CERTIFICATES_NAME + "` TEXT NOT NULL"
            + ");";
    public static final String TABLE_MESSAGES="Messages";
    public static final String MSG_ID="msg_id";
    public static final String MSG_MESSAGE="msg_message";
    public static final String MSG_TYPE="msg_type";
    public String CREATE_MESSAGES_TABLE="CREATE TABLE IF NOT EXISTS " + TABLE_MESSAGES + "(" + MSG_ID + " INTEGER PRIMARY KEY, " + MSG_MESSAGE + " TEXT," + MSG_TYPE + " TEXT" + ")";
    public String INSERT_MESSAGES_TABLE="INSERT INTO " + TABLE_MESSAGES + "(msg_message, msg_type) VALUES('I am in trouble and need help urgently.', 'alert')";

    public static final Integer PRE_FAVOURITES_DB_VERSION = 2;
    public static final Integer PRE_TOKENS_DB_VERSION = 3;
    public static final Integer PRE_COMMENTS_DB_VERSION = 4;
    public static final Integer PRE_LOCAL_MUTE_DB_VERSION = 5;
    public static final Integer PRE_LOCAL_IGNORE_DB_VERSION = 6;
    public static final Integer PRE_CERTIFICATES_DB_VERSION = 7;
    public static final Integer CURRENT_DB_VERSION = 8;

    public PlumbleSQLiteDatabase(Context context) {
        super(context, DATABASE_NAME, null, CURRENT_DB_VERSION);
    }

    public PlumbleSQLiteDatabase(Context context, String name) {
        super(context, name, null, CURRENT_DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_SERVER_CREATE_SQL);
        db.execSQL(CREATE_CONTACTS_TABLE);
        db.execSQL(CREATE_MESSAGES_TABLE);
        db.execSQL(TABLE_FAVOURITES_CREATE_SQL);
        db.execSQL (TABLE_CERTIFICATES_CREATE_SQL );
        db.execSQL(CREATE_OWNER_TABLE);
        db.execSQL(CREATE_CONDITIONS_TABLE);
        db.execSQL(CREATE_PASSKEY_TABLE);
        db.execSQL(CREATE_INFO_TABLE);
        db.execSQL(CREATE_FREEAPP_TABLE);
        db.execSQL(CREATE_SETTINGS_TABLE);
        db.execSQL(CREATE_LOGO_TABLE);
        db.execSQL(TABLE_TOKENS_CREATE_SQL);
        db.execSQL(TABLE_COMMENTS_CREATE_SQL);
        db.execSQL(TABLE_LOCAL_MUTE_CREATE_SQL);
        db.execSQL(TABLE_LOCAL_IGNORE_CREATE_SQL);
        db.execSQL(CREATE_SERIALS_TABLE);
        db.execSQL(INSERT_TABLE_SERIALS);
        db.execSQL(INSERT_LOGO_TABLE);
        db.execSQL(INSERT_FREEAPP_TABLE);
        db.execSQL(INSERT_CONDITIONS_TABLE);
        db.execSQL(INSERT_MESSAGES_TABLE);
        db.execSQL(INSERT_SETTINGS_TABLE1);
        db.execSQL(INSERT_SETTINGS_TABLE2);
        db.execSQL(INSERT_SETTINGS_TABLE3);
        db.execSQL(INSERT_SETTINGS_TABLE4);
        db.execSQL ( INSERT_OWNER_TABLE );
        db.execSQL ( INSERT_INFO_TABLE );

    }

    @Override
    public void onUpgrade(
            SQLiteDatabase db,
            int oldVersion,
            int newVersion) {
        Log.w(Constants.TAG, "Database upgrade from " + oldVersion + " to " + newVersion);
        if (oldVersion <= PRE_FAVOURITES_DB_VERSION) {
            db.execSQL(TABLE_FAVOURITES_CREATE_SQL);
        }

        if (oldVersion <= PRE_TOKENS_DB_VERSION) {
            db.execSQL(TABLE_TOKENS_CREATE_SQL);
        }

        if (oldVersion <= PRE_COMMENTS_DB_VERSION) {
            db.execSQL(TABLE_COMMENTS_CREATE_SQL);
        }

        if (oldVersion <= PRE_LOCAL_MUTE_DB_VERSION) {
            db.execSQL(TABLE_LOCAL_MUTE_CREATE_SQL);
        }

        if (oldVersion <= PRE_LOCAL_IGNORE_DB_VERSION) {
            db.execSQL(TABLE_LOCAL_IGNORE_CREATE_SQL);
        }

        if (oldVersion <= PRE_CERTIFICATES_DB_VERSION) {
            db.execSQL(TABLE_CERTIFICATES_CREATE_SQL);
        }
    }

    @Override
    public void open() {
        // Do nothing. Database will be opened automatically when accessing it.
    }


    /**
     * method to get a message from the database
     * @param set_id settings type
     * @return
     */
    public Settingz getSettingz(int set_id) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_SETTINGS, new String[]{SET_ID, SET_VALUE, SET_TYPE}, SET_ID + "=?", new String[]{String.valueOf(set_id)}, null, null, null, null);
        if (cursor != null) {

            cursor.moveToFirst();
        }
        Settingz settingz = new Settingz (Integer.parseInt(cursor.getString(0)),cursor.getString(1), cursor.getString(2));
        return settingz;
    }
    /**
     * The query to update the contacts table
     * @param settingz
     * @return
     */
    public int updateSettingz(Settingz settingz){
        SQLiteDatabase db= this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SET_VALUE, settingz.get_value());
        return db.update(TABLE_SETTINGS, values, SET_TYPE + "=?", new String[] {settingz.get_type()});
    }


    /**
     * method to add terms and conditions table to database
     * @param contact
     */
    public void addContact(Contact contact){

        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put(KEY_NAME, contact.getName());
        values.put(KEY_PHONE, contact.getPhone_number());
        values.put(KEY_EMAIL, contact.getEmail());
        values.put(KEY_IMEI, contact.getIMEI());
        db.insert(TABLE_CONTACTS,null, values);
        db.close();

    }

    /**
     * method to add a custom message to the database
     * @param messages
     */
    public void addMessages(Messages messages){

        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put(MSG_MESSAGE, messages.getMessage());
        values.put(MSG_TYPE, messages.getType());
        db.insert(TABLE_MESSAGES,null, values);
        db.close();

    }
    /**
     * method to get a message from the database
     * @param id
     * @return
     */
    public Messages getMessage(int id) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_MESSAGES, new String[]{MSG_ID, MSG_MESSAGE, MSG_TYPE}, MSG_ID + "=?", new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        Messages messages = new Messages(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2));
        return messages;
    }
    /**
     * method to get all records messages
     * @return
     */

    public List<Messages> getAllMessages(){

        List<Messages> messages = new ArrayList<>();
        String selectQuery="SELECT * FROM " + TABLE_MESSAGES;
        SQLiteDatabase db=this.getReadableDatabase();

        Cursor cursor=db.rawQuery(selectQuery,null);

        if(cursor.moveToFirst()){

            do{
                Messages messages1 = new Messages();

                messages1.setId(Integer.parseInt(cursor.getString(0)));
                messages1.setMessage(cursor.getString(1));
                messages1.setType(cursor.getString(2));

                messages.add(messages1);

            }while (cursor.moveToNext());
        }
        return  messages;

    }

    /**
     * The query to update the contacts table
     * @param messages
     * @return
     */
    public int updateMessages(Messages messages){

        SQLiteDatabase db= this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(MSG_MESSAGE, messages.getMessage());
        values.put(MSG_TYPE, messages.getType());

        return db.update(TABLE_MESSAGES, values, MSG_ID + "=?", new String[] {String.valueOf(messages.getId())});

    }
    /**
     * Query to count all the messages in the contacts table
     * @return
     */
    public int getMessagesCount(){
        String countQuery="SELECT * FROM " + TABLE_MESSAGES;
        SQLiteDatabase db =this.getReadableDatabase();
        Cursor cursor=db.rawQuery(countQuery, null);
        cursor.close();
        return cursor.getCount();

    }

    /**
     * method to get contacts from the database
     * @param id
     * @return
     */
    public Contact getContact(int id) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_CONTACTS, new String[]{KEY_ID, KEY_NAME,KEY_PHONE,KEY_EMAIL}, KEY_ID + "=?", new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null) {

            cursor.moveToFirst();
        }
        Contact contact = new Contact(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3));
        return contact;
    }
    /**
     * method to get all records contacts saved under the contacts table
     * @return
     */

    public List<Contact> getAllContacts(){

        List<Contact> contactList = new ArrayList<>();
        String selectQuery="SELECT * FROM " + TABLE_CONTACTS;
        SQLiteDatabase rs=this.getReadableDatabase();

        Cursor cursor=rs.rawQuery(selectQuery,null);

        if(cursor.moveToFirst()){

            do{
                Contact contact = new Contact();

                contact.setId(Integer.parseInt(cursor.getString(0)));
                contact.setIMEI(cursor.getString(1));
                contact.setName(cursor.getString(2));
                contact.setPhone_number(cursor.getString(3));
                //contact.setEmail(cursor.getString(4));
                contactList.add(contact);

            }while (cursor.moveToNext());
        }
        return  contactList;

    }
    /**
     * The query to update the contacts table
     * @param contact
     * @return
     */
    public int updateContact(Contact contact){

        SQLiteDatabase db= this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, contact.getName());
        values.put(KEY_PHONE, contact.getPhone_number());
        return db.update(TABLE_CONTACTS, values, KEY_ID + "=?", new String[] {String.valueOf(contact.getId())});

    }


    /**
     * The query to delete contacts from the database
     * @param contact
     */
    public void deleteContact(Contact contact){
        SQLiteDatabase db= this.getWritableDatabase();
        db.delete(TABLE_CONTACTS, KEY_ID + "=?", new String[] {String.valueOf(contact.getId())});
    }
    /**
     * Query to count all the contacts in the contacts table
     * @return
     */
    public int getContactCount(){
        String countQuery="SELECT * FROM " + TABLE_CONTACTS;
        SQLiteDatabase db =this.getReadableDatabase();
        Cursor cursor=db.rawQuery(countQuery, null);
        cursor.close();
        return cursor.getCount();
    }
    @Override
    public List<Server> getServers() {
        Cursor c = getReadableDatabase().query(
                TABLE_SERVER,
                new String[]{SERVER_ID, SERVER_NAME, SERVER_HOST,
                        SERVER_PORT, SERVER_USERNAME, SERVER_PASSWORD},
                null,
                null,
                null,
                null,
                null);

        List<Server> servers = new ArrayList<Server>();

        c.moveToFirst();
        while (!c.isAfterLast()) {
            Server server = new Server(c.getInt(c.getColumnIndex(SERVER_ID)),
                    c.getString(c.getColumnIndex(SERVER_NAME)),
                    c.getString(c.getColumnIndex(SERVER_HOST)),
                    c.getInt(c.getColumnIndex(SERVER_PORT)),
                    c.getString(c.getColumnIndex(SERVER_USERNAME)),
                    c.getString(c.getColumnIndex(SERVER_PASSWORD)));
            servers.add(server);
            c.moveToNext();
        }

        c.close();

        return servers;
    }
    /**
     * method to add records to passkey to database
     * @param passCode
     */
    @Override
    public void addPassCode(Pass_Code passCode){

        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put(PASS_VALUE, passCode.getPass_key ());
        db.insert(TABLE_PASSKEY,null, values);
        db.close();

    }

    /**
     * method to get Passkey accept from the database
     * @param id
     * @return
     */

    public Pass_Code getPassCode(int id) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_PASSKEY, new String[]{PASS_ID, PASS_VALUE}, PASS_ID + "=?", new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null) {

            cursor.moveToFirst();
        }
        Pass_Code passCode = new Pass_Code (Integer.parseInt(cursor.getString(0)), cursor.getString(1));
        return passCode;
    }

    /**
     *
     * Function to ftch all saved keys
     * @return
     */
    public List<Pass_Code> getAllKeys(){

        List<Pass_Code> pass_codes = new ArrayList<>();
        String selectQuery="SELECT * FROM " + TABLE_PASSKEY;
        SQLiteDatabase db=this.getReadableDatabase();

        Cursor cursor=db.rawQuery(selectQuery,null);

        if(cursor.moveToFirst()){

            do{
                Pass_Code pass_code = new Pass_Code ();

                pass_code.setId(Integer.parseInt(cursor.getString(0)));
                pass_code.setPass_key (cursor.getString(1));
                pass_codes.add(pass_code);

            }while (cursor.moveToNext());
        }
        return  pass_codes;

    }

    /**
     * The query to update the the pass keys table
     * @param pass_code
     * @return
     */
    public int updatePassKey(Pass_Code pass_code){
        SQLiteDatabase db= this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(PASS_VALUE, pass_code.getPass_key ());
        return db.update(TABLE_PASSKEY, values,PASS_ID + "=?", new String[] {String.valueOf(pass_code.getId())});
    }

    /**
     * method to add terms and conditions to database
     * @param terms_conditions
     */
    public void addTerms(Terms_Conditions terms_conditions){

        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put(TERMS_ACCEPT, terms_conditions.getAccept());
        db.insert(TABLE_CONDITIONS,null, values);
        db.close();

    }

    /**
     * method to get Conditions accept from the database
     * @param id
     * @return
     */
    Terms_Conditions getAccept(int id) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_CONDITIONS, new String[]{TERMS_ID, TERMS_ACCEPT}, TERMS_ID + "=?", new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null) {

            cursor.moveToFirst();
        }
        Terms_Conditions term_conditons = new Terms_Conditions(Integer.parseInt(cursor.getString(0)), cursor.getString(1));
        return term_conditons;
    }

    /**
     * method to get Conditions accept from the database
     * @param id
     * @return
     */
   public Server getServer(long id) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_SERVER, new String[]{SERVER_ID, SERVER_NAME,SERVER_HOST,SERVER_PORT, SERVER_USERNAME,SERVER_PASSWORD}, SERVER_ID + "=?", new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null) {

            cursor.moveToFirst();
        }
        Server server = new Server (Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2),Integer.parseInt ( cursor.getString(3)), cursor.getString(4), cursor.getString(5));
        return server;
    }

    /**
     * method to get all records contacts saved under the terms and conditions table
     * @return
     */

    public List<Server> getAllServers(){

        List<Server> servers = new ArrayList<>();
        String selectQuery="SELECT * FROM " + TABLE_CONDITIONS + " WHERE ID = 1";
        SQLiteDatabase db=this.getReadableDatabase();

        Cursor cursor=db.rawQuery(selectQuery,null);

        if(cursor.moveToFirst()){

            do{
                Server server = new Server (1);
                server.setId(Integer.parseInt(cursor.getString(0)));
                server.setName (cursor.getString(1));
                server.setHost (cursor.getString(2));
                server.setPort (Integer.parseInt (cursor.getString(3)));
                server.setUsername (cursor.getString(4));
                server.setPassword (cursor.getString(5));
                servers.add(server);

            }while (cursor.moveToNext());
        }
        return  servers;

    }




    /**
     * method to get all records contacts saved under the terms and conditions table
     * @return
     */

    public List<Terms_Conditions> getAllTerms(){

        List<Terms_Conditions> term_conditons = new ArrayList<>();
        String selectQuery="SELECT * FROM " + TABLE_CONDITIONS;
        SQLiteDatabase db=this.getReadableDatabase();

        Cursor cursor=db.rawQuery(selectQuery,null);

        if(cursor.moveToFirst()){

            do{
                Terms_Conditions terms_condition = new Terms_Conditions();

                terms_condition.setId(Integer.parseInt(cursor.getString(0)));
                terms_condition.setAccept(cursor.getString(1));
                term_conditons.add(terms_condition);

            }while (cursor.moveToNext());
        }
        return  term_conditons;

    }

    /**
     * The query to update the terms and table
     * @param term_conditons
     * @return
     */
    public int updateTerms(Terms_Conditions term_conditons){
        SQLiteDatabase db= this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TERMS_ACCEPT, term_conditons.getAccept());
        return db.update(TABLE_CONDITIONS, values,TERMS_ID + "=?", new String[] {String.valueOf(term_conditons.getId())});
    }
    /**
     * The query to update the terms and table
     * @param imagelogo
     * @return
     */
    public int updateLogo(imageLogo imagelogo){
        SQLiteDatabase db= this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(LOGO_NAME, imagelogo.getLogoname ());
        return db.update(TABLE_LOGO, values,LOGO_ID + "=?", new String[] {String.valueOf(imagelogo.getId())});
    }
    /**
     * method to get all records contacts saved under the terms and conditions table
     * @return
     */

    public List<imageLogo> getAllLogos(){

        List<imageLogo> logos = new ArrayList<>();
        String selectQuery="SELECT * FROM " + TABLE_LOGO;
        SQLiteDatabase db=this.getReadableDatabase();

        Cursor cursor=db.rawQuery(selectQuery,null);

        if(cursor.moveToFirst()){

            do{
                imageLogo logo = new imageLogo ();

                logo.setId(Integer.parseInt(cursor.getString(0)));
                logo.setLogoname (cursor.getString(1));
                logos.add(logo);

            }while (cursor.moveToNext());
        }
        return  logos;

    }

    /**
     * The query to update the freeApp table
     * @param FreeApp
     * @return
     */
    public int updatefreeApp(freeApp FreeApp){
        SQLiteDatabase db= this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(FREE_STATUS, FreeApp.getFreeStatus ());
        values.put(FREE_MENU, FreeApp.getAlertMenu ());
        return db.update(TABLE_FREEAPP, values,FREE_ID + "=?", new String[] {String.valueOf(FreeApp.getId())});
    }
    /**
     * method to get Conditions accept from the database
     * @param id
     * @return
     */
    freeApp getfreeAppRecords(int id) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_FREEAPP, new String[]{FREE_ID, FREE_STATUS, FREE_MENU}, FREE_ID + "=?", new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null) {

            cursor.moveToFirst();
        }
        freeApp freeApps = new freeApp (Integer.parseInt (cursor.getString(0)), cursor.getString(1),cursor.getString(2));
        return freeApps;
    }

    /**
     *
     * Lists all the records for the free app
     * @return
     */
    public List<freeApp> getAllfreeApp(){

        List<freeApp> freeApps = new ArrayList<>();
        String selectQuery="SELECT * FROM " + TABLE_FREEAPP;
        SQLiteDatabase db=this.getReadableDatabase();

        Cursor cursor=db.rawQuery(selectQuery,null);

        if(cursor.moveToFirst()){

            do{
                freeApp freeAppz = new freeApp ();

                freeAppz.setId(Integer.parseInt(cursor.getString(0)));
                freeAppz.setFreeStatus (cursor.getString(1));
                freeAppz.setAlertMenu (cursor.getString(2));
                freeApps.add(freeAppz);

            }while (cursor.moveToNext());
        }
        return  freeApps;

    }
    /**
     * method to add terms and conditions table to database
     * @param ownerList
     */
    public void addOwner(OwnerList ownerList){

        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put(OWNER_IMEI, ownerList.getIMEI());
        values.put(OWNER_NAME, ownerList.getName());
        values.put(OWNER_PHONE, ownerList.getPhone_number());
        values.put(OWNER_EMAIL, ownerList.getEmail());
        values.put(OWNER_GMAIL, ownerList.getGmail());
        values.put(OWNER_GROUP, ownerList.getGroup());
        values.put(OWNER_COMPANY, ownerList.getCompany ());
        db.insert(TABLE_OWNER,null, values);
        db.close();

    }
    /**
     * method to get contacts from the database
     * @param id
     * @return
     */
    public OwnerList getOwnerList(int id) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_OWNER, new String[]{OWNER_ID, OWNER_NAME,OWNER_PHONE,OWNER_EMAIL,OWNER_GMAIL,OWNER_GROUP,OWNER_COMPANY}, OWNER_ID + "=?", new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null) {

            cursor.moveToFirst();
        }
        OwnerList ownerList = new OwnerList(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3),  cursor.getString(4),  cursor.getString(5),  cursor.getString(6));
        return ownerList;
    }

     /** method to get all records contacts saved under the contacts table
     * @return
      * */
    public List<OwnerList> getAllOwnerList(){

        List<OwnerList> ownerLists = new ArrayList<>();
        String selectQuery="SELECT * FROM " + TABLE_OWNER;
        SQLiteDatabase db=this.getReadableDatabase();

        Cursor cursor=db.rawQuery(selectQuery,null);

        if(cursor.moveToFirst()){

            do{
                OwnerList ownerList = new OwnerList();

                ownerList.setId(cursor.getString(0));
                ownerList.setName(cursor.getString(1));
                ownerList.setPhone_number(cursor.getString(2));
                ownerList.setEmail(cursor.getString(3));
                ownerList.setGmail(cursor.getString(4));
                ownerList.setGroup(cursor.getString(5));
                ownerList.setGroup(cursor.getString(6));
                ownerLists.add(ownerList);

            }while (cursor.moveToNext());
        }
        return  ownerLists;

    }
    /**
     * The query to update the contacts table
     * @param ownerList
     * @return
     */
    public int updateOwnerList(OwnerList ownerList){

        SQLiteDatabase db= this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(OWNER_NAME, ownerList.getName());
        values.put(OWNER_PHONE, ownerList.getPhone_number());
        values.put(OWNER_EMAIL, ownerList.getEmail());
        values.put(OWNER_GMAIL, ownerList.getGmail());
        values.put(OWNER_GROUP, ownerList.getGroup());
        values.put(OWNER_COMPANY, ownerList.getCompany ());
        return db.update(TABLE_OWNER, values, OWNER_ID + "=?", new String[] {String.valueOf(ownerList.getId())});

    }
    /**
     * The query to delete contacts from the database
     * @param ownerList
     */
    public void deleteOwnerList(OwnerList ownerList){
        SQLiteDatabase db= this.getWritableDatabase();
        db.delete(TABLE_OWNER, OWNER_ID + "=?", new String[] {String.valueOf(ownerList.getId())});
        db.close();
    }


    @Override
    public void addServer(Server server) {
        ContentValues values = new ContentValues();
        values.put(SERVER_NAME, server.getName());
        values.put(SERVER_HOST, server.getHost());
        values.put(SERVER_PORT, server.getPort());
        values.put(SERVER_USERNAME, server.getUsername());
        values.put(SERVER_PASSWORD, server.getPassword());

        server.setId(getWritableDatabase().insert(TABLE_SERVER, null, values));
    }

    @Override
    public void updateServer(Server server) {
        SQLiteDatabase db=this.getWritableDatabase ();
        ContentValues values = new ContentValues();
        values.put(SERVER_NAME, server.getName());
        values.put(SERVER_HOST, server.getHost());
        values.put(SERVER_PORT, server.getPort());
        //values.put(SERVER_USERNAME, server.getUsername());
        //values.put(SERVER_PASSWORD, server.getPassword());
        getWritableDatabase().update(
                TABLE_SERVER,
                values,
                SERVER_ID + "=?",
                new String[]{Long.toString(server.getId())});
    }


    @Override
    public void removeServer(Server server) {
        getWritableDatabase().delete(TABLE_SERVER, SERVER_ID + "=?",
                new String[] { String.valueOf(server.getId()) });
        // Clean up server-specific entries
        getWritableDatabase().delete(TABLE_FAVOURITES, FAVOURITES_SERVER + "=?",
                new String[] { String.valueOf(server.getId()) });
        getWritableDatabase().delete(TABLE_TOKENS, TOKENS_SERVER + "=?",
                new String[] { String.valueOf(server.getId()) });
        getWritableDatabase().delete(TABLE_LOCAL_MUTE, LOCAL_MUTE_SERVER + "=?",
                new String[] { String.valueOf(server.getId()) });
        getWritableDatabase().delete(TABLE_LOCAL_IGNORE, LOCAL_IGNORE_SERVER + "=?",
                new String[]{String.valueOf(server.getId())});
    }

    public List<Integer> getPinnedChannels(long serverId) {

        final Cursor c = getReadableDatabase().query(
                TABLE_FAVOURITES,
                new String[]{FAVOURITES_CHANNEL},
                FAVOURITES_SERVER + "=?",
                new String[]{String.valueOf(serverId)},
                null,
                null,
                null);

        List<Integer> favourites = new ArrayList<Integer>();
        c.moveToFirst();
        while (!c.isAfterLast()) {
            favourites.add(c.getInt(0));
            c.moveToNext();
        }

        c.close();

        return favourites;
    }

    @Override
    public void addPinnedChannel(long serverId, int channelId) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(FAVOURITES_CHANNEL, channelId);
        contentValues.put(FAVOURITES_SERVER, serverId);
        getWritableDatabase().insert(TABLE_FAVOURITES, null, contentValues);
    }

    @Override
    public long rowCount() {
        SQLiteDatabase db = this.getReadableDatabase ();
        long numRows = DatabaseUtils.longForQuery ( db, "SELECT COUNT(*) FROM server", null );
        db.close ();
        return numRows;
    }

    @Override
    public long keyCount() {
        SQLiteDatabase db = this.getReadableDatabase ();
        long numRows = DatabaseUtils.longForQuery ( db, "SELECT COUNT(*) FROM Pass_Code", null );
        db.close ();
        return numRows;
    }

    @Override
    public boolean isChannelPinned(long serverId, int channelId) {
        Cursor c = getReadableDatabase().query(
                TABLE_FAVOURITES,
                new String[]{FAVOURITES_CHANNEL},
                FAVOURITES_SERVER + "=? AND " +
                FAVOURITES_CHANNEL + "=?",
                new String[]{String.valueOf(serverId), String.valueOf(channelId)},
                null,
                null,
                null);
        c.moveToFirst();
        return !c.isAfterLast();
    }

    public void removePinnedChannel(long serverId, int channelId) {
        getWritableDatabase().delete(TABLE_FAVOURITES, "server = ? AND channel = ?", new String[] { Long.toString(serverId), Integer.toString(channelId)});
    }

    @Override
    public List<String> getAccessTokens(long serverId) {
        Cursor cursor = getReadableDatabase().query(TABLE_TOKENS, new String[] { TOKENS_VALUE }, TOKENS_SERVER+"=?", new String[] { String.valueOf(serverId) }, null, null, null);
        cursor.moveToFirst();
        List<String> tokens = new ArrayList<String>();
        while(!cursor.isAfterLast()) {
            tokens.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();
        return tokens;
    }

    @Override
    public void addAccessToken(long serverId, String token) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(TOKENS_SERVER, serverId);
        contentValues.put(TOKENS_VALUE, token);
        getWritableDatabase().insert(TABLE_TOKENS, null, contentValues);
    }

    @Override
    public void removeAccessToken(long serverId, String token) {
        getWritableDatabase().delete(TABLE_TOKENS, TOKENS_SERVER+"=? AND "+TOKENS_VALUE+"=?", new String[] {String.valueOf(serverId), token });
    }

    @Override
    public List<Integer> getLocalMutedUsers(long serverId) {
        Cursor cursor = getReadableDatabase().query(TABLE_LOCAL_MUTE,
                new String[] { LOCAL_MUTE_USER },
                LOCAL_MUTE_SERVER + "=?",
                new String[] { String.valueOf(serverId) },
                null, null, null);
        cursor.moveToNext();
        List<Integer> users = new ArrayList<Integer>();
        while (!cursor.isAfterLast()) {
            users.add(cursor.getInt(0));
            cursor.moveToNext();
        }
        return users;
    }

    @Override
    public void addLocalMutedUser(long serverId, int userId) {
        ContentValues values = new ContentValues();
        values.put(LOCAL_MUTE_SERVER, serverId);
        values.put(LOCAL_MUTE_USER, userId);
        getWritableDatabase().insert(TABLE_LOCAL_MUTE, null, values);
    }

    @Override
    public void removeLocalMutedUser(long serverId, int userId) {
        getWritableDatabase().delete(TABLE_LOCAL_MUTE,
                LOCAL_MUTE_SERVER + "=? AND " + LOCAL_MUTE_USER + "=?",
                new String[] { String.valueOf(serverId), String.valueOf(userId) });
    }

    @Override
    public List<Integer> getLocalIgnoredUsers(long serverId) {
        Cursor cursor = getReadableDatabase().query(TABLE_LOCAL_IGNORE,
                new String[] { LOCAL_IGNORE_USER },
                LOCAL_IGNORE_SERVER + "=?",
                new String[] { String.valueOf(serverId) },
                null, null, null);
        cursor.moveToFirst();
        List<Integer> users = new ArrayList<Integer>();
        while (!cursor.isAfterLast()) {
            users.add(cursor.getInt(0));
            cursor.moveToNext();
        }
        return users;
    }

    @Override
    public void addLocalIgnoredUser(long serverId, int userId) {
        ContentValues values = new ContentValues();
        values.put(LOCAL_IGNORE_SERVER, serverId);
        values.put(LOCAL_IGNORE_USER, userId);
        getWritableDatabase().insert(TABLE_LOCAL_IGNORE, null, values);
    }

    @Override
    public void removeLocalIgnoredUser(long serverId, int userId) {
        getWritableDatabase().delete(TABLE_LOCAL_IGNORE,
                LOCAL_IGNORE_SERVER + "=? AND " + LOCAL_IGNORE_USER + "=?",
                new String[] { String.valueOf(serverId), String.valueOf(userId) });
    }

    @Override
    public DatabaseCertificate addCertificate(String name, byte[] certificate) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_CERTIFICATES_NAME, name);
        values.put(COLUMN_CERTIFICATES_DATA, certificate);
        long id = getWritableDatabase().insert(TABLE_CERTIFICATES, null, values);
        return new DatabaseCertificate(id, name);
    }

    @Override
    public List<DatabaseCertificate> getCertificates() {
        Cursor cursor = getReadableDatabase().query(TABLE_CERTIFICATES,
                new String[] { COLUMN_CERTIFICATES_ID, COLUMN_CERTIFICATES_NAME },
                null, null, null, null, null);
        List<DatabaseCertificate> certificates = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            certificates.add(new DatabaseCertificate(cursor.getLong(0), cursor.getString(1)));
            cursor.moveToNext();
        }
        cursor.close();
        return certificates;
    }

    @Override
    public byte[] getCertificateData(long id) {
        Cursor cursor = getReadableDatabase().query(TABLE_CERTIFICATES,
                new String[] { COLUMN_CERTIFICATES_DATA },
                COLUMN_CERTIFICATES_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null);
        if (!cursor.moveToFirst())
            return null;
        byte[] data = cursor.getBlob(0);
        cursor.close();
        return data;
    }

    @Override
    public void removeCertificate(long id) {
        getWritableDatabase().delete(TABLE_CERTIFICATES,
                COLUMN_CERTIFICATES_ID + "=?",
                new String[] { String.valueOf(id) });
    }

    @Override
    public boolean isCommentSeen(String hash, byte[] commentHash) {
        Cursor cursor = getReadableDatabase().query(TABLE_COMMENTS,
                new String[]{COMMENTS_WHO, COMMENTS_COMMENT, COMMENTS_SEEN}, COMMENTS_WHO + "=? AND " + COMMENTS_COMMENT + "=?",
                new String[]{hash, new String(commentHash)},
                null, null, null);
        boolean hasNext = cursor.moveToNext();
        cursor.close();
        return hasNext;
    }

    @Override
    public void markCommentSeen(String hash, byte[] commentHash) {
        ContentValues values = new ContentValues();
        values.put(COMMENTS_WHO, hash);
        values.put(COMMENTS_COMMENT, commentHash);
        values.put(COMMENTS_SEEN, "datetime('now')");
        getWritableDatabase().replace(TABLE_COMMENTS, null, values);
    }
    /**
     * method to add terms and conditions table to database
     * @param serial
     */
    public void addSerial(Serial serial){

        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put(KEY_NAME, serial.getName());
        values.put(KEY_PHONE, serial.getPhone_number());
        values.put(KEY_EMAIL, serial.getEmail());
        values.put(KEY_IMEI, serial.getIMEI());
        db.insert(TABLE_SERIALS,null, values);
        db.close();

    }
    /**
     * method to get contacts from the database
     * @param id
     * @return
     */
    public Serial getSerial(int id) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_SERIALS, new String[]{KEY_ID,KEY_NAME,KEY_PHONE,KEY_EMAIL,KEY_IMEI}, KEY_ID + "=?", new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null) {

            cursor.moveToFirst();
        }
        Serial serial = new Serial (Integer.parseInt (cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getString(3),cursor.getString(4));
        return serial;
    }
    /**
     * method to get all records contacts saved under the contacts table
     * @return
     */

    public List<Serial> getAllSerials(){

        List<Serial> serialList = new ArrayList<>();
        String selectQuery="SELECT * FROM " + TABLE_SERIALS;
        SQLiteDatabase db=this.getReadableDatabase();

        Cursor cursor=db.rawQuery(selectQuery,null);

        if(cursor.moveToFirst()){

            do{
                Serial serial = new Serial ();

                serial.setId(Integer.parseInt(cursor.getString(0)));
                serial.setIMEI(cursor.getString(1));
                serial.setName(cursor.getString(2));
                serial.setPhone_number(cursor.getString(3));
                //contact.setEmail(cursor.getString(4));
                serialList.add(serial);

            }while (cursor.moveToNext());
        }
        return  serialList;

    }
    /**
     * The query to update the contacts table
     * @param serial
     * @return
     */
    public int updateSerial(Serial serial){

        SQLiteDatabase db= this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SERIAL_NAME, serial.getName());
        values.put(SERIAL_PHONE, serial.getPhone_number());
        values.put(SERIAL_EMAIL, serial.getEmail ());
        values.put(SERIAL_IMEI, serial.getIMEI ());
        return db.update(TABLE_SERIALS, values, SERIAL_ID + "=?", new String[] {String.valueOf(serial.getId())});

    }


    /**
     * The query to delete contacts from the database
     * @param serial
     */
    public void deleteSerial(Serial serial){
        SQLiteDatabase db= this.getWritableDatabase();
        db.delete(TABLE_SERIALS, KEY_ID + "=?", new String[] {String.valueOf(serial.getId())});
    }


    /**
     * Query to count all the contacts in the contacts table
     * @return
     */
    public int getSerialCount(){
        String countQuery="SELECT * FROM " + TABLE_SERIALS;
        SQLiteDatabase db =this.getReadableDatabase();
        Cursor cursor=db.rawQuery(countQuery, null);
        cursor.close();
        return cursor.getCount();
    }

    /** method to get all records contacts saved under the contacts table
     * @return
     * */
    public List<user_info> getUserInfo() {

        List <user_info> user_infos = new ArrayList <> ();
        String selectQuery = "SELECT * FROM " + TABLE_OWNER;
        SQLiteDatabase db = this.getReadableDatabase ();

        Cursor cursor = db.rawQuery ( selectQuery, null );

        if (cursor.moveToFirst ()) {

            do {
                user_info userInfo = new user_info ();

                userInfo.setId ( cursor.getInt ( 0 ) );
                userInfo.setUsercompany ( cursor.getString ( 1 ) );
                userInfo.setExpiryDate ( cursor.getString ( 2 ) );
                userInfo.setAccountID ( cursor.getString ( 3 ) );
                userInfo.setUserphone ( cursor.getString ( 4 ));
                userInfo.setPttmodule ( cursor.getString ( 5 ) );
                userInfo.setSmsmodule ( cursor.getString ( 6 ) );
                userInfo.setQR_Scans ( cursor.getString ( 7 ) );
                userInfo.setCustom_events ( cursor.getString ( 8 ) );
                userInfo.setAlerts ( cursor.getString ( 9 ) );
                userInfo.setChannel_name ( cursor.getString ( 10 ) );
                user_infos.add ( userInfo );

            } while (cursor.moveToNext ());
        }
        return user_infos;

    }
    /**
     * method to get a message from the database
     * @param id
     * @return
     */

    public user_info getUserInfo(int id) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_USER_INFO, new String[]{USER_ID, DISTRIBUTOR_NAME, EXPIRY_DATE, DISTRIBUTOR_PHONE, PTT_MODULE, SMS_MODULE, ALERT_EVENTS, CHANNEL_NAME},
                USER_ID + "=?", new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        user_info userInfo = new user_info (Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5),
                 cursor.getString(6), cursor.getString(7));
        return userInfo;
    }

    /**
     * The query to update the contacts table
     * @param userInfo
     * @return
     */
    public int updateUserInfo(user_info userInfo){

        SQLiteDatabase db= this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DISTRIBUTOR_NAME, userInfo.getUsercompany ());
        values.put(EXPIRY_DATE, userInfo.getExpiryDate ());
        values.put(DISTRIBUTOR_PHONE, userInfo.getUserphone ());
        values.put(PTT_MODULE, userInfo.getPttmodule ());
        values.put(SMS_MODULE, userInfo.getSmsmodule ());
        values.put(ALERT_EVENTS, userInfo.getAlerts ());
        values.put(CHANNEL_NAME, userInfo.getChannel_name ());
         return db.update(TABLE_USER_INFO, values, USER_ID + "=?", new String[] {String.valueOf(userInfo.getId())});

    }

    /**
     * method to get a message from the database
     * @param id
     * @return
     */
    public user_info get_UserInfo(int id) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_USER_INFO, new String[]{USER_ID, CHANNEL_NAME}, USER_ID + "=?", new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        user_info userInfo = new user_info (Integer.parseInt(cursor.getString(0)), cursor.getString(1));
        return userInfo;
    }
}
