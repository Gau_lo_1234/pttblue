/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.magtouch.bluepass;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.magtouch.bluepass.free";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "free";
  public static final int VERSION_CODE = 82;
  public static final String VERSION_NAME = "1.3";
  // Fields from product flavor: free
  public static final boolean DONATE_NAG = true;
  // Fields from default config.
}
